<?xml version="1.0" encoding="UTF-8"?>
<solution name="org.clawiz.core.common.storage" uuid="0a9c1f21-e818-46f9-8672-c3f49f66fd3d" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions>
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
    <language slang="l:97fd598e-b769-49ad-bf12-ef327309f6db:org.clawiz.core.common.language" version="0" />
  </languageVersions>
  <dependencyVersions>
    <module reference="0a9c1f21-e818-46f9-8672-c3f49f66fd3d(org.clawiz.core.common.storage)" version="0" />
  </dependencyVersions>
</solution>

