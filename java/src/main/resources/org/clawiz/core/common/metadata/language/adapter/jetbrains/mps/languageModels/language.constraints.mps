<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:acab77a7-6e9a-4d16-99e8-7f39d33b8bfd(org.clawiz.core.common.language.constraints)">
  <persistence version="9" />
  <languages>
    <devkit ref="00000000-0000-4000-0000-5604ebd4f22c(jetbrains.mps.devkit.aspect.constraints)" />
  </languages>
  <imports>
    <import index="lehn" ref="r:f00fd6de-b38e-411a-9521-a0b7f412e1e8(org.clawiz.core.common.language.structure)" />
  </imports>
  <registry>
    <language id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints">
      <concept id="8401916545537438642" name="jetbrains.mps.lang.constraints.structure.InheritedNodeScopeFactory" flags="ng" index="1dDu$B">
        <reference id="8401916545537438643" name="kind" index="1dDu$A" />
      </concept>
      <concept id="1213093968558" name="jetbrains.mps.lang.constraints.structure.ConceptConstraints" flags="ng" index="1M2fIO">
        <reference id="1213093996982" name="concept" index="1M2myG" />
        <child id="1213100494875" name="referent" index="1Mr941" />
      </concept>
      <concept id="1148687176410" name="jetbrains.mps.lang.constraints.structure.NodeReferentConstraint" flags="ng" index="1N5Pfh">
        <reference id="1148687202698" name="applicableLink" index="1N5Vy1" />
        <child id="1148687345559" name="searchScopeFactory" index="1N6uqs" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="1M2fIO" id="IcIQHTvV3I">
    <property role="3GE5qa" value="type.key" />
    <ref role="1M2myG" to="lehn:6porqNrmTAS" resolve="TypeKeyField" />
    <node concept="1N5Pfh" id="IcIQHTvV3J" role="1Mr941">
      <ref role="1N5Vy1" to="lehn:6porqNrmTAU" resolve="field" />
      <node concept="1dDu$B" id="IcIQHTvV3O" role="1N6uqs">
        <ref role="1dDu$A" to="lehn:6porqNrmPQb" resolve="TypeField" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="1yNWL2q_dHQ">
    <property role="3GE5qa" value="service.method" />
    <ref role="1M2myG" to="lehn:1WjxkW0cEWa" resolve="ServiceMethodRef" />
    <node concept="1N5Pfh" id="1yNWL2q_dQJ" role="1Mr941">
      <ref role="1N5Vy1" to="lehn:1WjxkW0cEWc" resolve="method" />
      <node concept="1dDu$B" id="1yNWL2q_dQL" role="1N6uqs">
        <ref role="1dDu$A" to="lehn:xAAq0nImMq" resolve="ServiceMethod" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="1yNWL2q_dQT">
    <property role="3GE5qa" value="type" />
    <ref role="1M2myG" to="lehn:6porqNrmg18" resolve="Type" />
    <node concept="1N5Pfh" id="1yNWL2q_dQU" role="1Mr941">
      <ref role="1N5Vy1" to="lehn:1yNWL2q$y8n" resolve="toStringKey" />
      <node concept="1dDu$B" id="5J_TEGDjURY" role="1N6uqs">
        <ref role="1dDu$A" to="lehn:6porqNrmTAM" resolve="TypeKey" />
      </node>
    </node>
  </node>
</model>

