package org.clawiz.core.common.language.editor;

/*Generated by MPS */

import jetbrains.mps.editor.runtime.descriptor.AbstractEditorBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.openapi.editor.EditorContext;
import jetbrains.mps.openapi.editor.cells.EditorCell;
import jetbrains.mps.nodeEditor.cells.EditorCell_Collection;
import jetbrains.mps.nodeEditor.cellLayout.CellLayout_Horizontal;
import jetbrains.mps.nodeEditor.cells.EditorCell_Constant;
import jetbrains.mps.lang.editor.cellProviders.SingleRoleCellProvider;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import org.jetbrains.mps.openapi.language.SContainmentLink;
import jetbrains.mps.openapi.editor.cells.CellActionType;
import jetbrains.mps.editor.runtime.impl.cellActions.CellAction_DeleteSmart;
import jetbrains.mps.openapi.editor.cells.DefaultSubstituteInfo;
import jetbrains.mps.nodeEditor.cellMenu.SChildSubstituteInfo;
import jetbrains.mps.openapi.editor.menus.transformation.SNodeLocation;

/*package*/ class ReadOnlyIfConditionTypeProperty_EditorBuilder_a extends AbstractEditorBuilder {
  @NotNull
  private SNode myNode;

  public ReadOnlyIfConditionTypeProperty_EditorBuilder_a(@NotNull EditorContext context, @NotNull SNode node) {
    super(context);
    myNode = node;
  }

  @NotNull
  @Override
  public SNode getNode() {
    return myNode;
  }

  /*package*/ EditorCell createCell() {
    return createCollection_kj9jqo_a();
  }

  private EditorCell createCollection_kj9jqo_a() {
    EditorCell_Collection editorCell = new EditorCell_Collection(getEditorContext(), myNode, new CellLayout_Horizontal());
    editorCell.setCellId("Collection_kj9jqo_a");
    editorCell.setBig(true);
    editorCell.setCellContext(getCellFactory().getCellContext());
    editorCell.addEditorCell(createConstant_kj9jqo_a0());
    editorCell.addEditorCell(createRefNode_kj9jqo_b0());
    return editorCell;
  }
  private EditorCell createConstant_kj9jqo_a0() {
    EditorCell_Constant editorCell = new EditorCell_Constant(getEditorContext(), myNode, "readonly if");
    editorCell.setCellId("Constant_kj9jqo_a0");
    editorCell.setDefaultText("");
    return editorCell;
  }
  private EditorCell createRefNode_kj9jqo_b0() {
    SingleRoleCellProvider provider = new ReadOnlyIfConditionTypeProperty_EditorBuilder_a.conditionSingleRoleHandler_kj9jqo_b0(myNode, MetaAdapterFactory.getContainmentLink(0x97fd598eb76949adL, 0xbf12ef327309f6dbL, 0x70a34bafa66d4b1fL, 0x70a34bafa66d4b25L, "condition"), getEditorContext());
    return provider.createCell();
  }
  private static class conditionSingleRoleHandler_kj9jqo_b0 extends SingleRoleCellProvider {
    @NotNull
    private SNode myNode;

    public conditionSingleRoleHandler_kj9jqo_b0(SNode ownerNode, SContainmentLink containmentLink, EditorContext context) {
      super(containmentLink, context);
      myNode = ownerNode;
    }

    @Override
    @NotNull
    public SNode getNode() {
      return myNode;
    }

    protected EditorCell createChildCell(SNode child) {
      EditorCell editorCell = getUpdateSession().updateChildNodeCell(child);
      editorCell.setAction(CellActionType.DELETE, new CellAction_DeleteSmart(getNode(), MetaAdapterFactory.getContainmentLink(0x97fd598eb76949adL, 0xbf12ef327309f6dbL, 0x70a34bafa66d4b1fL, 0x70a34bafa66d4b25L, "condition"), child));
      editorCell.setAction(CellActionType.BACKSPACE, new CellAction_DeleteSmart(getNode(), MetaAdapterFactory.getContainmentLink(0x97fd598eb76949adL, 0xbf12ef327309f6dbL, 0x70a34bafa66d4b1fL, 0x70a34bafa66d4b25L, "condition"), child));
      installCellInfo(child, editorCell);
      return editorCell;
    }



    private void installCellInfo(SNode child, EditorCell editorCell) {
      if (editorCell.getSubstituteInfo() == null || editorCell.getSubstituteInfo() instanceof DefaultSubstituteInfo) {
        editorCell.setSubstituteInfo(new SChildSubstituteInfo(editorCell, myNode, MetaAdapterFactory.getContainmentLink(0x97fd598eb76949adL, 0xbf12ef327309f6dbL, 0x70a34bafa66d4b1fL, 0x70a34bafa66d4b25L, "condition"), child));
      }
      if (editorCell.getRole() == null) {
        editorCell.setRole("condition");
      }
    }
    @Override
    protected EditorCell createEmptyCell() {
      getCellFactory().pushCellContext();
      getCellFactory().setNodeLocation(new SNodeLocation.FromParentAndLink(getNode(), MetaAdapterFactory.getContainmentLink(0x97fd598eb76949adL, 0xbf12ef327309f6dbL, 0x70a34bafa66d4b1fL, 0x70a34bafa66d4b25L, "condition")));
      try {
        EditorCell editorCell = super.createEmptyCell();
        editorCell.setCellId("empty_condition");
        installCellInfo(null, editorCell);
        setCellContext(editorCell);
        return editorCell;
      } finally {
        getCellFactory().popCellContext();
      }
    }
    protected String getNoTargetText() {
      return "<no condition>";
    }
  }
}
