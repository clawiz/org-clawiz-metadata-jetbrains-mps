<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:0e32d7b6-46a9-491c-93ed-58400da79b20(org.clawiz.core.common.language.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="5" />
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="8" />
  </languages>
  <imports>
    <import index="o8zo" ref="r:314576fc-3aee-4386-a0a5-a38348ac317d(jetbrains.mps.scope)" />
    <import index="lehn" ref="r:f00fd6de-b38e-411a-9521-a0b7f412e1e8(org.clawiz.core.common.language.structure)" implicit="true" />
    <import index="tpcu" ref="r:00000000-0000-4000-0000-011c89590282(jetbrains.mps.lang.core.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1180031783296" name="jetbrains.mps.lang.smodel.structure.Concept_IsSubConceptOfOperation" flags="nn" index="2Zo12i">
        <child id="1180031783297" name="conceptArgument" index="2Zo12j" />
      </concept>
      <concept id="2644386474301421077" name="jetbrains.mps.lang.smodel.structure.LinkIdRefExpression" flags="nn" index="359W_D">
        <reference id="2644386474301421078" name="conceptDeclaration" index="359W_E" />
        <reference id="2644386474301421079" name="linkDeclaration" index="359W_F" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="6QcvG2ThKSK">
    <property role="3GE5qa" value="type" />
    <ref role="13h7C2" to="lehn:6porqNrmg18" resolve="Type" />
    <node concept="13hLZK" id="6QcvG2ThKSL" role="13h7CW">
      <node concept="3clFbS" id="6QcvG2ThKSM" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="5J_TEGDjNm6" role="13h7CS">
      <property role="TrG5h" value="getScope" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="tpcu:52_Geb4QDV$" resolve="getScope" />
      <node concept="3clFbS" id="5J_TEGDjNm9" role="3clF47">
        <node concept="3clFbH" id="5J_TEGDjRl4" role="3cqZAp" />
        <node concept="3clFbJ" id="5J_TEGDjNoL" role="3cqZAp">
          <node concept="3clFbS" id="5J_TEGDjNoN" role="3clFbx">
            <node concept="3cpWs6" id="5J_TEGDjNQY" role="3cqZAp">
              <node concept="2YIFZM" id="5J_TEGDjNTL" role="3cqZAk">
                <ref role="37wK5l" to="o8zo:6t3ylNOzI9Y" resolve="forNamedElements" />
                <ref role="1Pybhc" to="o8zo:7ipADkTevLm" resolve="SimpleRoleScope" />
                <node concept="13iPFW" id="5J_TEGDjNVB" role="37wK5m" />
                <node concept="359W_D" id="5J_TEGDjOtG" role="37wK5m">
                  <ref role="359W_E" to="lehn:6porqNrmg18" resolve="Type" />
                  <ref role="359W_F" to="lehn:6porqNrmQ8g" resolve="fields" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="5J_TEGDjNxE" role="3clFbw">
            <node concept="37vLTw" id="5J_TEGDjNpj" role="2Oq$k0">
              <ref role="3cqZAo" node="5J_TEGDjNmM" resolve="kind" />
            </node>
            <node concept="2Zo12i" id="5J_TEGDjNLo" role="2OqNvi">
              <node concept="chp4Y" id="5J_TEGDjNNN" role="2Zo12j">
                <ref role="cht4Q" to="lehn:6porqNrmPQb" resolve="TypeField" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5J_TEGDjRnx" role="3cqZAp" />
        <node concept="3clFbJ" id="5J_TEGDjQei" role="3cqZAp">
          <node concept="3clFbS" id="5J_TEGDjQej" role="3clFbx">
            <node concept="3cpWs6" id="5J_TEGDjQek" role="3cqZAp">
              <node concept="2YIFZM" id="5J_TEGDjQel" role="3cqZAk">
                <ref role="37wK5l" to="o8zo:6t3ylNOzI9Y" resolve="forNamedElements" />
                <ref role="1Pybhc" to="o8zo:7ipADkTevLm" resolve="SimpleRoleScope" />
                <node concept="13iPFW" id="5J_TEGDjQem" role="37wK5m" />
                <node concept="359W_D" id="5J_TEGDjQen" role="37wK5m">
                  <ref role="359W_E" to="lehn:6porqNrmg18" resolve="Type" />
                  <ref role="359W_F" to="lehn:6porqNrmUr1" resolve="keys" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="5J_TEGDjQeo" role="3clFbw">
            <node concept="37vLTw" id="5J_TEGDjQep" role="2Oq$k0">
              <ref role="3cqZAo" node="5J_TEGDjNmM" resolve="kind" />
            </node>
            <node concept="2Zo12i" id="5J_TEGDjQeq" role="2OqNvi">
              <node concept="chp4Y" id="5J_TEGDjQkf" role="2Zo12j">
                <ref role="cht4Q" to="lehn:6porqNrmTAM" resolve="TypeKey" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5J_TEGDjRpZ" role="3cqZAp" />
        <node concept="3cpWs6" id="5J_TEGDjNnE" role="3cqZAp">
          <node concept="10Nm6u" id="5J_TEGDjNo7" role="3cqZAk" />
        </node>
      </node>
      <node concept="37vLTG" id="5J_TEGDjNmM" role="3clF46">
        <property role="TrG5h" value="kind" />
        <node concept="3bZ5Sz" id="5J_TEGDjNmN" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="5J_TEGDjNmO" role="3clF46">
        <property role="TrG5h" value="child" />
        <node concept="3Tqbb2" id="5J_TEGDjNmP" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="5J_TEGDjNmQ" role="3clF45">
        <ref role="3uigEE" to="o8zo:3fifI_xCtN$" resolve="Scope" />
      </node>
      <node concept="3Tm1VV" id="5J_TEGDjNmR" role="1B3o_S" />
    </node>
  </node>
  <node concept="13h7C7" id="5vpT9cKfgS">
    <property role="3GE5qa" value="service.method" />
    <ref role="13h7C2" to="lehn:1WjxkW0cEWa" resolve="ServiceMethodRef" />
    <node concept="13i0hz" id="5vpT9cKfh3" role="13h7CS">
      <property role="TrG5h" value="getScope" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="tpcu:52_Geb4QDV$" resolve="getScope" />
      <node concept="3clFbS" id="5vpT9cKfh4" role="3clF47">
        <node concept="3clFbH" id="5vpT9cKfh5" role="3cqZAp" />
        <node concept="3clFbJ" id="5vpT9cKfh6" role="3cqZAp">
          <node concept="3clFbS" id="5vpT9cKfh7" role="3clFbx">
            <node concept="3cpWs6" id="5vpT9cKfh8" role="3cqZAp">
              <node concept="2YIFZM" id="5vpT9cKfh9" role="3cqZAk">
                <ref role="37wK5l" to="o8zo:6t3ylNOzI9Y" resolve="forNamedElements" />
                <ref role="1Pybhc" to="o8zo:7ipADkTevLm" resolve="SimpleRoleScope" />
                <node concept="2OqwBi" id="5vpT9cKie7" role="37wK5m">
                  <node concept="13iPFW" id="5vpT9cKfha" role="2Oq$k0" />
                  <node concept="3TrEf2" id="5vpT9cKipy" role="2OqNvi">
                    <ref role="3Tt5mk" to="lehn:1WjxkW0cEWb" resolve="service" />
                  </node>
                </node>
                <node concept="359W_D" id="5vpT9cKfhb" role="37wK5m">
                  <ref role="359W_E" to="lehn:xAAq0nImMn" resolve="ServiceNode" />
                  <ref role="359W_F" to="lehn:xAAq0nImMD" resolve="methods" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="5vpT9cKfhc" role="3clFbw">
            <node concept="37vLTw" id="5vpT9cKfhd" role="2Oq$k0">
              <ref role="3cqZAo" node="5vpT9cKfhu" resolve="kind" />
            </node>
            <node concept="2Zo12i" id="5vpT9cKfhe" role="2OqNvi">
              <node concept="chp4Y" id="5vpT9cKi3P" role="2Zo12j">
                <ref role="cht4Q" to="lehn:xAAq0nImMq" resolve="ServiceMethod" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5vpT9cKfhr" role="3cqZAp" />
        <node concept="3cpWs6" id="5vpT9cKfhs" role="3cqZAp">
          <node concept="10Nm6u" id="5vpT9cKfht" role="3cqZAk" />
        </node>
      </node>
      <node concept="37vLTG" id="5vpT9cKfhu" role="3clF46">
        <property role="TrG5h" value="kind" />
        <node concept="3bZ5Sz" id="5vpT9cKfhv" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="5vpT9cKfhw" role="3clF46">
        <property role="TrG5h" value="child" />
        <node concept="3Tqbb2" id="5vpT9cKfhx" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="5vpT9cKfhy" role="3clF45">
        <ref role="3uigEE" to="o8zo:3fifI_xCtN$" resolve="Scope" />
      </node>
      <node concept="3Tm1VV" id="5vpT9cKfhz" role="1B3o_S" />
    </node>
    <node concept="13hLZK" id="5vpT9cKfgT" role="13h7CW">
      <node concept="3clFbS" id="5vpT9cKfgU" role="2VODD2" />
    </node>
  </node>
</model>

