<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:f00fd6de-b38e-411a-9521-a0b7f412e1e8(org.clawiz.core.common.language.structure)">
  <persistence version="9" />
  <languages>
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1082978164219" name="jetbrains.mps.lang.structure.structure.EnumerationDataTypeDeclaration" flags="ng" index="AxPO7">
        <property id="1212080844762" name="hasNoDefaultMember" index="PDuV0" />
        <property id="1212087449254" name="noValueText" index="Q2FuW" />
        <property id="1197591154882" name="memberIdentifierPolicy" index="3lZH7k" />
        <reference id="1083171729157" name="memberDataType" index="M4eZT" />
        <reference id="1083241965437" name="defaultMember" index="Qgau1" />
        <child id="1083172003582" name="member" index="M5hS2" />
      </concept>
      <concept id="1083171877298" name="jetbrains.mps.lang.structure.structure.EnumerationMemberDeclaration" flags="ig" index="M4N5e">
        <property id="1083923523172" name="externalValue" index="1uS6qo" />
        <property id="1083923523171" name="internalValue" index="1uS6qv" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="6porqNrmg18">
    <property role="TrG5h" value="Type" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Type" />
    <property role="3GE5qa" value="type" />
    <property role="EcuMT" value="7374764979001032776" />
    <node concept="1TJgyj" id="1yNWL2q$y8n" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="toStringKey" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="1780033530379772439" />
      <ref role="20lvS9" node="6porqNrmTAM" resolve="TypeKey" />
    </node>
    <node concept="1TJgyi" id="5teVQz_ANmG" role="1TKVEl">
      <property role="TrG5h" value="description" />
      <property role="IQ2nx" value="6291228963290953132" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="1bI9L5gdN_I" role="1TKVEl">
      <property role="TrG5h" value="install" />
      <property role="IQ2nx" value="1364070692453562734" />
      <ref role="AX2Wp" node="7YEFpv2ByYp" resolve="BooleanDefaultTrueEnumeration" />
    </node>
    <node concept="1TJgyi" id="44sHdQFnLvW" role="1TKVEl">
      <property role="TrG5h" value="tableName" />
      <property role="IQ2nx" value="4691823775969122300" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="2s9otCCoXIJ" role="1TKVEl">
      <property role="TrG5h" value="javaName" />
      <property role="IQ2nx" value="2812887031877983151" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="6porqNrmQ8g" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="fields" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="7374764979001188880" />
      <ref role="20lvS9" node="6porqNrmPQb" resolve="TypeField" />
    </node>
    <node concept="1TJgyj" id="6porqNrmUr1" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="keys" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="7374764979001206465" />
      <ref role="20lvS9" node="6porqNrmTAM" resolve="TypeKey" />
    </node>
    <node concept="1TJgyj" id="6V$9YeXALQW" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="childs" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="7990555497954221500" />
      <ref role="20lvS9" node="6porqNrmg18" resolve="Type" />
    </node>
    <node concept="1TJgyj" id="7ARNxauA7LX" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="properties" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="8770705378692856957" />
      <ref role="20lvS9" node="7ARNxauA7LI" resolve="AbstractTypeProperty" />
    </node>
    <node concept="1TJgyj" id="7_Cm_A34H0x" role="1TKVEi">
      <property role="IQ2ns" value="8748341616664825889" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="serviceMethods" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="yAB$tbwYhX" resolve="TypeMethod" />
    </node>
    <node concept="PrWs8" id="7YEFpv2ByYk" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="IcIQHTw7iB" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="6porqNrmPx8">
    <property role="TrG5h" value="AbstractValueType" />
    <property role="3GE5qa" value="common.valuetype" />
    <property role="R5$K7" value="true" />
    <property role="EcuMT" value="7374764979001186376" />
  </node>
  <node concept="1TIwiD" id="6porqNrmPQb">
    <property role="TrG5h" value="TypeField" />
    <property role="3GE5qa" value="type.field" />
    <property role="EcuMT" value="7374764979001187723" />
    <node concept="1TJgyi" id="5teVQz_ANmH" role="1TKVEl">
      <property role="TrG5h" value="description" />
      <property role="IQ2nx" value="6291228963290953133" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="6porqNrmPQf" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="valueType" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="7374764979001187727" />
      <ref role="20lvS9" node="6porqNrmPx8" resolve="AbstractValueType" />
    </node>
    <node concept="1TJgyj" id="3ohFwV2ui1Z" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="properties" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="3896086531067682943" />
      <ref role="20lvS9" node="3ohFwV2ui1V" resolve="AbstractTypeFieldProperty" />
    </node>
    <node concept="PrWs8" id="7YEFpv2ByYN" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6porqNrmPYz">
    <property role="3GE5qa" value="common.valuetype" />
    <property role="TrG5h" value="ValueTypeNumber" />
    <property role="34LRSv" value="number" />
    <property role="EcuMT" value="7374764979001188259" />
    <ref role="1TJDcQ" node="6porqNrmPx8" resolve="AbstractValueType" />
    <node concept="1TJgyi" id="6porqNrmSHw" role="1TKVEl">
      <property role="TrG5h" value="precision" />
      <property role="IQ2nx" value="7374764979001199456" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="6porqNrmSHx" role="1TKVEl">
      <property role="TrG5h" value="scale" />
      <property role="IQ2nx" value="7374764979001199457" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="6porqNrmQ7A">
    <property role="3GE5qa" value="common.valuetype" />
    <property role="TrG5h" value="ValueTypeDate" />
    <property role="34LRSv" value="date" />
    <property role="EcuMT" value="7374764979001188838" />
    <ref role="1TJDcQ" node="6porqNrmPx8" resolve="AbstractValueType" />
  </node>
  <node concept="1TIwiD" id="6porqNrmQ8b">
    <property role="3GE5qa" value="common.valuetype" />
    <property role="TrG5h" value="ValueTypeString" />
    <property role="34LRSv" value="string" />
    <property role="EcuMT" value="7374764979001188875" />
    <ref role="1TJDcQ" node="6porqNrmPx8" resolve="AbstractValueType" />
    <node concept="1TJgyi" id="6porqNrmRYT" role="1TKVEl">
      <property role="TrG5h" value="length" />
      <property role="IQ2nx" value="7374764979001196473" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="6porqNrmSHv">
    <property role="3GE5qa" value="common.valuetype" />
    <property role="TrG5h" value="ValueTypeScaledNumber" />
    <property role="34LRSv" value="number(,)" />
    <property role="EcuMT" value="7374764979001199455" />
    <ref role="1TJDcQ" node="6porqNrmPYz" resolve="ValueTypeNumber" />
  </node>
  <node concept="1TIwiD" id="6porqNrmTAM">
    <property role="TrG5h" value="TypeKey" />
    <property role="3GE5qa" value="type.key" />
    <property role="EcuMT" value="7374764979001203122" />
    <node concept="1TJgyj" id="6porqNrmUqI" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="fields" />
      <property role="20lbJX" value="1..n" />
      <property role="IQ2ns" value="7374764979001206446" />
      <ref role="20lvS9" node="6porqNrmTAS" resolve="TypeKeyField" />
    </node>
    <node concept="1TJgyi" id="6porqNrmTAO" role="1TKVEl">
      <property role="TrG5h" value="unique" />
      <property role="IQ2nx" value="7374764979001203124" />
      <ref role="AX2Wp" node="7YEFpv2ByYH" resolve="BooleanDefaultFalseEnumeration" />
    </node>
    <node concept="PrWs8" id="7YEFpv2ByYE" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6porqNrmTAS">
    <property role="3GE5qa" value="type.key" />
    <property role="TrG5h" value="TypeKeyField" />
    <property role="EcuMT" value="7374764979001203128" />
    <node concept="1TJgyj" id="6porqNrmTAU" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="field" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="7374764979001203130" />
      <ref role="20lvS9" node="6porqNrmPQb" resolve="TypeField" />
    </node>
    <node concept="PrWs8" id="7YEFpv2ByYK" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="1bI9L5gdW_Z">
    <property role="3GE5qa" value="common.valuetype" />
    <property role="TrG5h" value="ValueTypeObject" />
    <property role="34LRSv" value="-&gt;" />
    <property role="EcuMT" value="1364070692453599615" />
    <ref role="1TJDcQ" node="6porqNrmPx8" resolve="AbstractValueType" />
    <node concept="1TJgyj" id="1bI9L5gdWA1" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="referencedType" />
      <property role="20lbJX" value="0..1" />
      <property role="IQ2ns" value="1364070692453599617" />
      <ref role="20lvS9" node="6porqNrmg18" resolve="Type" />
    </node>
  </node>
  <node concept="1TIwiD" id="4WUZoVQo97Y">
    <property role="3GE5qa" value="common.valuetype" />
    <property role="TrG5h" value="ValueTypeDateTime" />
    <property role="34LRSv" value="datetime" />
    <property role="EcuMT" value="5709154268106232318" />
    <ref role="1TJDcQ" node="6porqNrmPx8" resolve="AbstractValueType" />
  </node>
  <node concept="1TIwiD" id="7ARNxauA7LI">
    <property role="TrG5h" value="AbstractTypeProperty" />
    <property role="3GE5qa" value="type.property" />
    <property role="R5$K7" value="true" />
    <property role="EcuMT" value="8770705378692856942" />
  </node>
  <node concept="1TIwiD" id="3ohFwV2ui1T">
    <property role="TrG5h" value="DefaultValueTypeFieldProperty" />
    <property role="3GE5qa" value="type.field.property.defaultvalue" />
    <property role="R5$K7" value="false" />
    <property role="EcuMT" value="3896086531067682937" />
    <property role="34LRSv" value="default value" />
    <ref role="1TJDcQ" node="3ohFwV2ui1V" resolve="AbstractTypeFieldProperty" />
  </node>
  <node concept="1TIwiD" id="3ohFwV2ui1V">
    <property role="3GE5qa" value="type.field.property" />
    <property role="TrG5h" value="AbstractTypeFieldProperty" />
    <property role="R5$K7" value="true" />
    <property role="EcuMT" value="3896086531067682939" />
  </node>
  <node concept="1TIwiD" id="2LNTlDxV0Js">
    <property role="3GE5qa" value="common.valuetype" />
    <property role="TrG5h" value="ValueTypeEnumeration" />
    <property role="34LRSv" value="enum" />
    <property role="EcuMT" value="3203155936343231452" />
    <ref role="1TJDcQ" node="6porqNrmPx8" resolve="AbstractValueType" />
    <node concept="1TJgyj" id="2LNTlDxV3cL" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="values" />
      <property role="20lbJX" value="1..n" />
      <property role="IQ2ns" value="3203155936343241521" />
      <ref role="20lvS9" node="2LNTlDxV3ct" resolve="ValueTypeEnumerationValue" />
    </node>
  </node>
  <node concept="1TIwiD" id="2LNTlDxV3ct">
    <property role="3GE5qa" value="common.valuetype" />
    <property role="TrG5h" value="ValueTypeEnumerationValue" />
    <property role="EcuMT" value="3203155936343241501" />
    <node concept="1TJgyi" id="2LNTlDxV3cx" role="1TKVEl">
      <property role="TrG5h" value="description" />
      <property role="IQ2nx" value="3203155936343241505" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="PrWs8" id="7YEFpv2B$d0" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="2j4WJkiqPZ9">
    <property role="3GE5qa" value="common.valuetype" />
    <property role="TrG5h" value="ValueTypeBoolean" />
    <property role="34LRSv" value="logical" />
    <property role="EcuMT" value="2649509615190630345" />
    <ref role="1TJDcQ" node="6porqNrmPx8" resolve="AbstractValueType" />
  </node>
  <node concept="1TIwiD" id="2nKQUcI4FD_">
    <property role="3GE5qa" value="common.valuetype" />
    <property role="TrG5h" value="ValueTypeBLOB" />
    <property role="34LRSv" value="BLOB" />
    <property role="EcuMT" value="2733926467713022565" />
    <ref role="1TJDcQ" node="6porqNrmPx8" resolve="AbstractValueType" />
  </node>
  <node concept="1TIwiD" id="2nKQUcI4FDK">
    <property role="3GE5qa" value="common.valuetype" />
    <property role="TrG5h" value="ValueTypeText" />
    <property role="34LRSv" value="text" />
    <property role="EcuMT" value="2733926467713022576" />
    <ref role="1TJDcQ" node="6porqNrmPx8" resolve="AbstractValueType" />
  </node>
  <node concept="1TIwiD" id="1yNWL2qySRY">
    <property role="3GE5qa" value="type.field.property.validator" />
    <property role="TrG5h" value="RequiredTypeFieldProperty" />
    <property role="34LRSv" value="required" />
    <property role="EcuMT" value="1780033530379341310" />
    <ref role="1TJDcQ" node="3ohFwV2ui1V" resolve="AbstractTypeFieldProperty" />
  </node>
  <node concept="1TIwiD" id="xAAq0nImMn">
    <property role="TrG5h" value="ServiceNode" />
    <property role="19KtqR" value="true" />
    <property role="3GE5qa" value="service" />
    <property role="34LRSv" value="Service" />
    <property role="EcuMT" value="605340112799755415" />
    <node concept="1TJgyj" id="xAAq0nImMD" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="methods" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="605340112799755433" />
      <ref role="20lvS9" node="xAAq0nImMq" resolve="ServiceMethod" />
    </node>
    <node concept="PrWs8" id="7YEFpv2ByY7" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="xAAq0nImMq">
    <property role="3GE5qa" value="service.method" />
    <property role="TrG5h" value="ServiceMethod" />
    <property role="EcuMT" value="605340112799755418" />
    <node concept="1TJgyj" id="7y$dPqeESH_" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="valueType" />
      <property role="20lbJX" value="0..1" />
      <property role="IQ2ns" value="8693134025632222053" />
      <ref role="20lvS9" node="32y5cLH3_eq" resolve="AbstractServiceMethodValueType" />
    </node>
    <node concept="1TJgyj" id="32y5cLH3lGl" role="1TKVEi">
      <property role="IQ2ns" value="3504386328283142933" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="parameters" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="32y5cLH3e39" resolve="ServiceMethodParameter" />
    </node>
    <node concept="1TJgyj" id="32y5cLH3lI0" role="1TKVEi">
      <property role="IQ2ns" value="3504386328283143040" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="statements" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="32y5cLH34n4" resolve="AbstractMethodStatement" />
    </node>
    <node concept="PrWs8" id="7YEFpv2ByYh" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="1WjxkW0cEWa">
    <property role="3GE5qa" value="service.method" />
    <property role="TrG5h" value="ServiceMethodRef" />
    <property role="34LRSv" value="service method" />
    <property role="EcuMT" value="2239280014085500682" />
    <node concept="1TJgyj" id="1WjxkW0cEWb" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="service" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="2239280014085500683" />
      <ref role="20lvS9" node="xAAq0nImMn" resolve="ServiceNode" />
    </node>
    <node concept="1TJgyj" id="1WjxkW0cEWc" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="method" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="2239280014085500684" />
      <ref role="20lvS9" node="xAAq0nImMq" resolve="ServiceMethod" />
    </node>
    <node concept="PrWs8" id="1WjxkW0cJST" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
  <node concept="AxPO7" id="7YEFpv2ByYp">
    <property role="3GE5qa" value="common.enumeration" />
    <property role="TrG5h" value="BooleanDefaultTrueEnumeration" />
    <property role="PDuV0" value="false" />
    <property role="3lZH7k" value="derive_from_internal_value" />
    <property role="Q2FuW" value="true" />
    <ref role="M4eZT" to="tpck:fKAQMTB" resolve="boolean" />
    <ref role="Qgau1" node="7YEFpv2ByYq" />
    <node concept="M4N5e" id="7YEFpv2ByYq" role="M5hS2">
      <property role="1uS6qv" value="true" />
      <property role="1uS6qo" value="true" />
    </node>
    <node concept="M4N5e" id="7YEFpv2ByYr" role="M5hS2">
      <property role="1uS6qv" value="false" />
      <property role="1uS6qo" value="false" />
    </node>
  </node>
  <node concept="AxPO7" id="7YEFpv2ByYH">
    <property role="3GE5qa" value="common.enumeration" />
    <property role="TrG5h" value="BooleanDefaultFalseEnumeration" />
    <property role="PDuV0" value="false" />
    <property role="3lZH7k" value="derive_from_internal_value" />
    <property role="Q2FuW" value="false" />
    <ref role="M4eZT" to="tpck:fKAQMTB" resolve="boolean" />
    <ref role="Qgau1" node="7YEFpv2ByYJ" />
    <node concept="M4N5e" id="7YEFpv2ByYI" role="M5hS2">
      <property role="1uS6qv" value="true" />
      <property role="1uS6qo" value="true" />
    </node>
    <node concept="M4N5e" id="7YEFpv2ByYJ" role="M5hS2">
      <property role="1uS6qv" value="false" />
      <property role="1uS6qo" value="false" />
    </node>
  </node>
  <node concept="1TIwiD" id="28N7VjDcBiJ">
    <property role="EcuMT" value="2464348282968765615" />
    <property role="3GE5qa" value="type.field.property.database" />
    <property role="TrG5h" value="TableColumnTypeFieldProperty" />
    <property role="34LRSv" value="table column" />
    <ref role="1TJDcQ" node="3ohFwV2ui1V" resolve="AbstractTypeFieldProperty" />
    <node concept="1TJgyi" id="28N7VjDcBiK" role="1TKVEl">
      <property role="IQ2nx" value="2464348282968765616" />
      <property role="TrG5h" value="columnName" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="2jHeDCe3qHR">
    <property role="EcuMT" value="2660847388180982647" />
    <property role="3GE5qa" value="type.property.extension" />
    <property role="TrG5h" value="AbstractExtensionTypeProperty" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="7ARNxauA7LI" resolve="AbstractTypeProperty" />
    <node concept="1TJgyj" id="2jHeDCe3qIz" role="1TKVEi">
      <property role="IQ2ns" value="2660847388180982691" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="method" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2jHeDCe3qIs" resolve="AbstractExtensionTypePropertyMethod" />
    </node>
    <node concept="1TJgyj" id="2jHeDCe3qJq" role="1TKVEi">
      <property role="IQ2ns" value="2660847388180982746" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="action" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2jHeDCe3qJ1" resolve="AbstractExtensionTypePropertyAction" />
    </node>
  </node>
  <node concept="1TIwiD" id="2jHeDCe3qI2">
    <property role="EcuMT" value="2660847388180982658" />
    <property role="3GE5qa" value="type.property.extension" />
    <property role="TrG5h" value="BeforeExtensionTypeProperty" />
    <property role="34LRSv" value="before" />
    <ref role="1TJDcQ" node="2jHeDCe3qHR" resolve="AbstractExtensionTypeProperty" />
  </node>
  <node concept="1TIwiD" id="2jHeDCe3qI3">
    <property role="EcuMT" value="2660847388180982659" />
    <property role="3GE5qa" value="type.property.extension" />
    <property role="TrG5h" value="AfterExtensionTypeProperty" />
    <property role="34LRSv" value="after" />
    <ref role="1TJDcQ" node="2jHeDCe3qHR" resolve="AbstractExtensionTypeProperty" />
  </node>
  <node concept="1TIwiD" id="2jHeDCe3qIs">
    <property role="EcuMT" value="2660847388180982684" />
    <property role="3GE5qa" value="type.property.extension.method" />
    <property role="TrG5h" value="AbstractExtensionTypePropertyMethod" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="2jHeDCe3qIv">
    <property role="EcuMT" value="2660847388180982687" />
    <property role="3GE5qa" value="type.property.extension.method" />
    <property role="TrG5h" value="CheckExtensionTypePropertyMethod" />
    <property role="34LRSv" value="check" />
    <ref role="1TJDcQ" node="2jHeDCe3qIs" resolve="AbstractExtensionTypePropertyMethod" />
  </node>
  <node concept="1TIwiD" id="2jHeDCe3qIw">
    <property role="EcuMT" value="2660847388180982688" />
    <property role="3GE5qa" value="type.property.extension.method" />
    <property role="TrG5h" value="SaveExtensionTypePropertyMethod" />
    <property role="34LRSv" value="save" />
    <ref role="1TJDcQ" node="2jHeDCe3qIs" resolve="AbstractExtensionTypePropertyMethod" />
  </node>
  <node concept="1TIwiD" id="2jHeDCe3qIx">
    <property role="EcuMT" value="2660847388180982689" />
    <property role="3GE5qa" value="type.property.extension.method" />
    <property role="TrG5h" value="DeleteExtensionTypePropertyMethod" />
    <property role="34LRSv" value="delete" />
    <ref role="1TJDcQ" node="2jHeDCe3qIs" resolve="AbstractExtensionTypePropertyMethod" />
  </node>
  <node concept="1TIwiD" id="2jHeDCe3qIy">
    <property role="EcuMT" value="2660847388180982690" />
    <property role="3GE5qa" value="type.property.extension.method" />
    <property role="TrG5h" value="CreateExtensionTypePropertyMethod" />
    <property role="34LRSv" value="create" />
    <ref role="1TJDcQ" node="2jHeDCe3qIs" resolve="AbstractExtensionTypePropertyMethod" />
  </node>
  <node concept="1TIwiD" id="2jHeDCe3qJ1">
    <property role="EcuMT" value="2660847388180982721" />
    <property role="3GE5qa" value="type.property.extension.action" />
    <property role="TrG5h" value="AbstractExtensionTypePropertyAction" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="2jHeDCe3qJ2">
    <property role="EcuMT" value="2660847388180982722" />
    <property role="3GE5qa" value="type.property.extension.action" />
    <property role="TrG5h" value="CallServiceMethodExtensionTypePropertyAction" />
    <property role="34LRSv" value="call service method" />
    <ref role="1TJDcQ" node="2jHeDCe3qJ1" resolve="AbstractExtensionTypePropertyAction" />
    <node concept="1TJgyj" id="2jHeDCe3qJh" role="1TKVEi">
      <property role="IQ2ns" value="2660847388180982737" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="serviceMethod" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1WjxkW0cEWa" resolve="ServiceMethodRef" />
    </node>
  </node>
  <node concept="1TIwiD" id="3kI_owliyBM">
    <property role="3GE5qa" value="common.valuetype" />
    <property role="TrG5h" value="ValueTypeList" />
    <property role="34LRSv" value="[]" />
    <property role="EcuMT" value="3832164744644143602" />
    <ref role="1TJDcQ" node="6porqNrmPx8" resolve="AbstractValueType" />
    <node concept="1TJgyj" id="3kI_owliyBN" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="referencedType" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="3832164744644143603" />
      <ref role="20lvS9" node="6porqNrmg18" resolve="Type" />
    </node>
  </node>
  <node concept="1TIwiD" id="7rpCSfHG61T">
    <property role="EcuMT" value="8564055953445511289" />
    <property role="TrG5h" value="Role" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Role" />
    <property role="3GE5qa" value="security.role" />
    <node concept="PrWs8" id="7rpCSfHG61U" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="7rpCSfHGTUU" role="1TKVEl">
      <property role="IQ2nx" value="8564055953445723834" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="7rpCSfHG625">
    <property role="EcuMT" value="8564055953445511301" />
    <property role="3GE5qa" value="security.permission" />
    <property role="TrG5h" value="PermissionType" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Permission" />
    <node concept="PrWs8" id="7rpCSfHG626" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="7rpCSfHGTTM" role="1TKVEl">
      <property role="IQ2nx" value="8564055953445723762" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="72ziUYArkGu">
    <property role="EcuMT" value="8116414171235306270" />
    <property role="3GE5qa" value="type.condition" />
    <property role="TrG5h" value="AbstractTypeCondition" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="72ziUYArkGv">
    <property role="EcuMT" value="8116414171235306271" />
    <property role="3GE5qa" value="type.property.condition" />
    <property role="TrG5h" value="AbstractConditionTypeProperty" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="7ARNxauA7LI" resolve="AbstractTypeProperty" />
    <node concept="1TJgyj" id="72ziUYArkG_" role="1TKVEi">
      <property role="IQ2ns" value="8116414171235306277" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="condition" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="72ziUYArkGu" resolve="AbstractTypeCondition" />
    </node>
  </node>
  <node concept="1TIwiD" id="72ziUYArkGy">
    <property role="EcuMT" value="8116414171235306274" />
    <property role="3GE5qa" value="type.property.condition" />
    <property role="TrG5h" value="ChangeableIfConditionTypeProperty" />
    <property role="34LRSv" value="changeable if" />
    <ref role="1TJDcQ" node="72ziUYArkGv" resolve="AbstractConditionTypeProperty" />
  </node>
  <node concept="1TIwiD" id="72ziUYArkGT">
    <property role="EcuMT" value="8116414171235306297" />
    <property role="3GE5qa" value="type.property.condition" />
    <property role="TrG5h" value="VisibleIfConditionTypeProperty" />
    <property role="34LRSv" value="visible if" />
    <ref role="1TJDcQ" node="72ziUYArkGv" resolve="AbstractConditionTypeProperty" />
  </node>
  <node concept="1TIwiD" id="72ziUYArkHd">
    <property role="EcuMT" value="8116414171235306317" />
    <property role="3GE5qa" value="type.property.condition" />
    <property role="TrG5h" value="HiddenIfConditionTypeProperty" />
    <property role="34LRSv" value="hidden if" />
    <ref role="1TJDcQ" node="72ziUYArkGv" resolve="AbstractConditionTypeProperty" />
  </node>
  <node concept="1TIwiD" id="72ziUYArkH_">
    <property role="EcuMT" value="8116414171235306341" />
    <property role="3GE5qa" value="type.property.condition" />
    <property role="TrG5h" value="ReadOnlyIfConditionTypeProperty" />
    <property role="34LRSv" value="readonly if" />
    <ref role="1TJDcQ" node="72ziUYArkGv" resolve="AbstractConditionTypeProperty" />
  </node>
  <node concept="1TIwiD" id="72ziUYArkHT">
    <property role="EcuMT" value="8116414171235306361" />
    <property role="3GE5qa" value="type.field.property.condition" />
    <property role="TrG5h" value="AbstractConditionTypeFieldProperty" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="3ohFwV2ui1V" resolve="AbstractTypeFieldProperty" />
    <node concept="1TJgyj" id="72ziUYArkHW" role="1TKVEi">
      <property role="IQ2ns" value="8116414171235306364" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="condition" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="72ziUYArkGu" resolve="AbstractTypeCondition" />
    </node>
  </node>
  <node concept="1TIwiD" id="72ziUYArkI0">
    <property role="EcuMT" value="8116414171235306368" />
    <property role="3GE5qa" value="type.field.property.condition" />
    <property role="TrG5h" value="ChangeableIfConditionTypeFieldProperty" />
    <property role="34LRSv" value="changeable if" />
    <ref role="1TJDcQ" node="72ziUYArkHT" resolve="AbstractConditionTypeFieldProperty" />
  </node>
  <node concept="1TIwiD" id="72ziUYArkI1">
    <property role="EcuMT" value="8116414171235306369" />
    <property role="3GE5qa" value="type.field.property.condition" />
    <property role="TrG5h" value="VisibleIfConditionTypeFieldProperty" />
    <property role="34LRSv" value="visible if" />
    <ref role="1TJDcQ" node="72ziUYArkHT" resolve="AbstractConditionTypeFieldProperty" />
  </node>
  <node concept="1TIwiD" id="72ziUYArkI2">
    <property role="EcuMT" value="8116414171235306370" />
    <property role="3GE5qa" value="type.field.property.condition" />
    <property role="TrG5h" value="HiddenIfConditionTypeFieldProperty" />
    <property role="34LRSv" value="hidden if" />
    <ref role="1TJDcQ" node="72ziUYArkHT" resolve="AbstractConditionTypeFieldProperty" />
  </node>
  <node concept="1TIwiD" id="72ziUYArkI3">
    <property role="EcuMT" value="8116414171235306371" />
    <property role="3GE5qa" value="type.field.property.condition" />
    <property role="TrG5h" value="ReadOnlyIfConditionTypeFieldProperty" />
    <property role="34LRSv" value="readonly if" />
    <ref role="1TJDcQ" node="72ziUYArkHT" resolve="AbstractConditionTypeFieldProperty" />
  </node>
  <node concept="1TIwiD" id="72ziUYArkJg">
    <property role="EcuMT" value="8116414171235306448" />
    <property role="3GE5qa" value="type.condition" />
    <property role="TrG5h" value="RoleEqualTypeCondition" />
    <property role="34LRSv" value="role =" />
    <ref role="1TJDcQ" node="72ziUYArkGu" resolve="AbstractTypeCondition" />
    <node concept="1TJgyj" id="385iqfzvxiy" role="1TKVEi">
      <property role="IQ2ns" value="3604368045025137826" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="roles" />
      <property role="20lbJX" value="1..n" />
      <ref role="20lvS9" node="385iqfzvxi2" resolve="RoleRef" />
    </node>
  </node>
  <node concept="1TIwiD" id="72ziUYArkJR">
    <property role="EcuMT" value="8116414171235306487" />
    <property role="3GE5qa" value="type.condition" />
    <property role="TrG5h" value="RoleNotEqualTypeCondition" />
    <property role="34LRSv" value="role !=" />
    <ref role="1TJDcQ" node="72ziUYArkGu" resolve="AbstractTypeCondition" />
    <node concept="1TJgyj" id="385iqfzvxiW" role="1TKVEi">
      <property role="IQ2ns" value="3604368045025137852" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="roles" />
      <property role="20lbJX" value="1..n" />
      <ref role="20lvS9" node="385iqfzvxi2" resolve="RoleRef" />
    </node>
  </node>
  <node concept="1TIwiD" id="385iqfzvxi2">
    <property role="EcuMT" value="3604368045025137794" />
    <property role="3GE5qa" value="security.role" />
    <property role="TrG5h" value="RoleRef" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="385iqfzvxi3" role="1TKVEi">
      <property role="IQ2ns" value="3604368045025137795" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="role" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="7rpCSfHG61T" resolve="Role" />
    </node>
  </node>
  <node concept="1TIwiD" id="yAB$tbwYhU">
    <property role="EcuMT" value="623359627484193914" />
    <property role="3GE5qa" value="type.method.action" />
    <property role="TrG5h" value="AbstractTypeMethodAction" />
    <property role="R5$K7" value="true" />
  </node>
  <node concept="1TIwiD" id="yAB$tbwYhX">
    <property role="EcuMT" value="623359627484193917" />
    <property role="3GE5qa" value="type.method" />
    <property role="TrG5h" value="TypeMethod" />
    <ref role="1TJDcQ" node="xAAq0nImMq" resolve="ServiceMethod" />
  </node>
  <node concept="1TIwiD" id="32y5cLH34n4">
    <property role="EcuMT" value="3504386328283071940" />
    <property role="3GE5qa" value="service.method.statement" />
    <property role="TrG5h" value="AbstractMethodStatement" />
    <property role="R5$K7" value="true" />
    <node concept="PrWs8" id="32y5cLH34n5" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="32y5cLH3e39">
    <property role="EcuMT" value="3504386328283111625" />
    <property role="3GE5qa" value="service.method.parameter" />
    <property role="TrG5h" value="ServiceMethodParameter" />
    <node concept="1TJgyj" id="32y5cLH3e3c" role="1TKVEi">
      <property role="IQ2ns" value="3504386328283111628" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="valueType" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="32y5cLH3_eq" resolve="AbstractServiceMethodValueType" />
    </node>
    <node concept="PrWs8" id="32y5cLH3e3a" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="32y5cLH3ti7">
    <property role="EcuMT" value="3504386328283174023" />
    <property role="TrG5h" value="Table" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Table" />
    <property role="3GE5qa" value="database.table" />
    <node concept="PrWs8" id="32y5cLH3ti8" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyj" id="32y5cLH3tid" role="1TKVEi">
      <property role="IQ2ns" value="3504386328283174029" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="columns" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="32y5cLH3tia" resolve="TableColumn" />
    </node>
  </node>
  <node concept="1TIwiD" id="32y5cLH3tia">
    <property role="EcuMT" value="3504386328283174026" />
    <property role="3GE5qa" value="database.table.column" />
    <property role="TrG5h" value="TableColumn" />
    <node concept="1TJgyj" id="32y5cLH3tjZ" role="1TKVEi">
      <property role="IQ2ns" value="3504386328283174143" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="type" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="32y5cLH3tih" resolve="AbstractTableColumnType" />
    </node>
    <node concept="PrWs8" id="32y5cLH3tib" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="32y5cLH3tih">
    <property role="EcuMT" value="3504386328283174033" />
    <property role="3GE5qa" value="database.table.column.type" />
    <property role="TrG5h" value="AbstractTableColumnType" />
    <property role="R5$K7" value="true" />
    <node concept="PrWs8" id="32y5cLH3tik" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="32y5cLH3tk1">
    <property role="EcuMT" value="3504386328283174145" />
    <property role="3GE5qa" value="database.table.column.type" />
    <property role="TrG5h" value="VarcharTableColumnType" />
    <property role="34LRSv" value="varchar" />
    <ref role="1TJDcQ" node="32y5cLH3tih" resolve="AbstractTableColumnType" />
    <node concept="1TJgyi" id="32y5cLH3tk2" role="1TKVEl">
      <property role="IQ2nx" value="3504386328283174146" />
      <property role="TrG5h" value="length" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="32y5cLH3_eq">
    <property role="EcuMT" value="3504386328283206554" />
    <property role="3GE5qa" value="service.method.valuetype" />
    <property role="TrG5h" value="AbstractServiceMethodValueType" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="32y5cLH3_eu">
    <property role="EcuMT" value="3504386328283206558" />
    <property role="3GE5qa" value="service.method.valuetype" />
    <property role="TrG5h" value="ServiceMethodValueTypeNumber" />
    <property role="34LRSv" value="number" />
    <ref role="1TJDcQ" node="32y5cLH3_eq" resolve="AbstractServiceMethodValueType" />
  </node>
  <node concept="1TIwiD" id="32y5cLH3_eJ">
    <property role="EcuMT" value="3504386328283206575" />
    <property role="3GE5qa" value="service.method.valuetype" />
    <property role="TrG5h" value="ServiceMethodValueTypeString" />
    <property role="34LRSv" value="string" />
    <ref role="1TJDcQ" node="32y5cLH3_eq" resolve="AbstractServiceMethodValueType" />
  </node>
</model>

