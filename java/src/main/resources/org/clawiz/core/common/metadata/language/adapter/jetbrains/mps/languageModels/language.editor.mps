<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:c0c60d55-c3f8-4481-9d76-0a97b644852c(org.clawiz.core.common.language.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
  </languages>
  <imports>
    <import index="lehn" ref="r:f00fd6de-b38e-411a-9521-a0b7f412e1e8(org.clawiz.core.common.language.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi">
        <child id="1078153129734" name="inspectedCellModel" index="6VMZX" />
      </concept>
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <property id="1140524450557" name="separatorText" index="2czwfO" />
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1239814640496" name="jetbrains.mps.lang.editor.structure.CellLayout_VerticalGrid" flags="nn" index="2EHx9g" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1186414976055" name="jetbrains.mps.lang.editor.structure.DrawBorderStyleClassItem" flags="ln" index="VPXOz" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1214560368769" name="emptyNoTargetText" index="39s7Ar" />
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <property id="1140114345053" name="allowEmptyText" index="1O74Pk" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="6porqNrmnn5">
    <property role="3GE5qa" value="type" />
    <ref role="1XX52x" to="lehn:6porqNrmg18" resolve="Type" />
    <node concept="3EZMnI" id="6porqNrmTvQ" role="2wV5jI">
      <node concept="l2Vlx" id="6porqNrmTvR" role="2iSdaV" />
      <node concept="3F0ifn" id="6porqNrmTvV" role="3EZMnx">
        <property role="3F0ifm" value="Type" />
      </node>
      <node concept="3F0A7n" id="6porqNrmTvX" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="6porqNrmUs6" role="3EZMnx">
        <property role="3F0ifm" value="       table       " />
        <node concept="pVoyu" id="6porqNrmUs7" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="qWeEqWXD1t" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:44sHdQFnLvW" resolve="tableName" />
      </node>
      <node concept="3F0A7n" id="qWeEqWXD35" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="lehn:2s9otCCoXIJ" resolve="javaName" />
      </node>
      <node concept="3F0ifn" id="qWeEqWXCYi" role="3EZMnx">
        <property role="3F0ifm" value="       description " />
        <node concept="pVoyu" id="qWeEqWXCYj" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="qWeEqWXCXz" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="lehn:5teVQz_ANmG" resolve="description" />
      </node>
      <node concept="3F0ifn" id="qWeEqWXCSy" role="3EZMnx">
        <node concept="pVoyu" id="qWeEqWXCSz" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="1yNWL2q$y8t" role="3EZMnx">
        <node concept="pVoyu" id="1yNWL2q$y8u" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6porqNrmUs5" role="3EZMnx">
        <property role="3F0ifm" value="fields" />
        <node concept="pVoyu" id="6porqNrmUs8" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="1yNWL2q$y9N" role="3EZMnx">
        <node concept="pVoyu" id="1yNWL2q$y9O" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="6porqNrmTvZ" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:6porqNrmQ8g" resolve="fields" />
        <node concept="2EHx9g" id="3tzS5_SWx7G" role="2czzBx" />
        <node concept="lj46D" id="6porqNrmTw3" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="6porqNrmTw5" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6porqNrmUrQ" role="3EZMnx">
        <node concept="pVoyu" id="6porqNrmUrY" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6porqNrmUrV" role="3EZMnx">
        <property role="3F0ifm" value="keys" />
        <node concept="pVoyu" id="6porqNrmUrZ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="1yNWL2q$yay" role="3EZMnx">
        <node concept="pVoyu" id="1yNWL2q$yaz" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="6porqNrmZ1w" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:6porqNrmUr1" resolve="keys" />
        <node concept="2EHx9g" id="3tzS5_SWx7L" role="2czzBx" />
        <node concept="lj46D" id="6porqNrmZ1$" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="6porqNrmZ1A" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="1yNWL2q$yc6" role="3EZMnx">
        <node concept="pVoyu" id="1yNWL2q$yc7" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="1yNWL2q$ybj" role="3EZMnx">
        <property role="3F0ifm" value="  toString :" />
        <node concept="pVoyu" id="1yNWL2q$ybk" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="1iCGBv" id="1yNWL2q$yd$" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:1yNWL2q$y8n" resolve="toStringKey" />
        <node concept="1sVBvm" id="1yNWL2q$ydA" role="1sWHZn">
          <node concept="3F0A7n" id="1yNWL2q$yeq" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <property role="1O74Pk" value="true" />
            <property role="39s7Ar" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="6V$9YeXAgLn" role="3EZMnx">
        <node concept="pVoyu" id="6V$9YeXAgTt" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="7_Cm_A34H1q" role="3EZMnx">
        <property role="3F0ifm" value="properties      " />
        <node concept="pVoyu" id="7_Cm_A34H1r" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="7ARNxauA7Mc" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:7ARNxauA7LX" resolve="properties" />
        <node concept="2EHx9g" id="3tzS5_SWx7K" role="2czzBx" />
      </node>
      <node concept="3F0ifn" id="7_Cm_A34H5q" role="3EZMnx">
        <node concept="pVoyu" id="7_Cm_A34H5r" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
    <node concept="3EZMnI" id="1bI9L5gdQX4" role="6VMZX">
      <node concept="l2Vlx" id="1bI9L5gdQX5" role="2iSdaV" />
      <node concept="3F0ifn" id="1bI9L5gdQX6" role="3EZMnx">
        <property role="3F0ifm" value="install" />
      </node>
      <node concept="3F0A7n" id="1bI9L5gdQX8" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="lehn:1bI9L5gdN_I" resolve="install" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6porqNrmQ7E">
    <property role="3GE5qa" value="type.field" />
    <ref role="1XX52x" to="lehn:6porqNrmPQb" resolve="TypeField" />
    <node concept="3EZMnI" id="6porqNrmQ7J" role="2wV5jI">
      <node concept="3F0A7n" id="6porqNrmQ7P" role="3EZMnx">
        <property role="1$x2rV" value="&lt;Имя поля&gt;" />
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        <node concept="VPXOz" id="3tzS5_SWxOk" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F1sOY" id="6porqNrmQ7Z" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:6porqNrmPQf" resolve="valueType" />
        <node concept="VPXOz" id="3tzS5_SWxOj" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="2iRfu4" id="6porqNrmQ7L" role="2iSdaV" />
      <node concept="3F0ifn" id="5teVQz_ANmN" role="3EZMnx">
        <property role="3F0ifm" value="  &quot;" />
      </node>
      <node concept="3F0A7n" id="5teVQz_ANmP" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="lehn:5teVQz_ANmH" resolve="description" />
      </node>
      <node concept="3F0ifn" id="1yNWL2q$ik8" role="3EZMnx">
        <property role="3F0ifm" value="&quot;  " />
      </node>
      <node concept="3F2HdR" id="3ohFwV2ui29" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:3ohFwV2ui1Z" resolve="properties" />
        <node concept="2iRkQZ" id="3ohFwV2ui2a" role="2czzBx" />
        <node concept="VPM3Z" id="3ohFwV2ui2b" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="VPXOz" id="3tzS5_SWxOo" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="VPXOz" id="3tzS5_SWxB_" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6porqNrmQ83">
    <property role="3GE5qa" value="common.valuetype" />
    <ref role="1XX52x" to="lehn:6porqNrmQ7A" resolve="ValueTypeDate" />
    <node concept="3F0ifn" id="6porqNrmQ85" role="2wV5jI">
      <property role="3F0ifm" value="date" />
    </node>
  </node>
  <node concept="24kQdi" id="6porqNrmQ86">
    <property role="3GE5qa" value="common.valuetype" />
    <ref role="1XX52x" to="lehn:6porqNrmPYz" resolve="ValueTypeNumber" />
    <node concept="3EZMnI" id="6porqNrmQ88" role="2wV5jI">
      <node concept="2iRfu4" id="6porqNrmQ89" role="2iSdaV" />
      <node concept="3F0ifn" id="6porqNrmQ8a" role="3EZMnx">
        <property role="3F0ifm" value="number" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6porqNrmQ8c">
    <property role="3GE5qa" value="common.valuetype" />
    <ref role="1XX52x" to="lehn:6porqNrmQ8b" resolve="ValueTypeString" />
    <node concept="3EZMnI" id="6porqNrmRYU" role="2wV5jI">
      <node concept="2iRfu4" id="6porqNrmRYV" role="2iSdaV" />
      <node concept="3F0ifn" id="6porqNrmRYW" role="3EZMnx">
        <property role="3F0ifm" value="string(" />
      </node>
      <node concept="3F0A7n" id="6porqNrmRZ0" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:6porqNrmRYT" resolve="length" />
      </node>
      <node concept="3F0ifn" id="6porqNrmRZ2" role="3EZMnx">
        <property role="3F0ifm" value=")" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6porqNrmSMx">
    <property role="3GE5qa" value="common.valuetype" />
    <ref role="1XX52x" to="lehn:6porqNrmSHv" resolve="ValueTypeScaledNumber" />
    <node concept="3EZMnI" id="6porqNrmSMB" role="2wV5jI">
      <node concept="3F0ifn" id="6porqNrmSMF" role="3EZMnx">
        <property role="3F0ifm" value="number(" />
      </node>
      <node concept="3F0A7n" id="6porqNrmSMP" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:6porqNrmSHw" resolve="precision" />
      </node>
      <node concept="3F0ifn" id="6porqNrmSMO" role="3EZMnx">
        <property role="3F0ifm" value="," />
      </node>
      <node concept="3F0A7n" id="6porqNrmSMQ" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:6porqNrmSHx" resolve="scale" />
      </node>
      <node concept="3F0ifn" id="6porqNrmSMM" role="3EZMnx">
        <property role="3F0ifm" value=")" />
      </node>
      <node concept="2iRfu4" id="6porqNrmSMD" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="6porqNrmUqC">
    <property role="3GE5qa" value="type.key" />
    <ref role="1XX52x" to="lehn:6porqNrmTAS" resolve="TypeKeyField" />
    <node concept="1iCGBv" id="6porqNrmUqE" role="2wV5jI">
      <property role="1$x2rV" value="&lt;Выбор поля&gt;" />
      <ref role="1NtTu8" to="lehn:6porqNrmTAU" resolve="field" />
      <node concept="1sVBvm" id="6porqNrmUqF" role="1sWHZn">
        <node concept="3F0A7n" id="6porqNrmUqH" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6porqNrmUqK">
    <property role="3GE5qa" value="type.key" />
    <ref role="1XX52x" to="lehn:6porqNrmTAM" resolve="TypeKey" />
    <node concept="3EZMnI" id="6porqNrmUre" role="2wV5jI">
      <node concept="2iRfu4" id="7e_dWSFMlnD" role="2iSdaV" />
      <node concept="3F0A7n" id="6porqNrmUrg" role="3EZMnx">
        <property role="1$x2rV" value="&lt;Название ключа&gt;" />
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="6porqNrmUri" role="3EZMnx">
        <property role="3F0ifm" value="(" />
      </node>
      <node concept="3F2HdR" id="6porqNrmUrk" role="3EZMnx">
        <property role="2czwfO" value="," />
        <ref role="1NtTu8" to="lehn:6porqNrmUqI" resolve="fields" />
        <node concept="l2Vlx" id="6porqNrmUrl" role="2czzBx" />
      </node>
      <node concept="3F0ifn" id="6porqNrmUrn" role="3EZMnx">
        <property role="3F0ifm" value=")" />
      </node>
      <node concept="3F0ifn" id="qWeEqWZcwi" role="3EZMnx">
        <property role="3F0ifm" value="    unique : " />
      </node>
      <node concept="3F0A7n" id="6porqNrmUrp" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:6porqNrmTAO" resolve="unique" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="1bI9L5gdWA2">
    <property role="3GE5qa" value="common.valuetype" />
    <ref role="1XX52x" to="lehn:1bI9L5gdW_Z" resolve="ValueTypeObject" />
    <node concept="3EZMnI" id="1bI9L5gdWA4" role="2wV5jI">
      <node concept="3F0ifn" id="1bI9L5gdXVQ" role="3EZMnx">
        <property role="3F0ifm" value="-&gt;" />
      </node>
      <node concept="1iCGBv" id="1bI9L5gdWA7" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:1bI9L5gdWA1" resolve="referencedType" />
        <node concept="1sVBvm" id="1bI9L5gdWA8" role="1sWHZn">
          <node concept="3EZMnI" id="1bI9L5gdWAg" role="2wV5jI">
            <node concept="3F0A7n" id="1bI9L5gdWAm" role="3EZMnx">
              <property role="1Intyy" value="true" />
              <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
            </node>
            <node concept="2iRfu4" id="1bI9L5gdWAh" role="2iSdaV" />
            <node concept="VPM3Z" id="1bI9L5gdWAi" role="3F10Kt">
              <property role="VOm3f" value="false" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="1bI9L5gdWA6" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="4WUZoVQo981">
    <property role="3GE5qa" value="common.valuetype" />
    <ref role="1XX52x" to="lehn:4WUZoVQo97Y" resolve="ValueTypeDateTime" />
    <node concept="3F0ifn" id="4WUZoVQo983" role="2wV5jI">
      <property role="3F0ifm" value="datetime" />
    </node>
  </node>
  <node concept="24kQdi" id="2LNTlDxV3cy">
    <property role="3GE5qa" value="common.valuetype" />
    <ref role="1XX52x" to="lehn:2LNTlDxV3ct" resolve="ValueTypeEnumerationValue" />
    <node concept="3EZMnI" id="2LNTlDxV3c$" role="2wV5jI">
      <node concept="2iRfu4" id="3XkMtR1ePE5" role="2iSdaV" />
      <node concept="3F0ifn" id="2LNTlDxV3cB" role="3EZMnx">
        <property role="3F0ifm" value="(" />
      </node>
      <node concept="3F0A7n" id="2LNTlDxV3cD" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="2LNTlDxV3cF" role="3EZMnx">
        <property role="3F0ifm" value=":" />
      </node>
      <node concept="3F0A7n" id="2LNTlDxV3cH" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="lehn:2LNTlDxV3cx" resolve="description" />
      </node>
      <node concept="3F0ifn" id="2LNTlDxV3cJ" role="3EZMnx">
        <property role="3F0ifm" value=")" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="2LNTlDxV3cM">
    <property role="3GE5qa" value="common.valuetype" />
    <ref role="1XX52x" to="lehn:2LNTlDxV0Js" resolve="ValueTypeEnumeration" />
    <node concept="3EZMnI" id="2LNTlDxV3cO" role="2wV5jI">
      <node concept="3F0ifn" id="2LNTlDxV3cR" role="3EZMnx">
        <property role="3F0ifm" value="enum" />
      </node>
      <node concept="3F2HdR" id="2LNTlDxV3cT" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:2LNTlDxV3cL" resolve="values" />
        <node concept="2EHx9g" id="3XkMtR1ePE2" role="2czzBx" />
        <node concept="VPM3Z" id="2LNTlDxV3cV" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="l2Vlx" id="2LNTlDxV3cQ" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="2j4WJkiqSsb">
    <property role="3GE5qa" value="common.valuetype" />
    <ref role="1XX52x" to="lehn:2j4WJkiqPZ9" resolve="ValueTypeBoolean" />
    <node concept="3F0ifn" id="2j4WJkiqSsd" role="2wV5jI">
      <property role="3F0ifm" value="logical" />
    </node>
  </node>
  <node concept="24kQdi" id="2nKQUcI4FDF">
    <property role="3GE5qa" value="common.valuetype" />
    <ref role="1XX52x" to="lehn:2nKQUcI4FD_" resolve="ValueTypeBLOB" />
    <node concept="3F0ifn" id="2nKQUcI4FDH" role="2wV5jI">
      <property role="3F0ifm" value="BLOB" />
    </node>
  </node>
  <node concept="24kQdi" id="2nKQUcI4FDQ">
    <property role="3GE5qa" value="common.valuetype" />
    <ref role="1XX52x" to="lehn:2nKQUcI4FDK" resolve="ValueTypeText" />
    <node concept="3F0ifn" id="2nKQUcI4FDS" role="2wV5jI">
      <property role="3F0ifm" value="text" />
    </node>
  </node>
  <node concept="24kQdi" id="1yNWL2qySTy">
    <property role="3GE5qa" value="type.field.property.validator" />
    <ref role="1XX52x" to="lehn:1yNWL2qySRY" resolve="RequiredTypeFieldProperty" />
    <node concept="3EZMnI" id="1yNWL2qyToA" role="2wV5jI">
      <node concept="3F0ifn" id="1yNWL2qyToH" role="3EZMnx">
        <property role="3F0ifm" value="required" />
      </node>
      <node concept="2iRfu4" id="1yNWL2qyToD" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="1yNWL2qAw6Y">
    <property role="3GE5qa" value="service.method" />
    <ref role="1XX52x" to="lehn:xAAq0nImMq" resolve="ServiceMethod" />
    <node concept="3EZMnI" id="1yNWL2qAzLJ" role="2wV5jI">
      <node concept="3F1sOY" id="1yNWL2qAzLP" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:7y$dPqeESH_" resolve="valueType" />
      </node>
      <node concept="3F0A7n" id="1yNWL2qAzLV" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3EZMnI" id="32y5cLH3lGG" role="3EZMnx">
        <node concept="VPM3Z" id="32y5cLH3lGI" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3EZMnI" id="32y5cLH3lGS" role="3EZMnx">
          <node concept="VPM3Z" id="32y5cLH3lGU" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="32y5cLH3lH3" role="3EZMnx">
            <property role="3F0ifm" value="(" />
          </node>
          <node concept="3F2HdR" id="32y5cLH3lH9" role="3EZMnx">
            <ref role="1NtTu8" to="lehn:32y5cLH3lGl" resolve="parameters" />
            <node concept="2iRfu4" id="32y5cLH3lHc" role="2czzBx" />
            <node concept="VPM3Z" id="32y5cLH3lHd" role="3F10Kt">
              <property role="VOm3f" value="false" />
            </node>
          </node>
          <node concept="3F0ifn" id="32y5cLH3lHv" role="3EZMnx">
            <property role="3F0ifm" value=")" />
          </node>
          <node concept="l2Vlx" id="32y5cLH3lGX" role="2iSdaV" />
        </node>
        <node concept="3F2HdR" id="32y5cLH3lHK" role="3EZMnx">
          <ref role="1NtTu8" to="lehn:32y5cLH3lI0" resolve="statements" />
          <node concept="2iRkQZ" id="32y5cLH3lHN" role="2czzBx" />
          <node concept="VPM3Z" id="32y5cLH3lHO" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
        </node>
        <node concept="2iRkQZ" id="32y5cLH3lGL" role="2iSdaV" />
      </node>
      <node concept="2iRfu4" id="1yNWL2qAzLK" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="1yNWL2qAzM4">
    <property role="3GE5qa" value="service" />
    <ref role="1XX52x" to="lehn:xAAq0nImMn" resolve="ServiceNode" />
    <node concept="3EZMnI" id="1yNWL2qAzM6" role="2wV5jI">
      <node concept="3F0ifn" id="1yNWL2qAzMd" role="3EZMnx">
        <property role="3F0ifm" value="Service" />
      </node>
      <node concept="3F0A7n" id="1yNWL2qAzMj" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="1yNWL2qAzMr" role="3EZMnx">
        <node concept="pVoyu" id="1yNWL2qAzO8" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="1yNWL2qAzOb" role="3EZMnx">
        <property role="3F0ifm" value=" methods" />
        <node concept="pVoyu" id="1yNWL2qAzOc" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="1yNWL2qAzOk" role="3EZMnx">
        <node concept="pVoyu" id="1yNWL2qAzOl" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="1yNWL2qAEW5" role="3EZMnx">
        <property role="3F0ifm" value="    " />
        <node concept="pVoyu" id="1yNWL2qAEW6" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="1yNWL2qAzPd" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:xAAq0nImMD" resolve="methods" />
        <node concept="2iRkQZ" id="1yNWL2qAzPg" role="2czzBx" />
        <node concept="VPM3Z" id="1yNWL2qAzPh" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="3F0ifn" id="1yNWL2qAzOE" role="3EZMnx">
        <node concept="pVoyu" id="1yNWL2qAzOF" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="1yNWL2qAzM9" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="1yNWL2qAM0f">
    <property role="3GE5qa" value="service.method" />
    <ref role="1XX52x" to="lehn:1WjxkW0cEWa" resolve="ServiceMethodRef" />
    <node concept="3EZMnI" id="1yNWL2qAM0h" role="2wV5jI">
      <node concept="1iCGBv" id="1yNWL2qAM0Q" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:1WjxkW0cEWb" resolve="service" />
        <node concept="1sVBvm" id="1yNWL2qAM0S" role="1sWHZn">
          <node concept="3F0A7n" id="1yNWL2qAM10" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="1yNWL2qAM0k" role="2iSdaV" />
      <node concept="3F0ifn" id="1yNWL2qAM1j" role="3EZMnx">
        <property role="3F0ifm" value="." />
      </node>
      <node concept="1iCGBv" id="1yNWL2qAM1D" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:1WjxkW0cEWc" resolve="method" />
        <node concept="1sVBvm" id="1yNWL2qAM1F" role="1sWHZn">
          <node concept="3F0A7n" id="1yNWL2qAM1R" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="28N7VjDcBiV">
    <property role="3GE5qa" value="type.field.property.database" />
    <ref role="1XX52x" to="lehn:28N7VjDcBiJ" resolve="TableColumnTypeFieldProperty" />
    <node concept="3EZMnI" id="28N7VjDcBiX" role="2wV5jI">
      <node concept="3F0ifn" id="28N7VjDcBj4" role="3EZMnx">
        <property role="3F0ifm" value="table column" />
      </node>
      <node concept="3F0A7n" id="28N7VjDcBja" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:28N7VjDcBiK" resolve="columnName" />
      </node>
      <node concept="2iRfu4" id="28N7VjDcBj0" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="2jHeDCe3qI_">
    <property role="3GE5qa" value="type.property.extension.method" />
    <ref role="1XX52x" to="lehn:2jHeDCe3qIv" resolve="CheckExtensionTypePropertyMethod" />
    <node concept="3F0ifn" id="2jHeDCe3qII" role="2wV5jI">
      <property role="3F0ifm" value="check" />
    </node>
  </node>
  <node concept="24kQdi" id="2jHeDCe3qIM">
    <property role="3GE5qa" value="type.property.extension.method" />
    <ref role="1XX52x" to="lehn:2jHeDCe3qIy" resolve="CreateExtensionTypePropertyMethod" />
    <node concept="3F0ifn" id="2jHeDCe3qIO" role="2wV5jI">
      <property role="3F0ifm" value="create" />
    </node>
  </node>
  <node concept="24kQdi" id="2jHeDCe3qIR">
    <property role="3GE5qa" value="type.property.extension.method" />
    <ref role="1XX52x" to="lehn:2jHeDCe3qIx" resolve="DeleteExtensionTypePropertyMethod" />
    <node concept="3F0ifn" id="2jHeDCe3qIT" role="2wV5jI">
      <property role="3F0ifm" value="delete" />
    </node>
  </node>
  <node concept="24kQdi" id="2jHeDCe3qIW">
    <property role="3GE5qa" value="type.property.extension.method" />
    <ref role="1XX52x" to="lehn:2jHeDCe3qIw" resolve="SaveExtensionTypePropertyMethod" />
    <node concept="3F0ifn" id="2jHeDCe3qIY" role="2wV5jI">
      <property role="3F0ifm" value="save" />
    </node>
  </node>
  <node concept="24kQdi" id="2jHeDCe3qJ5">
    <property role="3GE5qa" value="type.property.extension.action" />
    <ref role="1XX52x" to="lehn:2jHeDCe3qJ2" resolve="CallServiceMethodExtensionTypePropertyAction" />
    <node concept="3EZMnI" id="2jHeDCe3qJ7" role="2wV5jI">
      <node concept="3F0ifn" id="2jHeDCe3qJe" role="3EZMnx">
        <property role="3F0ifm" value="call service method" />
      </node>
      <node concept="3F1sOY" id="2jHeDCe3qJm" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:2jHeDCe3qJh" resolve="serviceMethod" />
      </node>
      <node concept="2iRfu4" id="2jHeDCe3qJa" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="2jHeDCe3qJt">
    <property role="3GE5qa" value="type.property.extension" />
    <ref role="1XX52x" to="lehn:2jHeDCe3qI3" resolve="AfterExtensionTypeProperty" />
    <node concept="3EZMnI" id="2jHeDCe3qJv" role="2wV5jI">
      <node concept="3F0ifn" id="2jHeDCe3qJA" role="3EZMnx">
        <property role="3F0ifm" value="after" />
      </node>
      <node concept="3F1sOY" id="2jHeDCe3qJG" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:2jHeDCe3qIz" resolve="method" />
      </node>
      <node concept="3F1sOY" id="2jHeDCe3qJO" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:2jHeDCe3qJq" resolve="action" />
      </node>
      <node concept="2iRfu4" id="2jHeDCe3qJy" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="2jHeDCe3qJT">
    <property role="3GE5qa" value="type.property.extension" />
    <ref role="1XX52x" to="lehn:2jHeDCe3qI2" resolve="BeforeExtensionTypeProperty" />
    <node concept="3EZMnI" id="2jHeDCe3qJV" role="2wV5jI">
      <node concept="3F0ifn" id="2jHeDCe3qK2" role="3EZMnx">
        <property role="3F0ifm" value="before" />
      </node>
      <node concept="3F1sOY" id="2jHeDCe3qK8" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:2jHeDCe3qIz" resolve="method" />
      </node>
      <node concept="3F1sOY" id="2jHeDCe3qKg" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:2jHeDCe3qJq" resolve="action" />
      </node>
      <node concept="2iRfu4" id="2jHeDCe3qJY" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3kI_owliyBW">
    <property role="3GE5qa" value="common.valuetype" />
    <ref role="1XX52x" to="lehn:3kI_owliyBM" resolve="ValueTypeList" />
    <node concept="3EZMnI" id="3kI_owliyBY" role="2wV5jI">
      <node concept="3F0ifn" id="3kI_owliyC8" role="3EZMnx">
        <property role="3F0ifm" value="[]" />
      </node>
      <node concept="1iCGBv" id="3kI_owliyCh" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:3kI_owliyBN" resolve="referencedType" />
        <node concept="1sVBvm" id="3kI_owliyCj" role="1sWHZn">
          <node concept="3F0A7n" id="3kI_owliyCv" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="3kI_owliyC1" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="7rpCSfHG62g">
    <property role="3GE5qa" value="security.permission" />
    <ref role="1XX52x" to="lehn:7rpCSfHG625" resolve="PermissionType" />
    <node concept="3EZMnI" id="7rpCSfHG62l" role="2wV5jI">
      <node concept="3F0ifn" id="7rpCSfHG62n" role="3EZMnx">
        <property role="3F0ifm" value="Permission" />
      </node>
      <node concept="3F0A7n" id="7rpCSfHG62v" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="l2Vlx" id="7rpCSfHG62o" role="2iSdaV" />
      <node concept="3F0ifn" id="7rpCSfHGTTS" role="3EZMnx">
        <node concept="pVoyu" id="7rpCSfHGTTX" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="7rpCSfHGTU2" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="7rpCSfHGTU6" role="3EZMnx">
        <property role="3F0ifm" value="description" />
        <node concept="pVoyu" id="7rpCSfHGTU7" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="7rpCSfHGTU8" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="7rpCSfHGTUG" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="lehn:7rpCSfHGTTM" resolve="description" />
      </node>
      <node concept="3F0ifn" id="7rpCSfHGTUh" role="3EZMnx">
        <node concept="pVoyu" id="7rpCSfHGTUi" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="7rpCSfHGTUj" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="7rpCSfHG62H">
    <property role="3GE5qa" value="security.role" />
    <ref role="1XX52x" to="lehn:7rpCSfHG61T" resolve="Role" />
    <node concept="3EZMnI" id="7rpCSfHG62J" role="2wV5jI">
      <node concept="3F0ifn" id="7rpCSfHG62Q" role="3EZMnx">
        <property role="3F0ifm" value="Role" />
      </node>
      <node concept="3F0A7n" id="7rpCSfHG62W" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="7rpCSfHGTUW" role="3EZMnx">
        <node concept="pVoyu" id="7rpCSfHGTUX" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="7rpCSfHGTUY" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="7rpCSfHGTUZ" role="3EZMnx">
        <property role="3F0ifm" value="description" />
        <node concept="pVoyu" id="7rpCSfHGTV0" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="7rpCSfHGTV1" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="7rpCSfHGTV2" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="lehn:7rpCSfHGTUU" resolve="description" />
      </node>
      <node concept="3F0ifn" id="7rpCSfHGTV3" role="3EZMnx">
        <node concept="pVoyu" id="7rpCSfHGTV4" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="7rpCSfHGTV5" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="7rpCSfHG62M" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="72ziUYArkGz">
    <property role="3GE5qa" value="type.property.condition" />
    <ref role="1XX52x" to="lehn:72ziUYArkGy" resolve="ChangeableIfConditionTypeProperty" />
    <node concept="3EZMnI" id="72ziUYArkGB" role="2wV5jI">
      <node concept="3F0ifn" id="72ziUYArkGI" role="3EZMnx">
        <property role="3F0ifm" value="changeable if" />
      </node>
      <node concept="3F1sOY" id="72ziUYArkGO" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:72ziUYArkG_" resolve="condition" />
      </node>
      <node concept="2iRfu4" id="72ziUYArkGE" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="72ziUYArkGU">
    <property role="3GE5qa" value="type.property.condition" />
    <ref role="1XX52x" to="lehn:72ziUYArkGT" resolve="VisibleIfConditionTypeProperty" />
    <node concept="3EZMnI" id="72ziUYArkGW" role="2wV5jI">
      <node concept="3F0ifn" id="72ziUYArkH3" role="3EZMnx">
        <property role="3F0ifm" value="visible if" />
      </node>
      <node concept="3F1sOY" id="72ziUYArkH9" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:72ziUYArkG_" resolve="condition" />
      </node>
      <node concept="2iRfu4" id="72ziUYArkGZ" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="72ziUYArkHe">
    <property role="3GE5qa" value="type.property.condition" />
    <ref role="1XX52x" to="lehn:72ziUYArkHd" resolve="HiddenIfConditionTypeProperty" />
    <node concept="3EZMnI" id="72ziUYArkHg" role="2wV5jI">
      <node concept="3F0ifn" id="72ziUYArkHn" role="3EZMnx">
        <property role="3F0ifm" value="hidden if" />
      </node>
      <node concept="3F1sOY" id="72ziUYArkHx" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:72ziUYArkG_" resolve="condition" />
      </node>
      <node concept="2iRfu4" id="72ziUYArkHj" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="72ziUYArkHA">
    <property role="3GE5qa" value="type.property.condition" />
    <ref role="1XX52x" to="lehn:72ziUYArkH_" resolve="ReadOnlyIfConditionTypeProperty" />
    <node concept="3EZMnI" id="72ziUYArkHC" role="2wV5jI">
      <node concept="3F0ifn" id="72ziUYArkHJ" role="3EZMnx">
        <property role="3F0ifm" value="readonly if" />
      </node>
      <node concept="3F1sOY" id="72ziUYArkHP" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:72ziUYArkG_" resolve="condition" />
      </node>
      <node concept="2iRfu4" id="72ziUYArkHF" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="72ziUYArkI4">
    <property role="3GE5qa" value="type.field.property.condition" />
    <ref role="1XX52x" to="lehn:72ziUYArkI0" resolve="ChangeableIfConditionTypeFieldProperty" />
    <node concept="3EZMnI" id="72ziUYArkI6" role="2wV5jI">
      <node concept="3F0ifn" id="72ziUYArkId" role="3EZMnx">
        <property role="3F0ifm" value="changeable if" />
      </node>
      <node concept="3F1sOY" id="72ziUYArkIj" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:72ziUYArkHW" resolve="condition" />
      </node>
      <node concept="2iRfu4" id="72ziUYArkI9" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="72ziUYArkIn">
    <property role="3GE5qa" value="type.field.property.condition" />
    <ref role="1XX52x" to="lehn:72ziUYArkI1" resolve="VisibleIfConditionTypeFieldProperty" />
    <node concept="3EZMnI" id="72ziUYArkIp" role="2wV5jI">
      <node concept="3F0ifn" id="72ziUYArkIw" role="3EZMnx">
        <property role="3F0ifm" value="visible if" />
      </node>
      <node concept="3F1sOY" id="72ziUYArkIA" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:72ziUYArkHW" resolve="condition" />
      </node>
      <node concept="2iRfu4" id="72ziUYArkIs" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="72ziUYArkIE">
    <property role="3GE5qa" value="type.field.property.condition" />
    <ref role="1XX52x" to="lehn:72ziUYArkI2" resolve="HiddenIfConditionTypeFieldProperty" />
    <node concept="3EZMnI" id="72ziUYArkIG" role="2wV5jI">
      <node concept="3F0ifn" id="72ziUYArkIN" role="3EZMnx">
        <property role="3F0ifm" value="hidden if" />
      </node>
      <node concept="3F1sOY" id="72ziUYArkIT" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:72ziUYArkHW" resolve="condition" />
      </node>
      <node concept="2iRfu4" id="72ziUYArkIJ" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="72ziUYArkIX">
    <property role="3GE5qa" value="type.field.property.condition" />
    <ref role="1XX52x" to="lehn:72ziUYArkI3" resolve="ReadOnlyIfConditionTypeFieldProperty" />
    <node concept="3EZMnI" id="72ziUYArkIZ" role="2wV5jI">
      <node concept="3F0ifn" id="72ziUYArkJ6" role="3EZMnx">
        <property role="3F0ifm" value="readonly if" />
      </node>
      <node concept="3F1sOY" id="72ziUYArkJc" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:72ziUYArkHW" resolve="condition" />
      </node>
      <node concept="2iRfu4" id="72ziUYArkJ2" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="72ziUYArkJr">
    <property role="3GE5qa" value="type.condition" />
    <ref role="1XX52x" to="lehn:72ziUYArkJg" resolve="RoleEqualTypeCondition" />
    <node concept="3EZMnI" id="72ziUYArkJt" role="2wV5jI">
      <node concept="3F0ifn" id="72ziUYArkJ$" role="3EZMnx">
        <property role="3F0ifm" value="role = " />
      </node>
      <node concept="3F2HdR" id="385iqfzvxiN" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:385iqfzvxiy" resolve="roles" />
        <node concept="2EHx9g" id="385iqfzvCFk" role="2czzBx" />
        <node concept="VPM3Z" id="385iqfzvxiR" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="2iRfu4" id="72ziUYArkJw" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="72ziUYArkJT">
    <property role="3GE5qa" value="type.condition" />
    <ref role="1XX52x" to="lehn:72ziUYArkJR" resolve="RoleNotEqualTypeCondition" />
    <node concept="3EZMnI" id="72ziUYArkJV" role="2wV5jI">
      <node concept="3F0ifn" id="72ziUYArkK2" role="3EZMnx">
        <property role="3F0ifm" value="role !=" />
      </node>
      <node concept="3F2HdR" id="385iqfzvxj2" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:385iqfzvxiW" resolve="roles" />
        <node concept="2EHx9g" id="385iqfzvCFn" role="2czzBx" />
        <node concept="VPM3Z" id="385iqfzvxj6" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="2iRfu4" id="72ziUYArkJY" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="385iqfzvxid">
    <property role="3GE5qa" value="security.role" />
    <ref role="1XX52x" to="lehn:385iqfzvxi2" resolve="RoleRef" />
    <node concept="3EZMnI" id="385iqfzvxif" role="2wV5jI">
      <node concept="1iCGBv" id="385iqfzvxim" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:385iqfzvxi3" resolve="role" />
        <node concept="1sVBvm" id="385iqfzvxio" role="1sWHZn">
          <node concept="3F0A7n" id="385iqfzvxiv" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="385iqfzvxii" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="yAB$tbwYi0">
    <property role="3GE5qa" value="type.method" />
    <ref role="1XX52x" to="lehn:yAB$tbwYhX" resolve="TypeMethod" />
    <node concept="3EZMnI" id="yAB$tbwYi2" role="2wV5jI">
      <node concept="3F0A7n" id="yAB$tbwYib" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="yAB$tbxBil" role="3EZMnx">
        <property role="3F0ifm" value=":" />
      </node>
      <node concept="3F1sOY" id="yAB$tbwYik" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:7y$dPqeESH_" resolve="valueType" />
      </node>
      <node concept="3F0ifn" id="yAB$tbxNz3" role="3EZMnx">
        <property role="3F0ifm" value="()" />
      </node>
      <node concept="2iRfu4" id="yAB$tbwYi5" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="32y5cLH3e3m">
    <property role="3GE5qa" value="service.method.parameter" />
    <ref role="1XX52x" to="lehn:32y5cLH3e39" resolve="ServiceMethodParameter" />
    <node concept="3EZMnI" id="32y5cLH3e3o" role="2wV5jI">
      <node concept="3F0A7n" id="32y5cLH3e3M" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F1sOY" id="32y5cLH3e3v" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:32y5cLH3e3c" resolve="valueType" />
      </node>
      <node concept="2iRfu4" id="32y5cLH3e3r" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="32y5cLH3tiu">
    <property role="3GE5qa" value="database.table" />
    <ref role="1XX52x" to="lehn:32y5cLH3ti7" resolve="Table" />
    <node concept="3EZMnI" id="32y5cLH3tiw" role="2wV5jI">
      <node concept="3F0ifn" id="32y5cLH3tiB" role="3EZMnx">
        <property role="3F0ifm" value="Table" />
      </node>
      <node concept="3F0A7n" id="32y5cLH3tiH" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="32y5cLH3tiP" role="3EZMnx">
        <node concept="lj46D" id="32y5cLH3tiU" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="32y5cLH3tiZ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="32y5cLH3tj3" role="3EZMnx">
        <property role="3F0ifm" value="columns" />
        <node concept="pVoyu" id="32y5cLH3tj5" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="32y5cLH3PtT" role="3EZMnx">
        <node concept="lj46D" id="32y5cLH3PtU" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="32y5cLH3PtV" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="32y5cLH3tjD" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:32y5cLH3tid" resolve="columns" />
        <node concept="2EHx9g" id="32y5cLH3tjW" role="2czzBx" />
        <node concept="VPM3Z" id="32y5cLH3tjH" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="lj46D" id="32y5cLH3Pty" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="32y5cLH3PtJ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="32y5cLH3tje" role="3EZMnx">
        <node concept="lj46D" id="32y5cLH3tjf" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="32y5cLH3tjg" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="32y5cLH3tiz" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="32y5cLH3tkc">
    <property role="3GE5qa" value="database.table.column.type" />
    <ref role="1XX52x" to="lehn:32y5cLH3tk1" resolve="VarcharTableColumnType" />
    <node concept="3EZMnI" id="32y5cLH3tke" role="2wV5jI">
      <node concept="3F0ifn" id="32y5cLH3tkl" role="3EZMnx">
        <property role="3F0ifm" value="varchar (" />
      </node>
      <node concept="3F0A7n" id="32y5cLH3tkr" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:32y5cLH3tk2" resolve="length" />
      </node>
      <node concept="3F0ifn" id="32y5cLH3tkz" role="3EZMnx">
        <property role="3F0ifm" value=")" />
      </node>
      <node concept="2iRfu4" id="32y5cLH3tkh" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="32y5cLH3_ev">
    <property role="3GE5qa" value="service.method.valuetype" />
    <ref role="1XX52x" to="lehn:32y5cLH3_eu" resolve="ServiceMethodValueTypeNumber" />
    <node concept="3EZMnI" id="32y5cLH3_ex" role="2wV5jI">
      <node concept="3F0ifn" id="32y5cLH3_eF" role="3EZMnx">
        <property role="3F0ifm" value="number" />
      </node>
      <node concept="2iRfu4" id="32y5cLH3_e$" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="32y5cLH3_eK">
    <property role="3GE5qa" value="service.method.valuetype" />
    <ref role="1XX52x" to="lehn:32y5cLH3_eJ" resolve="ServiceMethodValueTypeString" />
    <node concept="3EZMnI" id="32y5cLH3_eM" role="2wV5jI">
      <node concept="3F0ifn" id="32y5cLH3_eW" role="3EZMnx">
        <property role="3F0ifm" value="string" />
      </node>
      <node concept="2iRfu4" id="32y5cLH3_eP" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="32y5cLH3J7t">
    <property role="3GE5qa" value="database.table.column" />
    <ref role="1XX52x" to="lehn:32y5cLH3tia" resolve="TableColumn" />
    <node concept="3EZMnI" id="32y5cLH3J7v" role="2wV5jI">
      <node concept="3F0A7n" id="32y5cLH3J7D" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        <node concept="VPXOz" id="32y5cLH3Puw" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F1sOY" id="32y5cLH3J7M" role="3EZMnx">
        <ref role="1NtTu8" to="lehn:32y5cLH3tjZ" resolve="type" />
        <node concept="VPXOz" id="32y5cLH3Puz" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="2iRfu4" id="32y5cLH3J7y" role="2iSdaV" />
    </node>
  </node>
</model>

