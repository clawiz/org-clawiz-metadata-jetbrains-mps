/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data;

import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.datatype.MpsDataTypeDeclaration;
import org.clawiz.core.common.CoreException;
import org.clawiz.metadata.jetbrains.mps.parser.data.model.MpsModelNodeRef;

public class MpsPropertyDeclaration extends MpsBaseConcept {

    String propertyId;

    MpsDataTypeDeclaration dataType;

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public MpsDataTypeDeclaration getDataType() {
        return dataType;
    }

    public void setDataType(MpsDataTypeDeclaration dataType) {
        this.dataType = dataType;
    }

    MpsModelNodeRef valueTypeRef;


    public MpsModelNodeRef getValueTypeRef() {
        if ( valueTypeRef != null ) {
            return valueTypeRef;
        }
        if ( getMpsModelNode().getRefs().size() != 1) {
            throw new CoreException("Property ? node refs array size != 1", getName());
        }
        valueTypeRef = getMpsModelNode().getRefs().get(0);
        return valueTypeRef;
    }

    public boolean isEnumeration() {
        return getValueTypeRef().getNode() != null ;
    }

    String valueTypeClassName;

    public String getValueTypeClassName() {
        if ( valueTypeClassName != null ) {
            return valueTypeClassName;
        }

        if ( isEnumeration() ) {
            valueTypeClassName = this.getModel().nodeRefToJavaClassName(getValueTypeRef());
        } else {
            String typeName = getValueTypeRef().getResolve();
            if ( typeName == null ) {
                throw new CoreException("Property ? value type ref resolve is null", getName());
            }
            switch (typeName) {
                case "string"  : valueTypeClassName = "String"; break;
                case "integer" : valueTypeClassName = "BigDecimal"; break;
                case "number"  : valueTypeClassName = "BigDecimal"; break;
                case "boolean" : valueTypeClassName = "Boolean"; break;
                default: throw new CoreException("Wrong property ? value type ?", getName(), typeName);
            }
        }

        return valueTypeClassName;
    }

}
