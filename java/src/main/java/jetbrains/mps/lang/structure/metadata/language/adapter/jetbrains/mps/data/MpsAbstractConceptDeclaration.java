/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data;

import java.util.ArrayList;

public class MpsAbstractConceptDeclaration extends MpsBaseConcept {

    Boolean _abstract;
    String  conceptId;

    String conceptAlias;
    String languageId;

    ArrayList<MpsPropertyDeclaration> propertyDeclaration = new ArrayList<>();
    ArrayList<MpsLinkDeclaration> linkDeclaration = new ArrayList<>();

    public Boolean getAbstract() {
        return _abstract;
    }

    public boolean isAbatract() {
        return _abstract != null ? _abstract.booleanValue() : false;
    }

    public void setAbstract(Boolean _abstract) {
        this._abstract = _abstract;
    }

    public String getConceptId() {
        return conceptId;
    }

    public void setConceptId(String conceptId) {
        this.conceptId = conceptId;
    }

    public String getConceptAlias() {
        return conceptAlias;
    }

    public void setConceptAlias(String conceptAlias) {
        this.conceptAlias = conceptAlias;
    }

    public String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }

    public ArrayList<MpsPropertyDeclaration> getPropertyDeclaration() {
        return propertyDeclaration;
    }

    public ArrayList<MpsLinkDeclaration> getLinkDeclaration() {
        return linkDeclaration;
    }
}
