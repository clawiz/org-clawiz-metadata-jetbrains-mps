/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.datatype;

import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.core.common.system.generator.GeneratorUtils;

public class MpsEnumerationMemberDeclaration extends AbstractMpsNode {

    String internalValue;
    String externalValue;
    String javaIdentifier;

    public String getInternalValue() {
        return internalValue;
    }

    public void setInternalValue(String internalValue) {
        this.internalValue = internalValue;
    }

    public String getExternalValue() {
        return externalValue;
    }

    public void setExternalValue(String externalValue) {
        this.externalValue = externalValue;
    }

    public String getJavaIdentifier() {
        return javaIdentifier;
    }

    public void setJavaIdentifier(String javaIdentifier) {
        this.javaIdentifier = javaIdentifier;
    }

    public String getEnumerationValueName() {
        return ( getInternalValue() != null ? getInternalValue()
                :  ( getInternalValue() != null ? getInternalValue() : GeneratorUtils.toJavaName(getExternalValue())
                   )
               );
    }

}
