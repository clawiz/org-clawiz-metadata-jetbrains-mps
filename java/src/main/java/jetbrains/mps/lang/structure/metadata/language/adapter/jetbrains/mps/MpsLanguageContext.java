/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps;

import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.datatype.MpsEnumerationDataTypeDeclaration;
import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.datatype.MpsEnumerationMemberDeclaration;
import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.datatype.MpsPrimitiveDataTypeDeclaration;
import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.*;
import org.clawiz.metadata.jetbrains.mps.parser.context.AbstractMpsLanguageContext;

public class MpsLanguageContext extends AbstractMpsLanguageContext {


    @Override
    public String getLanguageName() {
        return "jetbrains.mps.lang.structure";
    }

    @Override
    public void prepare() {
        addClassMap(MpsConceptDeclaration.class);
        addClassMap(MpsInterfaceConceptDeclaration.class);
        addClassMap(MpsInterfaceConceptReference.class);

        addClassMap(MpsEnumerationDataTypeDeclaration.class);
        addClassMap(MpsEnumerationMemberDeclaration.class);
        addClassMap(MpsPrimitiveDataTypeDeclaration.class);

        addClassMap(MpsPropertyDeclaration.class);

        addClassMap(MpsLinkDeclaration.class);
    }

}
