/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data;

import org.clawiz.core.common.CoreException;

public class MpsLinkDeclaration extends MpsBaseConcept {

    String  role;
    String  metaClass;
    String  sourceCardinality;
    Boolean unordered;
    String  linkId;

    MpsLinkDeclaration specializedLink;
    MpsAbstractConceptDeclaration target;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getMetaClass() {
        return metaClass;
    }

    public void setMetaClass(String metaClass) {
        this.metaClass = metaClass;
    }

    public String getSourceCardinality() {
        return sourceCardinality;
    }

    public void setSourceCardinality(String sourceCardinality) {
        this.sourceCardinality = sourceCardinality;
    }

    public Boolean getUnordered() {
        return unordered;
    }

    public void setUnordered(Boolean unordered) {
        this.unordered = unordered;
    }

    public String getLinkId() {
        return linkId;
    }

    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }

    public MpsLinkDeclaration getSpecializedLink() {
        return specializedLink;
    }

    public void setSpecializedLink(MpsLinkDeclaration specializedLink) {
        this.specializedLink = specializedLink;
    }

    public MpsAbstractConceptDeclaration getTarget() {
        return target;
    }

    public void setTarget(MpsAbstractConceptDeclaration target) {
        this.target = target;
    }


    public boolean isArray() {
        // null = 0..1
        return getSourceCardinality() != null && (getSourceCardinality().equals("0..n") || getSourceCardinality().equals("1..n"));
    }

    String valueTypeClassName;

    public String getValueTypeClassName() {
        if ( valueTypeClassName != null ) {
            return valueTypeClassName;
        }

        if ( getTarget() == null ) {
            throw new CoreException("Target is null for node ?", getName());
        }

        valueTypeClassName = getModel().nodeToJavaClassName(getTarget());

        return valueTypeClassName;
    }

}
