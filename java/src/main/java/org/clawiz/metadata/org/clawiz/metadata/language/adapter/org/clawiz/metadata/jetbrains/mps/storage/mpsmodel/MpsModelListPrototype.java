package org.clawiz.metadata.org.clawiz.metadata.language.adapter.org.clawiz.metadata.jetbrains.mps.storage.mpsmodel;

import org.clawiz.core.common.system.object.AbstractObjectList;

public class MpsModelListPrototype extends AbstractObjectList<MpsModelObject> {
}
