package org.clawiz.metadata.org.clawiz.metadata.language.adapter.org.clawiz.metadata.jetbrains.mps.storage.mpsmodel;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;

public class MpsModelModelPrototype extends TypeModel {
    
    private static TypeField PACKAGE_NAME_FIELD;
    
    private static TypeField NAME_FIELD;
    
    private static TypeField MODEL_UID_FIELD;
    
    private static TypeField FILE_NAME_FIELD;
    
    private static TypeField DIRECTORY_NAME_FIELD;
    
    private static Type type;
    
    private MpsModelService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public MpsModelService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(MpsModelService.class).getType();
        
        
        PACKAGE_NAME_FIELD = type.getFields().get("PackageName");
        if ( PACKAGE_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.metadata.org.clawiz.metadata.language.adapter.org.clawiz.metadata.jetbrains.mps.storage.MpsModel.PackageName"}); }
        
        NAME_FIELD = type.getFields().get("Name");
        if ( NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.metadata.org.clawiz.metadata.language.adapter.org.clawiz.metadata.jetbrains.mps.storage.MpsModel.Name"}); }
        
        MODEL_UID_FIELD = type.getFields().get("ModelUid");
        if ( MODEL_UID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.metadata.org.clawiz.metadata.language.adapter.org.clawiz.metadata.jetbrains.mps.storage.MpsModel.ModelUid"}); }
        
        FILE_NAME_FIELD = type.getFields().get("FileName");
        if ( FILE_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.metadata.org.clawiz.metadata.language.adapter.org.clawiz.metadata.jetbrains.mps.storage.MpsModel.FileName"}); }
        
        DIRECTORY_NAME_FIELD = type.getFields().get("DirectoryName");
        if ( DIRECTORY_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.metadata.org.clawiz.metadata.language.adapter.org.clawiz.metadata.jetbrains.mps.storage.MpsModel.DirectoryName"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _packageName;
    
    public TypeFieldModel packageName() {
        
        if ( _packageName != null ) {
            return _packageName;
        }
        
        _packageName = new TypeFieldModel(this, PACKAGE_NAME_FIELD);
        return _packageName;
        
    }
    
    private TypeFieldModel _name;
    
    public TypeFieldModel name() {
        
        if ( _name != null ) {
            return _name;
        }
        
        _name = new TypeFieldModel(this, NAME_FIELD);
        return _name;
        
    }
    
    private TypeFieldModel _modelUid;
    
    public TypeFieldModel modelUid() {
        
        if ( _modelUid != null ) {
            return _modelUid;
        }
        
        _modelUid = new TypeFieldModel(this, MODEL_UID_FIELD);
        return _modelUid;
        
    }
    
    private TypeFieldModel _fileName;
    
    public TypeFieldModel fileName() {
        
        if ( _fileName != null ) {
            return _fileName;
        }
        
        _fileName = new TypeFieldModel(this, FILE_NAME_FIELD);
        return _fileName;
        
    }
    
    private TypeFieldModel _directoryName;
    
    public TypeFieldModel directoryName() {
        
        if ( _directoryName != null ) {
            return _directoryName;
        }
        
        _directoryName = new TypeFieldModel(this, DIRECTORY_NAME_FIELD);
        return _directoryName;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "PACKAGENAME" : return packageName();
        case "NAME" : return name();
        case "MODELUID" : return modelUid();
        case "FILENAME" : return fileName();
        case "DIRECTORYNAME" : return directoryName();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
