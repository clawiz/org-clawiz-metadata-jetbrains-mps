/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.parser.context;

import org.clawiz.core.common.system.service.Service;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.core.common.utils.StringUtils;

public abstract class AbstractMpsLanguageContext extends Service {

    private String languageName;

    boolean prepared = false;

    public abstract void prepare();

    abstract public String getLanguageName();

    MpsParserClassMapList classMaps = new MpsParserClassMapList();

    protected <T extends AbstractMpsNode> void addClassMap(String mpsClassName, Class<T> nodeClass) {
        if ( mpsClassName == null || nodeClass == null ) {
            throwException("All parameters of addClassMap must be not null : (?, ?)", new Object[]{mpsClassName, nodeClass});
        }

        MpsParserLanguageClassMap classMap = new MpsParserLanguageClassMap(getLanguageName(), mpsClassName, nodeClass);

        classMaps.add(classMap);
    }

    protected <T extends AbstractMpsNode> void addClassMap(Class<T> nodeClass) {
        String[] tokens = StringUtils.splitAndTrim(nodeClass.getName(), "\\.");
        String mpsClassName = tokens[tokens.length-1];
        addClassMap(mpsClassName, nodeClass);
        if ( mpsClassName.substring(0,3).equalsIgnoreCase("Mps")) {
            mpsClassName = mpsClassName.substring(3);
            addClassMap(mpsClassName, nodeClass);
        }

    }

    private void checkPrepare() {
        if ( prepared ) {
            return;
        }
        synchronized (classMaps) {
            classMaps.clear();
            prepare();
            prepared = true;
        }
    }

    public MpsParserClassMapList getClassMaps() {
        checkPrepare();
        return classMaps;
    }
}
