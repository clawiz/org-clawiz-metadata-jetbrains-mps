/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.generator.language;

import org.clawiz.metadata.jetbrains.mps.MpsBase;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.core.common.system.generator.java.AbstractJavaClassGenerator;
import org.clawiz.core.common.utils.StringUtils;

public class AbstractMpsLanguageConceptGenerator extends AbstractJavaClassGenerator {

    MpsLanguageGenerator                          languageGenerator;
    AbstractMpsNode                               node;

    GeneratedLanguage language;

    public MpsLanguageGenerator getLanguageGenerator() {
        return languageGenerator;
    }

    public void setLanguageGenerator(MpsLanguageGenerator languageGenerator) {
        this.languageGenerator = languageGenerator;
    }

    public GeneratedLanguage getLanguage() {
        return language;
    }

    public void setLanguage(GeneratedLanguage language) {
        this.language = language;
    }

    @Override
    public AbstractMpsNode getMetadataNode() {
        return node;
    }

    public AbstractMpsNode getNode() {
        return node;
    }

    public void setMetadataNode(AbstractMpsNode metadataNode) {
        this.node = metadataNode;
    }


    public String getNodePackageNameWithoutLanguage() {
        return MpsBase.removeLastLanguageFromPackageName(getNode().getPackageName());
    }

    public String getMetadataNodeStructurePackageName() {
        String result = getNodePackageNameWithoutLanguage() + ".metadata.data";
        if ( getNode().getVirtualPackage() != null ) {
            result = result + "." + getNode().getVirtualPackage();
        }
        return result;
    }

    @Override
    protected void prepare() {
        super.prepare();

        if ( node.getPackageName() == null ) {
            throwException("Node (model) package not defined. Check mps parser config");
        }

        String nodePackageName      = getNodePackageNameWithoutLanguage() + "." + MpsBase.METADATA_LANGUAGE_ADAPTER_JETBRAINS_MPS_PACKAGE_NAME + ".data"
                + ( !StringUtils.isEmpty(node.getVirtualPackage()) ? "." + node.getVirtualPackage() : "");

        setRootDestinationPath(getLanguageGenerator().getDestinationPath());

        setPackageName(nodePackageName);


    }

}
