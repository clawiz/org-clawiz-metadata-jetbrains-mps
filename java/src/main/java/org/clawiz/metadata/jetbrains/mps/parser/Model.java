/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.parser;

import org.clawiz.core.common.CoreException;
import org.clawiz.metadata.jetbrains.mps.MpsBase;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.metadata.jetbrains.mps.parser.context.MpsParserContext;
import org.clawiz.metadata.jetbrains.mps.parser.data.model.MpsModel;
import org.clawiz.metadata.jetbrains.mps.parser.data.model.MpsModelNodeRef;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 25.04.13
 * Time: 11?4
 * To change this template use File | Settings | File Templates.
 */
public class Model {

    MpsParserContext                  parserContext;

    ArrayList<AbstractMpsNode>        rootNodes = new ArrayList<>();

    HashMap<String, AbstractMpsNode>  nodesIdCache = new HashMap<>();

    HashMap<String, Model>            importedModelsIndexCache = new HashMap<>();

    String packageName;

    public MpsModel getMpsModel() {
        return parserContext.getMpsModel();
    }

    public MpsParserContext getParserContext() {
        return parserContext;
    }

    public void setParserContext(MpsParserContext parserContext) {
        this.parserContext = parserContext;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public ArrayList<AbstractMpsNode> getRootNodes() {
        return rootNodes;
    }

    public HashMap<String, AbstractMpsNode> getNodesIdCache() {
        return nodesIdCache;
    }

    public HashMap<String, Model> getImportedModelsIndexCache() {
        return importedModelsIndexCache;
    }

    public <T extends AbstractMpsNode> T getNodeByName(String name, Class<T> clazz) {
        for (AbstractMpsNode node : getRootNodes()) {
            if (node.getClass().isAssignableFrom(clazz)) {
                if (name.equalsIgnoreCase((node).getName())) {
                    return (T) node;
                }
            }
        }
        return null;
    }

    public String nodeToJavaClassName(AbstractMpsNode node) {
        return MpsBase.removeLastLanguageFromPackageName(node.getModel().getPackageName())
                + "." + MpsBase.METADATA_LANGUAGE_ADAPTER_JETBRAINS_MPS_PACKAGE_NAME
                + ".data"
                + (node.getVirtualPackage() != null ? "." + node.getVirtualPackage() : "")
                + ".Mps" + node.getName();
    }

    public String nodeIdToJavaClassName(String id) {
        AbstractMpsNode node = nodesIdCache.get(id);
        if (node == null) {
            throw new CoreException("Node with id '?' not found in model nodes cache", id);
        }
        return nodeToJavaClassName(node);
    }

    public String nodeRefToJavaClassName(MpsModelNodeRef ref) {
        return nodeIdToJavaClassName(ref.getNode() != null ? ref.getNode() : ref.getTo());
    }


}
