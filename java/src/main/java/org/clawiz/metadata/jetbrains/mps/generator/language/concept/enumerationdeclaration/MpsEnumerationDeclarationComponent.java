/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.generator.language.concept.enumerationdeclaration;

import org.clawiz.core.common.system.generator.java.component.AbstractJavaEnumComponent;
import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.datatype.MpsEnumerationDataTypeDeclaration;
import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.datatype.MpsEnumerationMemberDeclaration;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;

public class MpsEnumerationDeclarationComponent extends AbstractJavaEnumComponent {

    MpsEnumerationDataTypeDeclaration enumeration;

    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        enumeration = ((MpsLanguageEnumerationDeclarationGenerator)generator).getEnumeration();
    }

    public void addGetDefaultValueMethod() {
        if ( enumeration.getDefaultMember() == null ) {
            return;
        }

        JavaMethodElement method = addMethod(getName(), "getDefaultValue");
        method.setType(getName());
        method.setStatic(true);

        method.addText("return " + enumeration.getDefaultMember().getEnumerationValueName().toUpperCase() + ";");

    }

    public void addToConceptNodePropertyStringMethod() {

        JavaMethodElement method = addMethod("String", "toConceptNodePropertyString");
        method.addParameter(this.getName(), "value");
        method.setStatic(true);

        method.addText("if ( value == null ) { ");
        method.addText("    return null;");
        method.addText("}");
        method.addText(" ");
        method.addText("switch (value) {");
        for ( MpsEnumerationMemberDeclaration member: enumeration.getMember() ) {
            method.addText("    case " + member.getEnumerationValueName().toUpperCase() + " : return \"" + member.getEnumerationValueName() + "\";");
        }
        method.addText("}");
        method.addText(" ");

        method.addText("return null;");
    }


    @Override
    public void process() {
        super.process();

        setName("Mps" + getGenerator().getMetadataNode().getName());

        setExtends(getName() + "Prototype");

        for ( MpsEnumerationMemberDeclaration member: enumeration.getMember() ) {
            addValue( member.getEnumerationValueName().toUpperCase() );
        }

        addGetDefaultValueMethod();

        addStringToEnumMethod("\"Wrong " + getName() +  " value '?\", string");


        addToConceptNodePropertyStringMethod();

    }


}
