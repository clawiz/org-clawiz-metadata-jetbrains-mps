/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.data;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.metadata.jetbrains.mps.generator.solution.MpsSolutionGeneratorContext;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.AbstractMpsSolutionXMLElement;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.concept.MpsSolutionConceptNodeXMLElement;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.concept.MpsSolutionConceptPropertyXMLElement;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.concept.MpsSolutionConceptRefXMLElement;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.registry.AbstractMpsSolutionRegistryIndexedElement;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.registry.MpsSolutionRegistryLanguageConceptPropertyXMLElement;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.registry.MpsSolutionRegistryLanguageConceptXMLElement;
import org.clawiz.metadata.jetbrains.mps.parser.Model;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.reflection.ReflectionUtils;
import org.clawiz.metadata.jetbrains.mps.parser.data.model.MpsModelRegistry;
import org.clawiz.metadata.jetbrains.mps.parser.data.model.MpsModelLink;
import org.clawiz.metadata.jetbrains.mps.parser.data.model.MpsModelNode;
import org.clawiz.metadata.jetbrains.mps.parser.context.MpsParserContext;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import static org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.registry.MpsSolutionRegistryXMLElement.JETBRAINS_MPS_LANG_CORE_BASECONCEPT_CONCEPT_ID;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 25.04.13
 * Time: 11?1
 * To change this template use File | Settings | File Templates.
 */
public class AbstractMpsNode extends MetadataNode {

    String           mpsNodeId;
    String           virtualPackage;
    Session          session;

    MpsModelNode     mpsModelNode;
    ReflectionUtils  reflectionUtils;

    MpsParserContext            parserContext;
    MpsSolutionGeneratorContext solutionGeneratorContext;

    AbstractMpsNode  parent;
    Model            model;

    ArrayList<AbstractMpsNode>       mpsModelChildNodes = new ArrayList<>();
    ArrayList<AbstractMpsNode>       mpsModelLinkedNodes = new ArrayList<>();
    ArrayList<MpsModelLink>          allMpsModelLinks = new ArrayList<>();
    HashMap<String, AbstractMpsNode> mpsModelForeignKeys = new HashMap<>();

    MpsModelRegistry modelRegistry;

    public <T extends MetadataNode> T createMetadataNode(Class<T> clazz, MetadataNode parentNode, String parentNodeFieldName) {
        try {
            T node;
            node = clazz.newInstance();
            node.setName(getName());
            node.setParentNode(parentNode, parentNodeFieldName);
            node.setPackageName(getFullPackageName());
            node.setMetadataStoragePackageName(getPackageName());
            getParserContext().registerMetadataNodeMpsNodeId(getMpsNodeId(), node);
            return node;
        } catch (Exception e) {
            throw new CoreException("Exception on create new metadata node of class ? : ?", clazz.getName(), e.getMessage(), e);
        }
    }

    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return null;
    }

    protected void fillMetadataNode(MetadataNode node) {

    }

    private MetadataNode _metadataNode;
    public MetadataNode toMetadataNode() {
        return toMetadataNode(null);
    }

    public MetadataNode toMetadataNode(MetadataNode parentNode) {
        return toMetadataNode(parentNode, null);
    }

    public MetadataNode toMetadataNode(MetadataNode parentNode, String parentNodeFieldName) {

        if ( getMetadataNodeClass() != null ) {
            if ( _metadataNode != null ) {
                return _metadataNode;
            }

            try {
                MetadataNode node = createMetadataNode(getMetadataNodeClass(), parentNode, parentNodeFieldName);

                fillMetadataNode(node);
                _metadataNode = node;
                return _metadataNode;
            } catch (Exception e) {

                throw new CoreException("Exception on create node class ? instance : ?", getMetadataNodeClass().getName(), e.getMessage(), e);
            }

        }


        throw new CoreException("Method getMetadataNodeClass or toMetadataNode not implemented in ?", this.getClass().getName());
    }

    public <T extends AbstractMpsNode> T loadChildMetadataNode(MetadataNode metadataNode ) {

        Class<T> mpsNodeClass = getParserContext().getMetadataNodeToMpsNodeClassMapsCache().get(metadataNode.getClass());
        if ( mpsNodeClass == null ) {
            throwException("Target solution mps node class not defined for metadata node class '?'\n check if language added to mps solution model", metadataNode.getClass());
        }
        return loadChildMetadataNode(mpsNodeClass, metadataNode);
    }

    public <T extends AbstractMpsNode> T loadChildMetadataNode(Class<T> clazz, MetadataNode metadataNode ) {
        try {
            T childNode;
            try {
                childNode = clazz.newInstance();
            } catch (Exception e) {
                throw e;
            }
            childNode.setModel(getModel());
            childNode.setPackageName(getPackageName());
            childNode.setParserContext(getParserContext());
            childNode.setSolutionGeneratorContext(getSolutionGeneratorContext());
            childNode.setParent(this);
            childNode.setSession(getSession());
            childNode.setName(metadataNode.getName());
            try {
                childNode.loadMetadataNode(metadataNode);
            } catch (Exception e) {
                throw e;
            }
            return childNode;
        } catch (Exception e) {
            throw new CoreException("Exception on create node class ? : ?", clazz.getName(), e.getMessage(), e);
        }
    }

    public void loadMetadataNode(MetadataNode node) {
        throw new CoreException("Method " + this.getClass().getName() + ".loadMetadataNode not implemented");
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
        model.getNodesIdCache().put(mpsNodeId, this);
    }

    @Override
    public String getPackageName() {
        return super.getPackageName() != null ? super.getPackageName()
                : ( model != null ? model.getPackageName() : null );
    }

    public AbstractMpsNode getParent() {
        return parent;
    }

    public void setParent(AbstractMpsNode parent) {
        this.parent = parent;
    }

    public String getMpsNodeId() {
        return mpsNodeId;
    }
    public void setMpsNodeId(String mpsNodeId) {
        this.mpsNodeId = mpsNodeId;
    }

    public String getVirtualPackage() {
        return virtualPackage;
    }

    public void setVirtualPackage(String virtualPackage) {
        this.virtualPackage = virtualPackage;
    }

    public String getFullPackageName() {
        return getPackageName() + ( !StringUtils.isEmpty(getVirtualPackage()) ? "." + getVirtualPackage() : "");
    }

    public HashMap<String, AbstractMpsNode> getMpsModelForeignKeys() {
        return mpsModelForeignKeys;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public <T extends Service> T getService(Class<T> clazz) {
        return session.getService(clazz);
    }

    protected ReflectionUtils getReflectionUtils() {
        if ( reflectionUtils != null ) {
            return reflectionUtils;
        }
        reflectionUtils = session.getService(ReflectionUtils.class);
        return reflectionUtils;
    }

    public MpsModelNode getMpsModelNode() {
        return mpsModelNode;
    }

    public void setMpsModelNode(MpsModelNode mpsModelNode) {
        this.mpsModelNode = mpsModelNode;
    }



    public void addChild(AbstractMpsNode node, String role) {

        String foundFieldName       = null;
        String regsitryFieldName    = null;

        Field  field     = getReflectionUtils().findField(this.getClass(), role);
        if ( field != null ) {
            foundFieldName = role;
        } else {
            regsitryFieldName = node.getModelRegistry().indexToName(role);
            field = getReflectionUtils().findField(this.getClass(), regsitryFieldName);
            if ( field == null ) {
                field = getReflectionUtils().findField(this.getClass(), "_" + regsitryFieldName);
                if ( field == null ) {
                    throw new CoreException("Field '?' or '?' for role '?' not found in '?'", new Object[]{regsitryFieldName, "_" + regsitryFieldName, role, this.getClass().getName()});
                }
            }
            foundFieldName = regsitryFieldName;
        }

        Method getMethod = getReflectionUtils().findMethod(this.getClass(), "get" + foundFieldName);
        if ( getMethod == null ) {
            throw new CoreException("Get method '?' for field '?' not found in '?'", new Object[]{"get" + foundFieldName, foundFieldName, this.getClass()});
        }

        Object obj;
        try {
            obj = getMethod.invoke(this);
        } catch (Exception e) {
            throw new CoreException("Method GET for role '?' in '?' call exception : ? ", new Object[]{foundFieldName, this.getClass(), e.getMessage()}, e);
        }

        if ( Collection.class.isAssignableFrom(field.getType()) ) {
            if ( obj == null ) {
                throw new CoreException("Collection not initialized for role '?' in '?' field '?'", new Object[]{foundFieldName, this.getClass(), field.getName()});
            }
            try {
                ( (Collection<AbstractMpsNode>) obj ).add(node);
            } catch (Exception e) {
                throw new CoreException("Add item to collection for role '?' in '?' field '?' exception : ? "
                        , new Object[]{role, this.getClass(), field.getName(), e.getMessage()}, e);
            }
        } else {
            if ( obj != null ) {
                throw new CoreException("Object for role '?' in '?' field '?' already defined in (?) ", new Object[]{foundFieldName, this.getClass(), field.getName(), node.toString()});
            }
            Method setMethod = getReflectionUtils().findMethod(this.getClass(), "set" + foundFieldName);
            if ( setMethod == null ) {
                setMethod = getReflectionUtils().findMethod(this.getClass(), "set" + node.getModelRegistry().indexToName(role));
                if ( setMethod == null ) {
                    throw new CoreException("set method '?' for field '?' not found in '?'", new Object[]{"get" + foundFieldName, foundFieldName, this.getClass()});
                }
            }

            getReflectionUtils().invokeVoid(setMethod, this, node);


        }

        mpsModelChildNodes.add(node);

    }

    @Override
    public String toString() {
        if ( getName() != null ) {
            return getName() + "(" + mpsNodeId + ")";
        } else {
            return mpsNodeId ;
        }
    }

    public ArrayList<AbstractMpsNode> getMpsModelChildNodes() {
        return mpsModelChildNodes;
    }

    public ArrayList<AbstractMpsNode> getMpsModelLinkedNodes() {
        return mpsModelLinkedNodes;
    }

    public ArrayList<MpsModelLink> getAllMpsModelLinks() {
        return allMpsModelLinks;
    }

    public void fillForeignKeys() {

    }

    boolean childsForeignKeysFilled = false;
    public void addChildForeignKeys(HashMap<String, AbstractMpsNode> path) {

        if ( childsForeignKeysFilled ) {
            return;
        }

        if ( path.containsKey(this.getMpsNodeId())) {
            return;
        }

        path.put(this.getMpsNodeId(), this);
        for (AbstractMpsNode child : mpsModelChildNodes) {
            child.addChildForeignKeys(path);
        }
        for (AbstractMpsNode child : mpsModelForeignKeys.values()) {
            child.addChildForeignKeys(path);
        }
        path.remove(this.getMpsNodeId());


        for (AbstractMpsNode child : mpsModelChildNodes) {
            for ( AbstractMpsNode fkn: child.getMpsModelForeignKeys().values()) {
                addForeignKey(fkn);
            }
        }

//        Collection<AbstractNode> fkNodes = mpsModelForeignKeys.values();
        ArrayList<AbstractMpsNode> fkNodes = new ArrayList<>();
        for ( AbstractMpsNode node : mpsModelForeignKeys.values()) {
            fkNodes.add(node);
        }
        for (AbstractMpsNode child : fkNodes ) {
            for ( AbstractMpsNode fkn: child.getMpsModelForeignKeys().values()) {
                addForeignKey(fkn);
            }
        }

        childsForeignKeysFilled = true;

    }

    public MpsParserContext getParserContext() {
        return parserContext;
    }

    public void setParserContext(MpsParserContext parserContext) {
        this.parserContext = parserContext;
    }

    public MpsSolutionGeneratorContext getSolutionGeneratorContext() {
        return solutionGeneratorContext;
    }

    public void setSolutionGeneratorContext(MpsSolutionGeneratorContext solutionGeneratorContext) {
        this.solutionGeneratorContext = solutionGeneratorContext;
    }

    public void addForeignKey(AbstractMpsNode node) {
        if ( node == null ) {
            return;
        }
        if ( mpsModelForeignKeys.containsKey(node.getMpsNodeId())) {
            return;
        }
        addForeignKey(node.getParent());
        mpsModelForeignKeys.put(node.getMpsNodeId(), node);
    }

    public boolean isForeignKeysContain(AbstractMpsNode node) {
        if ( node == null ) {
            return false;
        }
        return mpsModelForeignKeys.containsKey(node.getMpsNodeId());
    }

    public <T extends AbstractMpsNode> ArrayList<T> getChilds(Class<T> clazz) {
        ArrayList<T> result = new ArrayList<>();

        for ( AbstractMpsNode node : mpsModelChildNodes) {
            if ( node.getClass() == clazz) {
                result.add((T) node);
            }
        }

        return result;
    }

    public <T extends AbstractMpsNode> T getChild(Class<T> clazz) {
        ArrayList<T> childs = getChilds(clazz);
        if ( childs.size() > 1 ) {
            throw new CoreException("Node '?' have more than one child of class '?'", new Object[]{toString(), clazz});
        }

        if ( childs.size() == 0 ) {
            return null;
        }
        return childs.get(0);
    }

    public <T extends AbstractMpsNode> boolean isChildExists(Class<T> clazz) {
        for ( AbstractMpsNode node : mpsModelChildNodes) {
            if ( node.getClass() == clazz) {
                return true;
            }
        }
        return false;
    }


    public MpsModelRegistry getModelRegistry() {
        return modelRegistry;
    }

    public void setModelRegistry(MpsModelRegistry modelRegistry) {
        this.modelRegistry = modelRegistry;
    }

    public String getLanguageName() {
        return null;
    }

    public String getLanguageId() {
        return null;
    }

    public String getLanguageConceptName() {
        return null;
    }

    public String getLanguageConceptId() {
        return null;
    }

    public enum  ConceptPropertyType {
        PROPERTY, REFERENCE, CHILD
    }
    public class ConceptProperty {

        String languageId;
        String languageName;

        String languageConceptId;
        String languageConceptName;

        ConceptPropertyType type;
        String id;
        String name;

        public ConceptProperty(String languageId, String languageName, String languageConceptId, String languageConceptName, ConceptPropertyType type, String id, String name) {
            this.languageId = languageId;
            this.languageName = languageName;
            this.languageConceptId = languageConceptId;
            this.languageConceptName = languageConceptName;
            this.type = type;
            this.id = id;
            this.name = name;
        }
    }
    public ArrayList<ConceptProperty> getConceptProperties() {
        throw new CoreException("Method ?.getConceptProperties not implemented", this.getClass().getName());
    }

    MpsSolutionRegistryLanguageConceptXMLElement ___languageConcept;
    public static final String JETBRAINS_MPS_CORE_BASECONCEPT_VIRTUAPACKAGE = "jetbrains.mps.lang.core.BaseConcept.virtualPackage";
    public MpsSolutionConceptNodeXMLElement saveToXMLElement(AbstractMpsSolutionXMLElement parentElement) {

        if ( ___languageConcept == null ) {

            MpsSolutionRegistryLanguageConceptXMLElement concept = parentElement.getRegistry().getLanguageConceptNamesCache().get(getLanguageConceptName());
            if ( concept == null ) {


                concept = parentElement.getRegistry().registerLanguage(getLanguageId(), getLanguageName())
                        .registerConcept(getLanguageConceptId(), getLanguageConceptName());
                for ( ConceptProperty property : getConceptProperties() ) {

                    MpsSolutionRegistryLanguageConceptPropertyXMLElement.PropertyType propertyType = null;
                    switch (property.type) {
                        case CHILD:     propertyType = MpsSolutionRegistryLanguageConceptPropertyXMLElement.PropertyType.CHILD; break;
                        case PROPERTY:  propertyType = MpsSolutionRegistryLanguageConceptPropertyXMLElement.PropertyType.PROPERTY; break;
                        case REFERENCE: propertyType = MpsSolutionRegistryLanguageConceptPropertyXMLElement.PropertyType.REFERENCE; break;
                        default: throw new CoreException("Wrong concept ? property type ?", getLanguageConceptName(), property.type.toString());
                    }
                    parentElement.getRegistry().registerLanguage(property.languageId, property.languageName)
                            .registerConcept(property.languageConceptId, property.languageConceptName)
                            .registerProperty(propertyType, property.id, property.name);

                }

                parentElement.getRegistry().getLanguageConceptNamesCache().put(getLanguageConceptName(), concept);

            }
            ___languageConcept = concept;
        }

        MpsSolutionConceptNodeXMLElement element = parentElement.addElement(MpsSolutionConceptNodeXMLElement.class);

        element.getRegistry().prepareConceptNodeId(element);
        element.getRegistry().getMpsNodeToIdCache().put(this, element.getConceptId());
        element.setConcept(___languageConcept.getIndex());

        ___currentNodeElement = element;

        addConceptNodeProperty(JETBRAINS_MPS_LANG_CORE_BASECONCEPT_CONCEPT_ID, "virtualPackage", getVirtualPackage());

        fillConceptNode();

        return element;

    }

    MpsSolutionConceptNodeXMLElement ___currentNodeElement;

    public String getConceptNodeIndex(String conceptId, String name) {
        AbstractMpsSolutionRegistryIndexedElement indexedElement = this.___currentNodeElement.getComponent().getRegistry().getConceptIdToElementCache().get(conceptId);
        if ( indexedElement == null ) {
            throw new CoreException("Concept with id ? not found in model registry", conceptId);
        }
        if ( ! (indexedElement instanceof  MpsSolutionRegistryLanguageConceptXMLElement) ){
            throw new CoreException("Concept with id ? (?) class ? not MpsSolutionRegistryLanguageConceptXMLElement", conceptId, indexedElement.getName(), indexedElement.getClass().getName());
        }
        MpsSolutionRegistryLanguageConceptXMLElement languageConcept = (MpsSolutionRegistryLanguageConceptXMLElement) indexedElement;
        String index = languageConcept.getPropertyIndex(name);
        if ( index == null ) {
                throw new CoreException("Unknown language '?' concept property name '?'", getLanguageConceptName(), name);
        }
        return index;
    }

    public String toQuotetString(String string) {
        return string != null ? string
                .replaceAll("&", "&amp;")
                .replaceAll("\\n", "#!#lf#!#")
//                .replaceAll("\\r", "#!#lf#!#")
//                .replaceAll("\\\\", "#!#backslash#!#")
//                .replaceAll("\\n", "\\\\n")
//                .replaceAll("&laquo;", "&quot;")
                .replaceAll("\"", "&quot;")
                .replaceAll("<", "&lt;")
                .replaceAll(">", "&gt;") : string;
    }


    public void addConceptNodeProperty(String conceptId, String name, String value) {
        if ( value == null ) {
            return;
        }

        MpsSolutionConceptPropertyXMLElement element = ___currentNodeElement.addElement(MpsSolutionConceptPropertyXMLElement.class);
        element.setRole(getConceptNodeIndex(conceptId, name));

        element.setValue(toQuotetString(value));

    }

    public void addConceptNodeChild(String conceptId, String name, AbstractMpsNode node) {
        if ( node == null ) {
            return;
        }
        node.saveToXMLElement(___currentNodeElement).setRole(getConceptNodeIndex(conceptId, name));
    }

    public void addConceptNodeRef(String conceptId, String name, AbstractMpsNode node) {
        if ( node == null ) {
            return;
        }
        MpsSolutionConceptRefXMLElement ref = ___currentNodeElement.addElement(MpsSolutionConceptRefXMLElement.class);
        ref.setRole(getConceptNodeIndex(conceptId, name));
        ref.setResolve(toQuotetString(node.getName()));
        String refId = ___currentNodeElement.getNodeConceptId(node);
        if ( refId == null ) {
            ___currentNodeElement.getComponent().getGenerator().getContext().addDeferredConceptRefResolve(___currentNodeElement, ref, node);
        } else {
            ref.setNode(refId);
        }
    }

    public void fillConceptNode() {
        throw new CoreException("Method ?.fillConceptNode not implemented", this.getClass().getName());
    }

}
