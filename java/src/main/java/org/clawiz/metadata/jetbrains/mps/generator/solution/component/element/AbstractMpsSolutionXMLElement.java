/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.generator.solution.component.element;

import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.system.generator.xml.component.element.AbstractXMLElement;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.metadata.jetbrains.mps.generator.solution.MpsSolutionGenerator;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.MpsSolutionNodeModelXMLComponent;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.registry.MpsSolutionRegistryXMLElement;
import org.clawiz.metadata.jetbrains.mps.parser.Model;

public abstract class AbstractMpsSolutionXMLElement extends AbstractXMLElement {

    MpsSolutionNodeModelXMLComponent solutionComponent;
    MpsSolutionRegistryXMLElement    registry;

    Model                            solutionModel;

    public Model getSolutionModel() {
        if ( solutionModel != null ) {
            return solutionModel;
        }
        solutionModel = getComponent().getGenerator().getContext().getSolutionModel();
        return solutionModel;
    }

    public MpsSolutionRegistryXMLElement getRegistry() {
        if ( registry != null ) {
            return registry;
        }
        registry      = getComponent().getRegistry();
        return registry;
    }

    @Override
    public MpsSolutionNodeModelXMLComponent getComponent() {
        return solutionComponent;
    }

    @Override
    public void setComponent(AbstractComponent component) {
        super.setComponent(component);
        solutionComponent = (MpsSolutionNodeModelXMLComponent) component;
    }

    public String getNodeConceptId(AbstractMpsNode node) {
        String id = getRegistry().getMpsNodeToIdCache().get(node);
        if ( id != null ) {
            return id;
        }
        return solutionComponent.getGenerator().getNodeConceptId(node);
    }

}
