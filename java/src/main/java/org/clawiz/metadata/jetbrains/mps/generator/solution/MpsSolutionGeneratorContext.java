/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.generator.solution;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeList;
import org.clawiz.core.common.system.module.Module;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.concept.MpsSolutionConceptNodeXMLElement;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.concept.MpsSolutionConceptRefXMLElement;
import org.clawiz.metadata.jetbrains.mps.parser.Model;
import org.clawiz.metadata.jetbrains.mps.parser.context.MpsParserContext;
import org.clawiz.metadata.jetbrains.mps.parser.context.MpsParserLanguageClassMap;

import java.util.ArrayList;
import java.util.HashMap;

public class MpsSolutionGeneratorContext {

    MetadataNodeList        sourceNodes   = new MetadataNodeList();
    Model                   model = new Model();
    Model                   solutionModel;
    MpsParserContext        parserContext;

    String                  destinationPath;
    String                  destinationRootPackageName;
    String                  sourceRootPackageName;
//    Class                   moduleClass;

    public <S extends MetadataNode, T extends AbstractMpsNode> Class<T> getTargetClassMap(Class<S> sourceClass) {
        return getParserContext().getMetadataNodeToMpsNodeClassMapsCache().get(sourceClass);
    }

    public MetadataNodeList getSourceNodes() {
        return sourceNodes;
    }

    public void setSourceNodes(MetadataNodeList sourceNodes) {
        this.sourceNodes = sourceNodes;
    }

    public Model getModel() {
        return model;
    }

    public Model getSolutionModel() {
        return solutionModel;
    }

    public void setSolutionModel(Model solutionModel) {
        this.solutionModel = solutionModel;
    }

    public MpsParserContext getParserContext() {
        return parserContext;
    }

    public void addSourceNode(MetadataNode node) {
        sourceNodes.add(node);
    }

    public void addSourceNodes(MetadataNodeList nodes) {
        sourceNodes.addAll(nodes);
    }

    public String getDestinationPath() {
        return destinationPath;
    }

    public void setDestinationPath(String destinationPath) {
        this.destinationPath = destinationPath;
    }

    public String getDestinationRootPackageName() {
        return destinationRootPackageName != null ? destinationRootPackageName : sourceRootPackageName;
    }

    public void setDestinationRootPackageName(String destinationRootPackageName) {
        this.destinationRootPackageName = destinationRootPackageName;
    }

    public String getSourceRootPackageName() {
        return sourceRootPackageName;
    }

    public void setSourceRootPackageName(String sourceRootPackageName) {
        this.sourceRootPackageName = sourceRootPackageName;
    }

/*
    public <T extends Module> Class<T> getModuleClass() {
        return moduleClass;
    }

    public <T extends Module> void setModuleClass(Class<T> moduleClass) {
        this.moduleClass = moduleClass;
    }
*/

    private HashMap<String, String> fileNamesCache = new HashMap<>();
    public String getFreeFileName(String fileName) {

        String name   = fileName != null ? fileName : "null";
        int    index = 2;

        while ( fileNamesCache.containsKey(name.toUpperCase()) ) {
            name = fileName + "_" + index++;
        }
        fileNamesCache.put(name.toUpperCase(), name.toUpperCase());

        return name;
    }

    public class DeferredConceptRefResolve {
        MpsSolutionConceptNodeXMLElement nodeXMLElement;
        MpsSolutionConceptRefXMLElement refXMLElement;
        AbstractMpsNode                 node;

        public DeferredConceptRefResolve(MpsSolutionConceptNodeXMLElement nodeXMLElement, MpsSolutionConceptRefXMLElement refXMLElement, AbstractMpsNode node) {
            this.nodeXMLElement = nodeXMLElement;
            this.refXMLElement = refXMLElement;
            this.node = node;
        }
    }

    protected ArrayList<DeferredConceptRefResolve> deferredConceptRefResolves = new ArrayList<>();
    public void addDeferredConceptRefResolve(MpsSolutionConceptNodeXMLElement nodeXMLElement, MpsSolutionConceptRefXMLElement refXMLElement, AbstractMpsNode node) {
        deferredConceptRefResolves.add(new DeferredConceptRefResolve(nodeXMLElement, refXMLElement, node));
    }


    public ArrayList<DeferredConceptRefResolve> getDeferredConceptRefResolves() {
        return deferredConceptRefResolves;
    }

    public class DeferredSolutionNodeResolve {
        public AbstractMpsNode mpsNode;
        public String          fieldName;
        public boolean         isArray;
        public MetadataNode    referencedMetadataNode;
    }
    ArrayList<DeferredSolutionNodeResolve> deferredSolutionNodeResolves = new ArrayList<>();
    public void addDeferredSolutionNodeResolve(AbstractMpsNode mpsNode, String fieldName, boolean isArray, MetadataNode referencedMetadataNode) {

        if ( referencedMetadataNode == null ) {
            return;
        }

        DeferredSolutionNodeResolve resolve = new DeferredSolutionNodeResolve();
        deferredSolutionNodeResolves.add(resolve);

        resolve.mpsNode               = mpsNode;
        resolve.fieldName             = fieldName;
        resolve.isArray                = isArray;
        resolve.referencedMetadataNode = referencedMetadataNode;
    }

    public ArrayList<DeferredSolutionNodeResolve> getDeferredSolutionNodeResolves() {
        return deferredSolutionNodeResolves;
    }


    public void prepareTargeClassMaps() {
        for (MpsParserLanguageClassMap classMap : getSolutionModel().getParserContext().getLanguageClassMaps()) {
            try {
                AbstractMpsNode mpsNode = (AbstractMpsNode) classMap.getNodeClass().newInstance();
                getParserContext().getMetadataNodeToMpsNodeClassMapsCache().put(mpsNode.getMetadataNodeClass(), classMap.getNodeClass());
            } catch (Exception e) {
                throw new CoreException("Exception on create mps node class ? instance : ?", classMap.getNodeClass().getName(), e.getMessage(), e);
            }
        }

    }
    public void prepare(Session session) {

        model.setPackageName(sourceRootPackageName);

        parserContext = new MpsParserContext(session);

        parserContext.setModel(model);
        model.setParserContext(parserContext);

    }
}
