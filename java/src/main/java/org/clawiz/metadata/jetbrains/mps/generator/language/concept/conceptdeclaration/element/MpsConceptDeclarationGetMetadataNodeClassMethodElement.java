package org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration.element;

import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsConceptDeclarationGetMetadataNodeClassMethodElement extends AbstractMpsConceptDeclarationMethodElement {

    @Override
    public void process() {
        super.process();

        setName("getMetadataNodeClass");
        setAccessLevel(AccessLevel.PUBLIC);

        getComponent().addImport(MetadataNode.class);
        addGeneric("T extends MetadataNode");
        setType("Class<T>");


        if ( getLanguage().getRefConcept(getDataStructureClassName()) != null ) {
            addText("return null;");
        } else  {
            addText("return (Class<T>) " + getDataStructureClassName() + ".class;");
        }



    }
}
