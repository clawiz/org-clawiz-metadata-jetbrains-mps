/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.install.model;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeList;
import org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeObject;
import org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType;
import org.clawiz.core.common.metadata.MetadataBase;
import org.clawiz.core.common.metadata.install.MetadataNodeListInstaller;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeList;
import org.clawiz.core.common.metadata.node.storage.MetadataNodeResourcesXMLStorage;
import org.clawiz.core.common.storage.type.TypeObject;
import org.clawiz.core.common.storage.type.TypeService;
import org.clawiz.core.common.system.installer.AbstractInstaller;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.metadata.jetbrains.mps.MpsBase;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.metadata.jetbrains.mps.parser.Model;
import org.clawiz.metadata.jetbrains.mps.parser.MpsParser;
import org.clawiz.metadata.jetbrains.mps.parser.context.MpsParserContext;
import org.clawiz.metadata.jetbrains.mps.storage.MpsMetadataStorage;
import org.clawiz.metadata.jetbrains.mps.storage.mpsmodel.MpsModelObject;
import org.clawiz.metadata.jetbrains.mps.storage.mpsmodel.MpsModelService;

import java.math.BigDecimal;

import static org.clawiz.core.common.system.Install.PARAMETER_INCREMENTAL;
import static org.clawiz.metadata.jetbrains.mps.storage.MpsMetadataStorage.STORAGE_TYPE_DIRECTORY;

public class JetbrainsMPSModelInstaller extends AbstractInstaller {

    public static final String PROCESS_DEFERRED_PROCESS_RESOLVE_EXTENSION_NAME = "processDeferredProcessResolve";

    MpsParser                       mpsParser;
    MpsModelService                 modelService;

    String                          modelPath;
    Model                           model;

    MetadataNodeList                metadataNodeList;
    MetadataNodeResourcesXMLStorage metadataNodeResourcesXMLStorage;
    MetadataBase                    metadataBase;

    @NotInitializeService
    MetadataNodeListInstaller metadataNodeListInstaller;

    public String getModelPath() {
        return modelPath;
    }

    public void setModelPath(String modelPath) {
        this.modelPath = modelPath;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }


    @NotInitializeService
    private TypeService typeService;
    public TypeService getTypeService() {
        if ( typeService == null ) {
            typeService = getService(TypeService.class);
        }
        return typeService;
    }

    protected void saveTypeHeaders() {

        for (AbstractMpsNode mpsNode : model.getRootNodes()) {

            if ( ! (mpsNode instanceof MpsType) ) {
                continue;
            }

            MpsType mpsType = (MpsType) mpsNode;
            if ( ! mpsType.isInstall() ) {
                continue;
            }
            logDebug("Save type header for " + mpsType.getFullName());

            String packageName = mpsType.getPackageName();
            if ( mpsType.getVirtualPackage() != null ) {
                packageName = packageName + "." + mpsType.getVirtualPackage();
            }
            BigDecimal typeId = getTypeService().packageNameToId(packageName, mpsType.getName());
            if ( typeId == null ) {
                TypeObject type = getTypeService().create();

                type.setPackageName(packageName);
                type.setName(mpsType.getName());
                type.setJavaName(mpsType.getJavaName() != null ? mpsType.getJavaName() : mpsType.getName());
                type.setTableName(mpsType.getTableName());

                type.save();

                typeId = type.getId();
            }


        }
    }

    public String getMetadataUrl() {
        return STORAGE_TYPE_DIRECTORY + ":" + getModelPath();
    }

    protected void saveProcessHeaders() {

/*
        ProcessService      processService      = getService(ProcessService.class);
        ProcessRuleService  processRuleService  = getService(ProcessRuleService.class);
        ProcessStateService processStateService = getService(ProcessStateService.class);

        for (AbstractMpsNode mpsNode : model.getRootNodes()) {

            if (!(mpsNode instanceof MpsProcess)) {
                continue;
            }

            logDebug("Save process header for " + mpsNode.getFullName()
            );

            MpsProcess mpsProcess = (MpsProcess) mpsNode;
            BigDecimal processId = processService.packageNameToId(mpsProcess.getFullPackageName(), mpsProcess.getName());
            if ( processId == null ) {
                processId = processService.packageNameToId(mpsProcess.getFullPackageName(), mpsProcess.getName(), true);
                Process process = new Process();
                process.setId(processId);
                process.setPackageName(mpsProcess.getFullPackageName());
                process.setName(mpsProcess.getName());

                metadataBase.saveNode(process, MpsMetadataStorage.class, getMetadataUrl());
            }

            for(MpsProcessRule mpsRule : mpsProcess.getRules() ) {
                processRuleService.processNameToId(processId, mpsRule.getName(), true);
            }

            for ( MpsProcessState mpsState : mpsProcess.getNodes() ) {
                processStateService.processNameToId(processId, mpsState.getName(), true);
            }

        }
*/
    }

    protected void fillNodeList() {

        metadataNodeList = new MetadataNodeList();
        for (AbstractMpsNode mpsNode : model.getRootNodes()) {
            if (mpsNode instanceof MpsType && !((MpsType) mpsNode).isInstall()) {
                continue;
            }
            metadataNodeList.add(mpsNode.toMetadataNode());
        }

    }



    private void prepareNodeList() {

        for ( Object object : metadataNodeList ) {
            MetadataNode node = (MetadataNode) object;
            if ( getDestinationPath() != null ) {
                metadataNodeResourcesXMLStorage.saveNode(node, getDestinationPath());
            } else {
                metadataBase.saveNode(node, metadataNodeResourcesXMLStorage.getClass(), metadataNodeResourcesXMLStorage.getNodeResourcesUrl(node));
            }
        }

        for ( Object object : metadataNodeList ) {
            ((MetadataNode) object).prepareNode(getSession());
        }

        metadataNodeList.sort();

    }

    protected void processDeferredQueues(MpsParserContext context) {

        for (AbstractMpsNode node : context.getDeferredTypesResolveMpsNodesQueue()) {

            if (node instanceof MpsValueTypeObject) {

                MpsType referencedType    = ((MpsValueTypeObject) node).getReferencedType();
                ValueTypeObject valueType = (ValueTypeObject) ((MpsValueTypeObject) node).toMetadataNode();

                if ( referencedType != null ) {
                    valueType.setReferencedType((Type) context.getNodeByClass(MpsType.class, referencedType).toMetadataNode());
                }

            } else if (node instanceof MpsValueTypeList) {

                MpsType referencedType  = ((MpsValueTypeList) node).getReferencedType();
                ValueTypeList valueType = (ValueTypeList) ((MpsValueTypeList) node).toMetadataNode();

                valueType.setReferencedType((Type) context.getNodeByClass(MpsType.class, referencedType).toMetadataNode());



            } else {
                throwException("Wrong deferredTypesResolveMpsNodesQueue node class ?", node.getClass().getName());
            }
        }

/*
        for ( AbstractMpsNode node : context.getDeferredStructureResolveMpsNodesQueue() ) {

            if ( node instanceof MpsStructureValueTypeStructureReference ) {

                MpsStructureValueTypeStructureReference mpsVt = (MpsStructureValueTypeStructureReference) node;

                context.getNodeByClass(MpsStructure.class, mpsVt.getReferencedStructure());
                ((StructureValueTypeStructureReference) mpsVt.toMetadataNode())
                        .setReferencedStructure((Structure) context.getNodeByClass(MpsStructure.class, mpsVt.getReferencedStructure()).toMetadataNode());


            } else {
                throwException("Wrong deferredStructureResolveMpsNodesQueue node class ?", node.getClass().getName());
            }

        }
*/

        metadataNodeList.sort();

    }


    @Override
    public void process() {


        setIncrementalMode(getParameterAsBoolean(PARAMETER_INCREMENTAL));
        setModel(mpsParser.parseDirectory(getModelPath(), getPackageName()));

        saveTypeHeaders();
        saveProcessHeaders();
        commit();

        fillNodeList();
        getService(MpsMetadataStorage.class).processDeferredParseNodeResolves(getModel().getParserContext());
        processDeferredQueues(getModel().getParserContext());
        prepareNodeList();

        BigDecimal modelId         = modelService.modelUidToId(model.getMpsModel().getModelUID());
        MpsModelObject modelObject = modelId != null ? modelService.load(modelId) : modelService.create();

        modelObject.setModelUid(model.getMpsModel().getModelUID());
        modelObject.setPackageName(getPackageName());
        modelObject.setName(modelObject.getModelUid());
        modelObject.setDirectoryName(getModelPath());

        modelObject.save();

        if ( getDestinationPath() != null ) {
            for (Object object : metadataNodeList ) {
                metadataNodeResourcesXMLStorage.saveNode((MetadataNode) object, getDestinationPath());
            }
        }

        metadataNodeListInstaller = addInstaller(MetadataNodeListInstaller.class);
        metadataNodeListInstaller.setNodes(metadataNodeList);
        getInstallers().remove(getInstallers().size()-1);
        metadataNodeListInstaller.run();

        getService(MetadataBase.class).clearCaches();
        getService(MpsParser.class).clearCaches();
        getService(MpsBase.class).clearCaches();


        logDebug("MPS model " + getModelPath() + "/" +  getPackageName() + " installed ");

    }

}
