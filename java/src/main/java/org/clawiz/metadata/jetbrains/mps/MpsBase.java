/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps;

import org.clawiz.core.common.Core;
import org.clawiz.core.common.CoreException;
import org.clawiz.metadata.jetbrains.mps.parser.Model;
import org.clawiz.metadata.jetbrains.mps.parser.MpsParserConfig;
import org.clawiz.metadata.jetbrains.mps.parser.MpsParser;
import org.clawiz.metadata.jetbrains.mps.storage.mpsmodel.MpsModelObject;
import org.clawiz.metadata.jetbrains.mps.storage.mpsmodel.MpsModelService;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.service.NotInitializeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 23.04.13
 * Time: 14?6
 * To change this template use File | Settings | File Templates.
 */
public class MpsBase extends Service {

    public static String METADATA_LANGUAGE_ADAPTER_JETBRAINS_MPS_PACKAGE_NAME = "metadata.language.adapter.jetbrains.mps";

    MpsModelService modelService;

    MpsParser       modelParser;

    public static ConcurrentHashMap<String, Model> modelsUIDCache = new ConcurrentHashMap<>();

    public void clearCaches() {
        modelsUIDCache.clear();
    }

    public Model getModelByUID(String uid, MpsParserConfig sourceConfig) {
        if ( uid == null ) {
            return null;
        }
        Model model = modelsUIDCache.get(uid);
        if ( model != null ) {
            return model;
        }

        BigDecimal modelId = modelService.modelUidToId(uid);
        if ( modelId == null ) {
            return null;
        }
        MpsModelObject modelObject = modelService.load(modelId);

        model = null;

        MpsParserConfig config = new MpsParserConfig();
        config.copyFrom(sourceConfig);
        config.setPackageName(modelObject.getPackageName());

        if ( modelObject.getFileName() != null ) {
            model = modelParser.parseFile(modelObject.getFileName(), config);
        } else if ( modelObject.getDirectoryName() != null ) {
            model = modelParser.parseDirectory(modelObject.getDirectoryName(), config);
        } else {
            throwException("Cannot load model '?' - file and directory not defined", new Object[]{uid});
        }

        modelsUIDCache.put(uid, model);
        model.setPackageName(modelObject.getPackageName());

        return model;
    }


    public Model parse(String sourceTypeName, String sourceName, String packageName) {
        if ( "MPS-FILE".equals(sourceTypeName)) {
            return  modelParser.parseFile(sourceName, packageName);
        } else if ( "MPS-DIR".equals(sourceTypeName)) {
            return  modelParser.parseDirectory(sourceName, packageName);
        } else {
            throwException("Wrong source type name '?' of source '?'", new Object[]{sourceTypeName, sourceName});
        }

        return null;
    }

    private static String languageSuffix = ".language";
    public static String removeLastLanguageFromPackageName(String packageName) {
        String result = packageName;
        int    pos    = result.indexOf(languageSuffix);
        if ( pos != ( result.length() - languageSuffix.length() )) {
            return result;
        }
        return  result.substring(0, pos);
    }

}
