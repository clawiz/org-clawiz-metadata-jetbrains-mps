/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.parser.data.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 *
 */
@Root(name="concept")
public class MpsModelRegistryLanguageConcept {

    @Attribute
    String name;

    @Attribute
    String id;

    @Attribute
    String flags;

    @Attribute
    String index;


    @ElementList(name="property", inline = true, required = false)
    ArrayList<MpsModelRegistryLanguageConceptProperty> properties= new ArrayList<MpsModelRegistryLanguageConceptProperty>();

    @ElementList(name="reference", inline = true, required = false)
    ArrayList<MpsModelRegistryLanguageConceptReference> references= new ArrayList<MpsModelRegistryLanguageConceptReference>();

    @ElementList(name="child", inline = true, required = false)
    ArrayList<MpsModelRegistryLanguageConceptChild> childs= new ArrayList<MpsModelRegistryLanguageConceptChild>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlags() {
        return flags;
    }

    public void setFlags(String flags) {
        this.flags = flags;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public ArrayList<MpsModelRegistryLanguageConceptProperty> getProperties() {
        return properties;
    }

    public ArrayList<MpsModelRegistryLanguageConceptChild> getChilds() {
        return childs;
    }

    public ArrayList<MpsModelRegistryLanguageConceptReference> getReferences() {
        return references;
    }
}
