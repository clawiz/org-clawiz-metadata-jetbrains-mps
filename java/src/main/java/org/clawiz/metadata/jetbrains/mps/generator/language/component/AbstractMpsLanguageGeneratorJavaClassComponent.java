/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.generator.language.component;

import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.system.generator.java.component.AbstractJavaClassComponent;
import org.clawiz.metadata.jetbrains.mps.MpsBase;
import org.clawiz.metadata.jetbrains.mps.generator.language.GeneratedLanguage;
import org.clawiz.metadata.jetbrains.mps.generator.language.MpsLanguageGenerator;

import java.io.File;

public class AbstractMpsLanguageGeneratorJavaClassComponent extends AbstractJavaClassComponent {

    MpsLanguageGenerator languageGenerator;

    GeneratedLanguage generatedLanguage;

    @Override
    public MpsLanguageGenerator getGenerator() {
        return languageGenerator;
    }

    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        languageGenerator = (MpsLanguageGenerator) generator;
    }

    public GeneratedLanguage getGeneratedLanguage() {
        return generatedLanguage;
    }

    public void setGeneratedLanguage(GeneratedLanguage generatedLanguage) {
        this.generatedLanguage = generatedLanguage;

        setPackageName(MpsBase.removeLastLanguageFromPackageName(generatedLanguage.getName()) + "." + MpsBase.METADATA_LANGUAGE_ADAPTER_JETBRAINS_MPS_PACKAGE_NAME);
        setDestinationPath(languageGenerator.getDestinationPath() + File.separator + "main" + File.separator + "java" + File.separator +
                getPackageName().replace('.', File.separatorChar));

    }
}
