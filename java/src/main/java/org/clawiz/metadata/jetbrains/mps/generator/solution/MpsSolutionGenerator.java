/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.generator.solution;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeList;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.file.FileUtils;
import org.clawiz.core.common.utils.reflection.ReflectionUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.MpsSolutionNodeModelXMLComponent;
import org.clawiz.metadata.jetbrains.mps.parser.MpsParser;
import org.clawiz.metadata.jetbrains.mps.parser.MpsParserConfig;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Ref;
import java.util.ArrayList;
import java.util.HashMap;

public class MpsSolutionGenerator extends AbstractGenerator {

    MpsSolutionGeneratorContext context;
    MpsParser                   mpsParser;
    FileUtils                   fileUtils;

    public MpsSolutionGeneratorContext getContext() {
        return context;
    }

    public void setContext(MpsSolutionGeneratorContext context) {
        this.context = context;
    }

    public void prepare() {
        getContext().prepare(getSession());
    }

    private class SourceToTargetNode {
        MetadataNode    sourceNode;
        AbstractMpsNode targetNode;

        public SourceToTargetNode(MetadataNode sourceNode, AbstractMpsNode targetNode) {
            this.sourceNode = sourceNode;
            this.targetNode = targetNode;
        }
    }
    private ArrayList<SourceToTargetNode> createdNodes;

    public void createTargetNodeInstance(MetadataNode sourceNode) {

        Class<AbstractMpsNode> mpsNodeClass = getContext().getTargetClassMap(sourceNode.getClass());
        if ( mpsNodeClass == null ) {
            throwException("Target mps node class not defined for node ? class ?", sourceNode.getName(), sourceNode.getClass().getName());
        }

        AbstractMpsNode targetNode = null;
        try {
            targetNode = mpsNodeClass.newInstance();
        } catch (Exception e) {
            throwException("Exception on create loaded mps node ? for metadata node ? (?) : ?", mpsNodeClass.getName(), sourceNode.getPackageName() + "." + sourceNode.getName(), sourceNode.getClass().getName(), e.getMessage(), e);
        }

        targetNode.setParserContext(getContext().getModel().getParserContext());
        targetNode.setModel(getContext().getModel());
        targetNode.setName(sourceNode.getName());
        targetNode.setPackageName(sourceNode.getPackageName());
        targetNode.setSolutionGeneratorContext(getContext());


        getContext().getModel().getRootNodes().add(targetNode);

        createdNodes.add(new SourceToTargetNode(sourceNode, targetNode));
    }

    protected void prepareVirtualPackages() {
        for ( SourceToTargetNode stn : createdNodes) {
            String sourcePackageName = stn.sourceNode.getPackageName();
            if ( sourcePackageName == null || (sourcePackageName + ".").indexOf(getContext().getSourceRootPackageName() + ".") != 0 ) {
                throwException("Source node ? (?) package name ? not started with composer base package name ?", stn.sourceNode.getName(), stn.sourceNode.getClass().getName(), stn.sourceNode.getPackageName(), getContext().getSourceRootPackageName());
            }
            if ( sourcePackageName.length() > getContext().getSourceRootPackageName().length() ) {
                stn.targetNode.setVirtualPackage(sourcePackageName.substring(getContext().getSourceRootPackageName().length()+1));
            }

        }
    }

    protected void clearSolutionDirectory() {
        for (File file : fileUtils.getDirectoryFilesAndDirs(new File(getContext().getDestinationPath())) ) {
            if ( file.getName().equals(".model")) {
                continue;
            }
            file.delete();
        }
    }

    protected void loadModelDefinition() {
        MpsParserConfig parserConfig = new MpsParserConfig();

        parserConfig.setPackageName(getContext().getDestinationRootPackageName());
        getContext().setSolutionModel(mpsParser.parseFile(getContext().getDestinationPath() + "/.model", parserConfig));

        getContext().prepareTargeClassMaps();
    }

    int conceptNodeIdIndex = 1;
    public String createNewConceptId() {
        String str = "0000000000" + (conceptNodeIdIndex++);
        str = str.substring(str.length()-10);
        return "e" + str;
    }


    private HashMap<AbstractMpsNode, String> nodeToConceptIdCache = new HashMap<>();

    private AbstractMpsNode getNodeRoot(AbstractMpsNode node) {
        return node.getParent() != null ? getNodeRoot(node.getParent()) : node;
    }

    public String getNodeConceptId(AbstractMpsNode node) {

        AbstractMpsNode rootNode = getNodeRoot(node);

        String id = nodeToConceptIdCache.get(node);
        if ( id != null ) {
            return id;
        }

        for (AbstractComponent component : getComponents() ) {

            if ( ! (component instanceof MpsSolutionNodeModelXMLComponent) ) {
                continue;
            }

            MpsSolutionNodeModelXMLComponent solutionComponent = (MpsSolutionNodeModelXMLComponent) component;
            if ( solutionComponent.getMpsNode() == rootNode ) {

                id = solutionComponent.getRegistry().getMpsNodeToIdCache().get(node);
                if ( id != null ) {
                    nodeToConceptIdCache.put(node, id);
                }
                return id;
            }

        }
        nodeToConceptIdCache.put(node, null);
        return null;
    }

    public void process() {

        clearSolutionDirectory();
        loadModelDefinition();

        createdNodes = new ArrayList<>();

        context.getSourceNodes().sort();

        for (Object node : context.getSourceNodes() ) {
            createTargetNodeInstance((MetadataNode) node);
        }

        for ( SourceToTargetNode stn : createdNodes ) {
            stn.targetNode.setSession(getSession());
            stn.targetNode.loadMetadataNode(stn.sourceNode);
        }

        prepareVirtualPackages();

        processDeferredSolutionNodeResolves();

        for (AbstractMpsNode mpsNode : context.getModel().getRootNodes()) {
            MpsSolutionNodeModelXMLComponent sc = addComponent(MpsSolutionNodeModelXMLComponent.class);
            sc.setMpsNode(mpsNode);
            sc.createSolutionModelXMLElement();
        }
    }


    protected void processDeferredConceptRefResolves() {
        for (MpsSolutionGeneratorContext.DeferredConceptRefResolve dr : getContext().getDeferredConceptRefResolves() ) {
            String refId = dr.nodeXMLElement.getNodeConceptId(dr.node);
            if ( refId == null ) {
                throw new CoreException("Deferred reference id not found in nodes cache for node '?' (?)", dr.node.getName(), dr.node.getClass().getName());
            } else {
                dr.refXMLElement.setNode(refId);
                dr.refXMLElement.getAttribute("node").setValue(refId);
            }
        }
    }


    protected void processDeferredSolutionNodeResolves() {

        HashMap<String, AbstractMpsNode> preparedNodesCache = new HashMap<>();
        for ( AbstractMpsNode mpsNode : getContext().getModel().getRootNodes() ) {
            preparedNodesCache.put(mpsNode.getFullName(), mpsNode);
        }

        for (MpsSolutionGeneratorContext.DeferredSolutionNodeResolve resolve : getContext().getDeferredSolutionNodeResolves()) {

            ArrayList<MetadataNode> path       = new ArrayList<>();
            ArrayList<String>       pathFields = new ArrayList<>();
            MetadataNode            node       = resolve.referencedMetadataNode;
            while ( node != null ) {
                path.add(node);
                pathFields.add(node.getParentNodeFieldName());
                node = node.getParentNode();
            }

            String key = path.get(path.size()-1).getFullName();
            AbstractMpsNode resultMpsNode = preparedNodesCache.get(key);

            if ( resultMpsNode == null ) {
                throwException("Node '?' not found in generated model roots", key);
            }

            for (int i = path.size() - 2; i >= 0; i --) {
                if ( pathFields.get(i) == null ) {
                    throwException("Parent node field name not defined for ? '?'", path.get(i).getClass().getName(), path.get(i).getFullName());
                }
                String getterName = "get" + StringUtils.toUpperFirstChar(pathFields.get(i));
                Method getter = ReflectionUtils.findMethod(resultMpsNode.getClass(), getterName);
                if ( getter == null ) {
                    throwException("Method '?' not found in class '?'", getterName, resultMpsNode.getClass().getName());
                }
                Object object = null;
                try {
                    object = getter.invoke(resultMpsNode);
                } catch (Exception e) {
                    throwException("Exception in call method '?' of class '?'", getterName, resultMpsNode.getClass().getName());
                }
                MetadataNode pathNode = path.get(i);
                if ( pathNode.getName() == null ) {
                    throwException("Cannot resolve path node of class '?' for NULL name", pathNode.getClass().getName());
                }
                if ( ArrayList.class.isAssignableFrom(object.getClass())) {
                    ArrayList<AbstractMpsNode> list = (ArrayList<AbstractMpsNode>) object;
                    AbstractMpsNode foundNode = null;
                    for (AbstractMpsNode listNode : list) {
                        if ( pathNode.getName().equals(listNode.getName()) ) {
                            foundNode = listNode;
                            break;
                        }
                    }
                    if ( foundNode == null ) {
                        throwException("Node name '?' not found in '?' class of '?'", pathNode.getName(), resultMpsNode.getFullName(), resultMpsNode.getClass().getName());
                    }
                    resultMpsNode = foundNode;

                } else if (AbstractMpsNode.class.isAssignableFrom(object.getClass())) {
                    resultMpsNode = (AbstractMpsNode) object;
                } else {
                    throwException("Field '?' of class '?' not extend MetadataNodeList or AbstractMpsNode class");
                }
            }

            // need to implement setters for Array
            String setterName = "set" + StringUtils.toUpperFirstChar(resolve.fieldName);
            Method setter = ReflectionUtils.findMethod(resolve.mpsNode.getClass(), setterName, resultMpsNode.getClass());
            if ( setter == null ) {
                throwException("Method '?' for '?' not found in class '?'", setterName, resolve.mpsNode.getClass().getName(), resultMpsNode.getClass().getName());
            }
            try {
                setter.invoke(resolve.mpsNode, resultMpsNode);
            } catch (Exception e) {
                // may be because array not implemented
                throwException("Exception in call method '?' of class '?'", setterName, resolve.mpsNode.getClass().getName());
            }

        }
    }

    @Override
    protected void done() {
        super.done();
        processDeferredConceptRefResolves();
    }
}
