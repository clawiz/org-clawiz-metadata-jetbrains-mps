package org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration;

import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.MpsConceptDeclaration;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.data.language.node.element.field.AbstractStructureField;
import org.clawiz.core.common.system.generator.java.component.element.JavaVariableElement;
import org.clawiz.core.common.utils.StringUtils;

public class PreparedConceptField {

    public String                     id;
    public MpsConceptDeclarationClassPrototypeComponent.PropertyType propertyType;
    public String                     name;
    public boolean                    isArray;
    public boolean                    isPrimitive;
    public boolean                    isEnumeration;
    public String                     type;
    public JavaVariableElement        variableElement;
    public AbstractStructureField dataStructureField;
    public MpsConceptDeclaration      linkedTargetConcept;

    public static String mpsTypeToMetadataNodeClassName(String type) {
        String[] tokens = StringUtils.splitByLastTokenOccurrence(type, "\\.");
        if ( tokens.length < 2 ) {
            throw new CoreException("Mps class type ? is not format package.name", type);
        }

        tokens[0] = tokens[0].replaceAll("\\.metadata\\.language\\.adapter\\.jetbrains\\.mps", "\\.metadata");

        String name = tokens[1];
        if ( name.indexOf("Mps") == 0 ) {
            name = name.substring(3);
        }
        return tokens[0] + "." + name;
    }

    public String getTypeMetadataNodeClassName() {
        return mpsTypeToMetadataNodeClassName(type);
    }

}
