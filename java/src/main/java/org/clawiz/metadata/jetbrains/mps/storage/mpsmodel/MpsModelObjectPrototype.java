package org.clawiz.metadata.jetbrains.mps.storage.mpsmodel;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class MpsModelObjectPrototype extends AbstractObject {
    
    /**
    * PackageName
    */
    private java.lang.String packageName;
    
    /**
    * Name
    */
    private java.lang.String name;
    
    /**
    * ModelUid
    */
    private java.lang.String modelUid;
    
    /**
    * FileName
    */
    private java.lang.String fileName;
    
    /**
    * DirectoryName
    */
    private java.lang.String directoryName;
    
    public MpsModelService service;
    
    /**
    * 
    * @return     PackageName
    */
    public java.lang.String getPackageName() {
        return this.packageName;
    }
    
    /**
    * Set 'PackageName' value
    * 
    * @param      packageName PackageName
    */
    public void setPackageName(java.lang.String packageName) {
        
        if ( packageName != null && packageName.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "MpsModel.PackageName", "250");
        }
        
         this.packageName = packageName;
    }
    
    /**
    * Set 'PackageName' value and return this object
    * 
    * @param      packageName PackageName
    * @return     MpsModel object
    */
    public MpsModelObjectPrototype withPackageName(java.lang.String packageName) {
        setPackageName(packageName);
        return this;
    }
    
    /**
    * 
    * @return     Name
    */
    public java.lang.String getName() {
        return this.name;
    }
    
    /**
    * Set 'Name' value
    * 
    * @param      name Name
    */
    public void setName(java.lang.String name) {
        
        if ( name != null && name.length() > 128) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "MpsModel.Name", "128");
        }
        
         this.name = name;
    }
    
    /**
    * Set 'Name' value and return this object
    * 
    * @param      name Name
    * @return     MpsModel object
    */
    public MpsModelObjectPrototype withName(java.lang.String name) {
        setName(name);
        return this;
    }
    
    /**
    * 
    * @return     ModelUid
    */
    public java.lang.String getModelUid() {
        return this.modelUid;
    }
    
    /**
    * Set 'ModelUid' value
    * 
    * @param      modelUid ModelUid
    */
    public void setModelUid(java.lang.String modelUid) {
        
        if ( modelUid != null && modelUid.length() > 128) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "MpsModel.ModelUid", "128");
        }
        
         this.modelUid = modelUid;
    }
    
    /**
    * Set 'ModelUid' value and return this object
    * 
    * @param      modelUid ModelUid
    * @return     MpsModel object
    */
    public MpsModelObjectPrototype withModelUid(java.lang.String modelUid) {
        setModelUid(modelUid);
        return this;
    }
    
    /**
    * 
    * @return     FileName
    */
    public java.lang.String getFileName() {
        return this.fileName;
    }
    
    /**
    * Set 'FileName' value
    * 
    * @param      fileName FileName
    */
    public void setFileName(java.lang.String fileName) {
        
        if ( fileName != null && fileName.length() > 255) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "MpsModel.FileName", "255");
        }
        
         this.fileName = fileName;
    }
    
    /**
    * Set 'FileName' value and return this object
    * 
    * @param      fileName FileName
    * @return     MpsModel object
    */
    public MpsModelObjectPrototype withFileName(java.lang.String fileName) {
        setFileName(fileName);
        return this;
    }
    
    /**
    * 
    * @return     DirectoryName
    */
    public java.lang.String getDirectoryName() {
        return this.directoryName;
    }
    
    /**
    * Set 'DirectoryName' value
    * 
    * @param      directoryName DirectoryName
    */
    public void setDirectoryName(java.lang.String directoryName) {
        
        if ( directoryName != null && directoryName.length() > 255) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "MpsModel.DirectoryName", "255");
        }
        
         this.directoryName = directoryName;
    }
    
    /**
    * Set 'DirectoryName' value and return this object
    * 
    * @param      directoryName DirectoryName
    * @return     MpsModel object
    */
    public MpsModelObjectPrototype withDirectoryName(java.lang.String directoryName) {
        setDirectoryName(directoryName);
        return this;
    }
    
    public MpsModelService getService() {
        return this.service;
    }
    
    public void setService(MpsModelService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(MpsModelObjectPrototype  target) {
        target.setPackageName(getPackageName());
        target.setName(getName());
        target.setModelUid(getModelUid());
        target.setFileName(getFileName());
        target.setDirectoryName(getDirectoryName());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((MpsModelObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((MpsModelObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(MpsModelObjectPrototype object) {
        return 
               isObjectsEquals(this.getPackageName(), object.getPackageName() ) 
            && isObjectsEquals(this.getName(), object.getName() ) 
            && isObjectsEquals(this.getModelUid(), object.getModelUid() ) 
            && isObjectsEquals(this.getFileName(), object.getFileName() ) 
            && isObjectsEquals(this.getDirectoryName(), object.getDirectoryName() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getPackageName() != null ? getPackageName().hashCode() : 0);
        result = result * 31 + (getName() != null ? getName().hashCode() : 0);
        result = result * 31 + (getModelUid() != null ? getModelUid().hashCode() : 0);
        result = result * 31 + (getFileName() != null ? getFileName().hashCode() : 0);
        result = result * 31 + (getDirectoryName() != null ? getDirectoryName().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'Name' fields
    * 
    * @return     Concatenated string values of key 'toName' fields
    */
    public String toName() {
        return getName();
    }
    
    /**
    * Prepare concatenated string values of key 'ModelUid' fields
    * 
    * @return     Concatenated string values of key 'toModelUid' fields
    */
    public String toModelUid() {
        return getModelUid();
    }
}
