/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration;

import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.MpsConceptDeclaration;
import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.MpsInterfaceConceptReference;
import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.MpsLinkDeclaration;
import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.MpsPropertyDeclaration;
import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.datatype.MpsEnumerationDataTypeDeclaration;
import org.clawiz.core.common.metadata.data.common.valuetype.*;
import org.clawiz.core.common.metadata.data.language.node.element.field.AbstractStructureField;
import org.clawiz.core.common.metadata.data.language.node.element.field.AttributeStructureField;
import org.clawiz.core.common.metadata.data.language.node.element.field.ElementStructureField;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaVariableElement;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration.element.MpsConceptDeclarationFillForeignKeysMethodElement;
import org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration.element.MpsConceptDeclarationFillMetadataNodeMethodElement;
import org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration.element.MpsConceptDeclarationGetMetadataNodeClassMethodElement;
import org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration.element.MpsConceptDeclarationLoadMetadataNodeMethodElement;
import org.clawiz.metadata.jetbrains.mps.parser.data.model.MpsModelNodeRef;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.registry.MpsSolutionRegistryXMLElement.JETBRAINS_MPS_LANG_CORE_INAMEDCONCEPT_CONCEPT_ID;

public class MpsConceptDeclarationClassPrototypeComponent extends AbstractMpsConceptDeclarationComponent {

    public enum  PropertyType {
        PROPERTY, CHILD, REFERENCE
    }

    ArrayList<PreparedConceptField> preparedConceptFields = new ArrayList<>();

    public ArrayList<PreparedConceptField> getPreparedConceptFields() {
        return preparedConceptFields;
    }

    public <V extends AbstractValueType> AttributeStructureField addDataStructureAttribute(String name, Class<V> valueTypeClass) {
        return getGenerator().getDataStructure().addAttribute(name, valueTypeClass);
    }

    public void addDataStructureField(PreparedConceptField field) {

        AbstractStructureField structureField = null;

        if ( field.isPrimitive ) {

            if ( field.isEnumeration ) {
                structureField = addDataStructureAttribute(field.name, ValueTypeEnumeration.class);
                getGenerator().getLanguageGenerator().getContext().addDeferredEnumerationStructureResolve((AttributeStructureField) structureField, field.getTypeMetadataNodeClassName());
            } else {
                if ( field.type.equals("String") ) {
                    structureField = addDataStructureAttribute(field.name, ValueTypeString.class);
                } else if ( field.type.equals("BigDecimal")) {
                    structureField = addDataStructureAttribute(field.name, ValueTypeNumber.class);
                } else if ( field.type.equals("Boolean")) {
                    structureField = addDataStructureAttribute(field.name, ValueTypeBoolean.class);
                } else {
                    throwException("Wrong primitive field ? value type name ?", field.name, field.type);
                }
            }

        } else {

            ElementStructureField elementDataStructureField = getGenerator().getDataStructure().addElement(field.name);
            structureField                                  = elementDataStructureField;

            structureField.setArray(field.isArray);
            elementDataStructureField.setReference(field.propertyType == PropertyType.REFERENCE);

            if ( getGenerator().getLanguage().getRefConcept(field.getTypeMetadataNodeClassName()) != null ) {
                elementDataStructureField.setReference(true);
            }

            String className = getGenerator().getLanguage().getClassNameWithoutRef(field.getTypeMetadataNodeClassName());

            getGenerator().getLanguageGenerator().getContext().addDeferredDataStructureResolve(elementDataStructureField, className);

        }

        structureField.setArray(field.isArray);
        field.dataStructureField = structureField;

    }

    public void addConceptField(String id, String propertyType, String name, String type, boolean isPrimitive, boolean isEnumeration, boolean isArray, MpsConceptDeclaration linkedTargetConcept) {

        PreparedConceptField field = new PreparedConceptField();

        field.id                  = id;
        field.propertyType        = PropertyType.valueOf(propertyType.toUpperCase());
        field.name                = name;
        field.type                = type;
        field.isArray             = isArray;
        field.isPrimitive         = isPrimitive;
        field.isEnumeration       = isEnumeration;
        field.linkedTargetConcept = linkedTargetConcept;

        if ( isArray ) {
            addImport(ArrayList.class);
            field.variableElement = addVariable("ArrayList<" + type + ">", name, "new ArrayList<>()");
        } else {
            field.variableElement = addVariable(type, name);
        }

        preparedConceptFields.add(field);

        addDataStructureField(field);

    }
    boolean iNamedConcept = false;

    public void addFields() {

        boolean namePropertyFound = false;
        for (MpsPropertyDeclaration property : getConcept().getPropertyDeclaration() ) {
            if ( property.getName().equals("name") ) {
                namePropertyFound = true;
                break;
            }
        }

        if ( ! namePropertyFound ) {
            for (MpsInterfaceConceptReference ref: getConcept().getImplements() ) {
                for (MpsModelNodeRef modelNodeRef : ref.getMpsModelNode().getRefs() ) {
                    if ( modelNodeRef.getResolve().equals("INamedConcept")) {
                        iNamedConcept = true;
                        break;
                    }
                }
            }
        }

        for (MpsPropertyDeclaration property : getConcept().getPropertyDeclaration() ) {
            addConceptField(property.getPropertyId(), "PROPERTY", property.getName(), property.getValueTypeClassName(), true, property.isEnumeration(), false, null);
        }


        for( MpsLinkDeclaration link : getConcept().getLinkDeclaration() ) {
            String propertyType = null;
            if ( link.getMetaClass().equals("reference") ) {
                propertyType = "REFERENCE";
            } else if ( link.getMetaClass().equals("aggregation")) {
                propertyType = "CHILD";
            } else {
                throwException("Wrong concept node ? link '?' metaclass ?", getConcept().getName(), link.getRole(), link.getMetaClass());
            }

            MpsConceptDeclaration linkedTarget = (link.getTarget() instanceof  MpsConceptDeclaration) ? (MpsConceptDeclaration) link.getTarget() : null;
            addConceptField(link.getLinkId(), propertyType, link.getRole(), link.getValueTypeClassName(), false, false, link.isArray(), linkedTarget);
        }


    }

    public void addFieldMethods() {

        for (PreparedConceptField field : preparedConceptFields) {

            JavaVariableElement ve = field.variableElement;

            JavaMethodElement getter = addMethod(
                    field.isArray ? ("ArrayList<" + field.type + ">") : field.type
                    , ve.getGetterName());
            getter.addText("return this." + ve.getName() + ";");

            if ( field.isPrimitive ) {

                JavaMethodElement setter = addMethod(ve.getSetterName());
                setter.addParameter("String", "value");

                setter.addText("if ( StringUtils.isEmpty(value) ) {");
                setter.addText("    this." + ve.getName() + " = null;");
                setter.addText("}");

                if ( field.isEnumeration ) {

                    setter.addText("this." + ve.getName() + " = " + field.type + ".to" + StringUtils.splitByLastTokenOccurrence(field.type, "\\.")[1] + "(value);");

                } else {

                    if ( field.type.equals("String") ) {
                        setter.addText("this." + ve.getName() + " = value;");
                    } else if ( field.type.equals("BigDecimal") ) {
                        setter.addText("this." + ve.getName() + " = StringUtils.toBigDecimal(value);");
                    } else if ( field.type.equals("Boolean" ) ) {
                        setter.addText("this." + ve.getName() + " = StringUtils.toBoolean(value);");
                    } else {
                        throwException("Wrong primitive property ? type ?", field.type);
                    }

                }

            } else {

                if ( ! field.isArray ) {

                    JavaMethodElement setter = addMethod(ve.getSetterName());
                    setter.addParameter(field.type, "value");
                    setter.addText("this." + field.name + " = value;");

                }

            }


        }

    }

    String _conceptWithStructureName;
    private String getConceptWithStructureName() {
        if ( _conceptWithStructureName != null ) {
            return _conceptWithStructureName;
        }
        _conceptWithStructureName = getGenerator().getLanguage().getName() + ".structure." +  getConcept().getName();
        return _conceptWithStructureName;
    }

    public void addLanguageRegistryMethods() {
        JavaMethodElement method = addMethod("String", "getLanguageId");
        method.addText("return \"" +  getGenerator().getLanguage().getUuid() + "\";");

        method = addMethod("String", "getLanguageName");
        method.addText("return \"" +  getGenerator().getLanguage().getName() + "\";");

        method = addMethod("String", "getLanguageConceptId");
        method.addText("return \"" +  getConcept().getConceptId() + "\";");

        method = addMethod("String", "getLanguageConceptName");
        method.addText("return \"" + getConceptWithStructureName() + "\";");

        method = addMethod("ArrayList<ConceptProperty>", "getConceptProperties");

        method.addText("ArrayList<ConceptProperty> result = new ArrayList<>();");
        if ( ! this.getExtends().equals("AbstractMpsNode")) {
            method.addText("result.addAll(super.getConceptProperties());");
            method.addText("");
        }

        for (int i = 0; i < preparedConceptFields.size(); i++ ) {
            PreparedConceptField field = preparedConceptFields.get(i);
            method.addText("result.add(new ConceptProperty("
                    + "\"" + getGenerator().getLanguage().getUuid() + "\", \"" + getGenerator().getLanguage().getName() + "\", "
                    + "\"" + getConcept().getConceptId() + "\", \"" + getConceptWithStructureName() + "\", "
                    + "ConceptPropertyType." + field.propertyType + ", \"" + field.id + "\", \"" + field.name + "\"));");
        }

        method.addText("");
        method.addText("return result;");


    }

    public void addFillConceptNodeMethod() {

        JavaMethodElement method = addMethod("fillConceptNode");

        method.addText("");

        if ( ! this.getExtends().equals("AbstractMpsNode")) {
            method.addText("super.fillConceptNode();");
            method.addText("");
        }

        ArrayList<String> refLines = new ArrayList<>();

        if ( iNamedConcept ) {
            method.addText("addConceptNodeProperty(\"" + JETBRAINS_MPS_LANG_CORE_INAMEDCONCEPT_CONCEPT_ID + "\", \"name\", getName());");
        }

        for (PreparedConceptField field : preparedConceptFields) {
            String getter;
            if ( field.type.equalsIgnoreCase("boolean") ) {
                getter = "is" + StringUtils.toUpperFirstChar(field.name) + "()";
            } else {
                getter = "get" + StringUtils.toUpperFirstChar(field.name) + "()";
            }

            if ( field.propertyType == PropertyType.PROPERTY ) {

                if ( ! field.type.equals("String") ) {
                    AbstractMpsNode typeNode = null;
                    if ( field.type.equals("BigDecimal") || field.type.equals("Boolean") ) {

                    } else {
                        typeNode = getGenerator().getLanguageGenerator().getLanguageNodeClass(field.type);
                    }

                    if ( typeNode != null && typeNode instanceof MpsEnumerationDataTypeDeclaration) {
                        getter = field.type + ".toConceptNodePropertyString(" + getter + ")";
                    } else {
                        getter = getter + " != null ? " + getter + ".toString() : null";
                    }
                }
                method.addText("addConceptNodeProperty(\"" + getConcept().getConceptId() + "\", \"" + field.name + "\", " + getter + ");");

            } else if ( field.propertyType == PropertyType.REFERENCE ) {

                if ( field.isArray ) {
                    refLines.add("for (AbstractMpsNode value : " + getter + " ) {");
                    refLines.add("    addConceptNodeRef(\"" + field.name + "\", value);");
                    refLines.add("}");
                } else {
                    refLines.add("addConceptNodeRef(\"" + getConcept().getConceptId() + "\", \"" + field.name + "\", " + getter + ");");
                }

            } else if ( field.propertyType == PropertyType.CHILD ) {

                if ( field.isArray ) {
                    method.addText("for (AbstractMpsNode value : " + getter + " ) {");
                    method.addText("    addConceptNodeChild(\"" + getConcept().getConceptId() + "\", \"" + field.name + "\", value);");
                    method.addText("}");
                } else {
                    method.addText("addConceptNodeChild(\"" + getConcept().getConceptId() + "\", \"" + field.name + "\", " + getter + ");");
                }

            } else {
                throwException("Wrong field property ? type ?", field.name, field.propertyType);
            }

        }

        for (String string : refLines ) {
            method.addText(string);
        }

        method.addText("");

    }


    @Override
    public void setPackageName(String packageName) {
        super.setPackageName(packageName);
    }

    @Override
    public void process() {
        super.process();

        setName(getGenerator().getComponentByClass(MpsConceptDeclarationClassComponent.class).getName() + "Prototype");

        if ( concept.getExtends() == null ) {
            setExtends("AbstractMpsNode");
        } else {
            setExtends(concept.getModel().nodeToJavaClassName(concept.getExtends()));
        }

        addImport(BigDecimal.class);
        addImport(StringUtils.class);
        addImport(AbstractMpsNode.class);
        addImport(ArrayList.class);

        addFields();
        addFieldMethods();


        addLanguageRegistryMethods();

        addFillConceptNodeMethod();

        addElement(MpsConceptDeclarationGetMetadataNodeClassMethodElement.class);
        addElement(MpsConceptDeclarationFillMetadataNodeMethodElement.class);
        addElement(MpsConceptDeclarationFillForeignKeysMethodElement.class);
        addElement(MpsConceptDeclarationLoadMetadataNodeMethodElement.class);

    }


}
