/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.install.model;

import org.clawiz.core.common.system.installer.script.xml.AbstractXMLScriptElementInstaller;
import org.clawiz.metadata.jetbrains.mps.parser.MpsParser;

public class XMLScriptJetbrainsMPSModelInstaller extends AbstractXMLScriptElementInstaller {

    public static final String ATTRIBUTE_NAME_MODEL_PATH = "model-path";

    MpsParser mpsParser;

    @Override
    public void process() {

        String modelPath = getAttribute(ATTRIBUTE_NAME_MODEL_PATH);
        if ( modelPath == null ) {
            throwException("Attribute 'model-path' not defined for install Jetbrains MPS model in script ?", getScriptFileName());
        }

        JetbrainsMPSModelInstaller modelInstaller = addInstaller(JetbrainsMPSModelInstaller.class);
        modelInstaller.setModelPath(relativeToFullFileName(modelPath));


    }
}
