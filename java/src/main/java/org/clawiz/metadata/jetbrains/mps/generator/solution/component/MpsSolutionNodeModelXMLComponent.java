/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.generator.solution.component;

import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.system.generator.xml.component.AbstractXMLComponent;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.metadata.jetbrains.mps.generator.solution.MpsSolutionGenerator;
import org.clawiz.metadata.jetbrains.mps.generator.solution.MpsSolutionGeneratorContext;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.*;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.concept.MpsSolutionConceptNodeXMLElement;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.registry.MpsSolutionRegistryXMLElement;

import java.util.ArrayList;

public class MpsSolutionNodeModelXMLComponent extends AbstractXMLComponent {

    MpsSolutionGenerator        solutionGenerator;
    MpsSolutionGeneratorContext generatorContext;

    AbstractMpsNode             mpsNode;

    MpsSolutionImportsXMLElement  imports;
    MpsSolutionRegistryXMLElement registry;
    ArrayList<MpsSolutionConceptNodeXMLElement> concepts = new ArrayList<>();

    @Override
    public MpsSolutionGenerator getGenerator() {
        return solutionGenerator;
    }

    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        solutionGenerator = (MpsSolutionGenerator) generator;
        generatorContext = solutionGenerator.getContext();
        setDestinationPath(generatorContext.getDestinationPath());
    }


    public AbstractMpsNode getMpsNode() {
        return mpsNode;
    }

    public void setMpsNode(AbstractMpsNode mpsNode) {
        this.mpsNode = mpsNode;
        setFileName(getGenerator().getContext().getFreeFileName(mpsNode.getName()) + ".mpsr");
    }



    public MpsSolutionImportsXMLElement getImports() {
        return imports;
    }

    public MpsSolutionRegistryXMLElement getRegistry() {
        return registry;
    }

    public ArrayList<MpsSolutionConceptNodeXMLElement> getConcepts() {
        return concepts;
    }

    MpsSolutionModelXMLElement _solutionModelXMLElement;
    public void createSolutionModelXMLElement() {
        if ( _solutionModelXMLElement != null ) {
            return;
        }
        _solutionModelXMLElement = addElement(MpsSolutionModelXMLElement.class);
        _solutionModelXMLElement.addElement(MpsSolutionPersistenceXMLElement.class);
        imports  = _solutionModelXMLElement.addElement(MpsSolutionImportsXMLElement.class);
        registry = _solutionModelXMLElement.addElement(MpsSolutionRegistryXMLElement.class);

        registry.addJetBrainsCommonLanguages();
    }

    @Override
    public void process() {
        super.process();

        logDebug("Generate solution model for " + getMpsNode().getFullName() + '(' + getMpsNode().getClass().getName() + ")");


        mpsNode.saveToXMLElement(_solutionModelXMLElement);

    }
}
