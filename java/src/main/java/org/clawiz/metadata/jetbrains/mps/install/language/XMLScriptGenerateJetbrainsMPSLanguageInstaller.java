/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.install.language;

import org.clawiz.core.common.system.installer.script.xml.AbstractXMLScriptElementInstaller;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.generator.language.GeneratedLanguage;
import org.clawiz.metadata.jetbrains.mps.generator.language.MpsLanguageGenerator;
import org.clawiz.metadata.jetbrains.mps.generator.language.MpsLanguageGeneratorContext;
import org.w3c.dom.Element;

public class XMLScriptGenerateJetbrainsMPSLanguageInstaller extends AbstractXMLScriptElementInstaller {

    public static final String ATTRIBUTE_NAME_NAME                = "name";

    public static final String ELEMENT_NAME_REFERENCE_CONCEPT     = "reference-concept";
    public static final String ATTRIBUTE_NAME_CONCEPT_NAME        = "concept-name";
    public static final String ATTRIBUTE_NAME_FIELD_NAME          = "field-name";
    public static final String ATTRIBUTE_NAME_METADATA_CLASS_NAME = "metadata-class-name";

    @Override
    public void process() {

        String languageName = getAttribute(ATTRIBUTE_NAME_NAME);
        if ( languageName == null ) {
            throwException("Attribute 'name' not defined for generate Jetbrains MPS language in script ?", getScriptFileName());
        }
        if( getDestinationPath() == null ) {
            logDebug("Destination path not defined, skip generate Jetbrains MPS language " + languageName);
            return;
        }

        MpsLanguageGenerator generator = getService(MpsLanguageGenerator.class);

        MpsLanguageGeneratorContext context = generator.getContext();
        context.setDestinationPath(getDestinationPath());

        GeneratedLanguage generatedLanguage = context.addGeneratedLanguage(languageName);

        for (int i=0; i < getElement().getChildNodes().getLength(); i++ ) {
            if ( ! (getElement().getChildNodes().item(i) instanceof Element) ) {
                continue;
            }
            Element element = (Element) getElement().getChildNodes().item(i);
            if ( element.getTagName().equals(ELEMENT_NAME_REFERENCE_CONCEPT)) {

                String conceptName  = element.getAttribute(ATTRIBUTE_NAME_CONCEPT_NAME);
                String fieldName    = element.getAttribute(ATTRIBUTE_NAME_FIELD_NAME);
                String metadataName = element.getAttribute(ATTRIBUTE_NAME_METADATA_CLASS_NAME);

                if (StringUtils.isEmpty(conceptName)) {
                    throwException("Attribute '?' not defined for '?' element of generate Jetbrains MPS language '?' node in script ?", ATTRIBUTE_NAME_CONCEPT_NAME, element.getTagName(), languageName, getScriptFileName());
                }
                if (StringUtils.isEmpty(fieldName)) {
                    throwException("Attribute '?' for concept '?' not defined for '?' element of generate Jetbrains MPS language '?' node in script ?", ATTRIBUTE_NAME_FIELD_NAME, conceptName, element.getTagName(), languageName, getScriptFileName());
                }
                if (StringUtils.isEmpty(metadataName)) {
                    throwException("Attribute '?' for concept '?' not defined for '?' element of generate Jetbrains MPS language '?' node in script ?", ATTRIBUTE_NAME_METADATA_CLASS_NAME, conceptName, element.getTagName(), languageName, getScriptFileName());
                }

                generatedLanguage.addRefConcept(conceptName, fieldName, metadataName);

            } else {
                throwException("Wrong element '?' in generate Jetbrains MPS language '?' node in script ?", element.getTagName(), languageName, getScriptFileName());
            }
        }




        generator.run();


    }
}
