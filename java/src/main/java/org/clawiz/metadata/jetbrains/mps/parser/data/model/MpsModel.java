/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.parser.data.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 23.04.13
 * Time: 13?6
 * To change this template use File | Settings | File Templates.
 */
public class MpsModel {

    @Attribute(name = "modelUID", required = false)
    String modelUID;

    @Element(name="persistence")
    MpsModelPersistence persistence;

    @Attribute(name="content", required = false)
    String content;

    @Attribute(name="ref", required = false)
    String ref;

    @ElementList(name="language", inline = true, required = false)
    ArrayList<MpsModelLanguage> languages = new ArrayList<MpsModelLanguage>();

    @ElementList(name="languages", required = false)
    ArrayList<MpsModelLanguageUse> languageUses = new ArrayList<MpsModelLanguageUse>();

    @ElementList(name="import", inline = true, required = false)
    ArrayList<MpsModelImport> imports = new ArrayList<MpsModelImport>();

    @ElementList(name="imports", required = false)
    ArrayList<MpsModelImport> importsList = new ArrayList<MpsModelImport>();

    @Element(name="registry", required = false)
    MpsModelRegistry registry;

    @ElementList(name="node", inline = true, required = false)
    ArrayList<MpsModelRoot> roots= new ArrayList<MpsModelRoot>();
/*

    @ElementList(name="node", inline = true, required = false)
    ArrayList<MpsModelNode> nodes= new ArrayList<MpsModelNode>();
*/

    @ElementList(name="attribute", inline = true, required = false)
    ArrayList<MpsModelAttribute> asttributes = new ArrayList<>();

    public String getModelUID() {
        return modelUID != null ? modelUID : ref;
    }

    public MpsModelPersistence getPersistence() {
        return persistence;
    }

    public ArrayList<MpsModelLanguage> getLanguages() {
        return languages;
    }

    public ArrayList<MpsModelLanguageUse> getLanguageUses() {
        return languageUses;
    }

    public ArrayList<MpsModelImport> getImports() {
        return imports;
    }

    public ArrayList<MpsModelRoot> getRoots() {
        return roots;
    }

    public void setModelUID(String modelUID) {
        this.modelUID = modelUID;
    }

    public void setPersistence(MpsModelPersistence persistence) {
        this.persistence = persistence;
    }


    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public MpsModelRegistry getRegistry() {
        return registry;
    }

    public MpsModelImport getImportByIndex(String index) {
        if ( index == null ) {
            return null;
        }
        for (MpsModelImport i : imports) {
            if ( index.equals(i.getIndex())) {
                return i;
            }
        }
        return null;
    }

    public ArrayList<MpsModelAttribute> getAsttributes() {
        return asttributes;
    }

    public ArrayList<MpsModelImport> getImportsList() {
        return importsList;
    }

}

