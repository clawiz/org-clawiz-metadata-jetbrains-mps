package org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration.element;

import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBoolean;
import org.clawiz.core.common.metadata.data.language.node.element.field.AttributeStructureField;
import org.clawiz.core.common.metadata.data.language.node.element.field.ElementStructureField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeNumber;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.generator.language.GeneratedLanguage;
import org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration.PreparedConceptField;

public class MpsConceptDeclarationLoadMetadataNodeMethodElement extends AbstractMpsConceptDeclarationMethodElement {

    public void processAttributeField(PreparedConceptField conceptField) {

        AttributeStructureField field = (AttributeStructureField) conceptField.dataStructureField;

        if ( field.isArray() ) {
            throwException("Attribute field ? arrays not implemented", getDataStructure().getFullName() + "." + field.getName());
        }

        String getter = "structure." + field.getGetMethodName() + "()";

        if ( field.getValueType() instanceof ValueTypeNumber ) {
            getter = getter + " != null ? " + getter + ".toString() : null";
        } else if ( field.getValueType() instanceof ValueTypeBoolean ) {
            getter = getter + " ? \"true\" : \"false\"";
        }

        if ( conceptField.isEnumeration ) {
            getter = getter +" != null ? " + getter + ".toString() : null";
        }

        addText(field.getSetMethodName() + "(" + getter + ");");

    }

    public void processElementField(PreparedConceptField conceptField) {

        ElementStructureField field = (ElementStructureField) conceptField.dataStructureField;

        String getter = null;
        String setter = null;
        String lpad   = "";

        GeneratedLanguage.RefConcept refConcept = getLanguage().getRefConcept(conceptField.getTypeMetadataNodeClassName());

        if ( field.isArray() ) {
            addText(field.getGetMethodName() + "().clear();");
            String nodeClassName = getLanguage().applyRefConceptToClassName(conceptField.getTypeMetadataNodeClassName());
            addText("for (" + nodeClassName + " metadataNode : structure." + field.getGetMethodName() + "() ) {");
            getter = "metadataNode";
            setter = field.getGetMethodName() + "().add";
            lpad   = "    ";
        } else {
            getter = "structure." + field.getGetMethodName() + "()";
            setter = field.getSetMethodName();
        }


        boolean isReference = field.isReference() || refConcept != null;

        addText(lpad + "if ( " + getter + " != null ) {");
        if ( isReference ) {
            addText(lpad + "    getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, \"" + field.getJavaVariableName() + "\", " + field.isArray() + ", " + getter + ");");
        } else {
            addText(lpad + "    " + setter + "(loadChildMetadataNode(" + getter + "));");
        }
        addText(lpad + "} else {");
        addText(lpad + "    " + setter + "(null);");
        addText(lpad + "}");

        if ( field.isArray() ) {
            addText("}");
        }


    }


    public void processField(PreparedConceptField conceptField) {

        addText("");
        if ( conceptField.isPrimitive ) {
            processAttributeField(conceptField);
        } else {
            processElementField(conceptField);
        }

    }
    @Override
    public void process() {
        super.process();

        setName("loadMetadataNode");

        addParameter("MetadataNode", "node");

        if ( getComponent().getConcept().getExtends() != null ) {
            addText("super.loadMetadataNode(node);");
        }

        if ( getLanguage().getRefConcept(getDataStructureClassName()) != null ) {
            return;
        }

        addText(getDataStructureClassName() + " structure = (" + getDataStructureClassName() + ") node;");

        for (PreparedConceptField field : getPreparedConceptFields() ) {
            processField(field);
        }

        addText("");

    }
}
