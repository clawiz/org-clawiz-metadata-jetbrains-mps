/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.generator.language;

import org.clawiz.core.common.metadata.data.language.node.AbstractLanguageNode;
import org.clawiz.core.common.metadata.data.language.node.element.Structure;
import org.clawiz.core.common.metadata.data.language.node.element.field.AttributeStructureField;
import org.clawiz.core.common.metadata.data.language.node.element.field.ElementStructureField;
import org.clawiz.core.common.metadata.data.language.node.enumeration.Enumeration;
import org.clawiz.metadata.jetbrains.mps.parser.MpsParserConfig;

import java.util.ArrayList;

public class MpsLanguageGeneratorContext {

    String          destinationPath;

    MpsParserConfig parserConfig;

    public MpsLanguageGeneratorContext() {
        parserConfig = new MpsParserConfig();
        parserConfig.addSkipImport("jetbrains.mps.lang.core.structure");
    }


    ArrayList<GeneratedLanguage> generatedLanguages = new ArrayList<>();


    public String getDestinationPath() {
        return destinationPath;
    }

    public void setDestinationPath(String destinationPath) {
        this.destinationPath = destinationPath;
    }

    public MpsParserConfig getParserConfig() {
        return parserConfig;
    }

    public void setParserConfig(MpsParserConfig parserConfig) {
        this.parserConfig = parserConfig;
    }

    public GeneratedLanguage addGeneratedLanguage(String name) {
        GeneratedLanguage language = new GeneratedLanguage();
        language.setName(name);
        generatedLanguages.add(language);
        return language;
    }

    public ArrayList<GeneratedLanguage> getGeneratedLanguages() {
        return generatedLanguages;
    }

    public class DeferredDataStructureResolve {
        ElementStructureField field;
        String                className;
    }

    ArrayList<DeferredDataStructureResolve> deferredDataStructureResolves = new ArrayList<>();
    public void addDeferredDataStructureResolve(ElementStructureField field, String className) {
        DeferredDataStructureResolve resolve = new DeferredDataStructureResolve();
        resolve.field       = field;
        resolve.className   = className;
        deferredDataStructureResolves.add(resolve);
    }

    public ArrayList<DeferredDataStructureResolve> getDeferredDataStructureResolves() {
        return deferredDataStructureResolves;
    }

    ArrayList<Structure> preparedMetadataDataStructures = new ArrayList<>();

    public ArrayList<Structure> getPreparedMetadataDataStructures() {
        return preparedMetadataDataStructures;
    }


    public class GeneratedLanguageNode {
        AbstractLanguageNode languageNode;
        GeneratedLanguage    generatedLanguage;

        public GeneratedLanguageNode(AbstractLanguageNode languageNode, GeneratedLanguage generatedLanguage) {
            this.languageNode = languageNode;
            this.generatedLanguage = generatedLanguage;
        }
    }



    ArrayList<GeneratedLanguageNode> generatedLanguageNodes = new ArrayList<>();
    public ArrayList<GeneratedLanguageNode> getGeneratedLanguageNodes() {
        return generatedLanguageNodes;
    }

    public void addGeneratedLanguageNode(AbstractLanguageNode languageNode, GeneratedLanguage generatedLanguage) {
        getGeneratedLanguageNodes().add(new GeneratedLanguageNode(languageNode, generatedLanguage));
    }

    public class DeferredEnumerationStructureResolve {
        AttributeStructureField field;
        String                      className;
    }

    ArrayList<DeferredEnumerationStructureResolve> deferredEnumerationStructureResolves = new ArrayList<>();
    public void addDeferredEnumerationStructureResolve(AttributeStructureField field, String className) {
        DeferredEnumerationStructureResolve resolve = new DeferredEnumerationStructureResolve();
        resolve.field       = field;
        resolve.className   = className;
        deferredEnumerationStructureResolves.add(resolve);
    }

    public ArrayList<DeferredEnumerationStructureResolve> getDeferredEnumerationStructureResolves() {
        return deferredEnumerationStructureResolves;
    }

    ArrayList<Enumeration> preparedMetadataEnumerationStructures = new ArrayList<>();

    public ArrayList<Enumeration> getPreparedMetadataEnumerationStructures() {
        return preparedMetadataEnumerationStructures;
    }

}
