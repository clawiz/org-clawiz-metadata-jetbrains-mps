/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.registry;

import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.AbstractMpsSolutionXMLElement;

public class MpsSolutionRegistryLanguageXMLElement extends AbstractMpsSolutionXMLElement {

    String languageId;
    MpsSolutionRegistryXMLElement registry;

    @Override
    public String getTagName() {
        return "language";
    }

    public String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }

    public MpsSolutionRegistryXMLElement getRegistry() {
        return registry;
    }

    @Override
    public void setParent(AbstractElement parent) {
        super.setParent(parent);
        registry = (MpsSolutionRegistryXMLElement) parent;
    }

    public MpsSolutionRegistryLanguageConceptXMLElement registerConcept(String id, String name) {
        for ( AbstractElement element : registry.getParent().getElements() ) {
            if ( element instanceof MpsSolutionRegistryLanguageConceptXMLElement
                    && ((MpsSolutionRegistryLanguageConceptXMLElement) element).getConceptId().equals(id)) {
                return (MpsSolutionRegistryLanguageConceptXMLElement) element;
            }
        }
        return registry.createIndexedElement(id, name, MpsSolutionRegistryLanguageConceptXMLElement.class, this);
    }

    @Override
    public void process() {
        super.process();
        addAttribute("id",   getLanguageId());
        addAttribute("name", getName());
    }
}
