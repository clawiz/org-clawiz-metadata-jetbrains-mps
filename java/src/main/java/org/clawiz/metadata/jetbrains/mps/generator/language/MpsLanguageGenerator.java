/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.generator.language;

import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.MpsConceptDeclaration;
import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.MpsInterfaceConceptDeclaration;
import org.clawiz.core.common.metadata.data.language.node.AbstractLanguageNode;
import org.clawiz.core.common.metadata.generator.node.AbstractMetadataNodeGenerator;
import org.clawiz.core.common.metadata.generator.node.AbstractMetadataNodeGeneratorContext;
import org.clawiz.core.common.metadata.generator.node.dataclass.MetadataNodeJavaDataClassGenerator;
import org.clawiz.core.common.metadata.generator.node.dataclass.MetadataNodeJavaDataClassGeneratorContext;
import org.clawiz.core.common.metadata.data.language.node.element.Structure;
import org.clawiz.core.common.metadata.data.language.node.enumeration.Enumeration;
import org.clawiz.core.common.metadata.generator.node.enumeration.MetadataNodeJavaEnumGenerator;
import org.clawiz.core.common.metadata.generator.node.enumeration.MetadataNodeJavaEnumGeneratorContext;
import org.clawiz.core.common.metadata.node.MetadataNodeList;
import org.clawiz.core.common.metadata.node.storage.MetadataNodeResourcesXMLStorage;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.file.FileUtils;
import org.clawiz.core.common.utils.xml.XMLUtils;
import org.clawiz.metadata.jetbrains.mps.MpsBase;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.metadata.jetbrains.mps.generator.language.component.context.MpsLanguageContextComponent;
import org.clawiz.metadata.jetbrains.mps.generator.language.component.context.MpsLanguageContextPrototypeComponent;
import org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration.MpsLanguageConceptDeclarationGenerator;
import org.clawiz.metadata.jetbrains.mps.generator.language.concept.enumerationdeclaration.MpsLanguageEnumerationDeclarationGenerator;
import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.datatype.MpsEnumerationDataTypeDeclaration;
import org.clawiz.metadata.jetbrains.mps.parser.Model;
import org.clawiz.metadata.jetbrains.mps.parser.MpsParser;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.HashMap;

public class MpsLanguageGenerator extends AbstractGenerator {

    MpsParser                       mpsParser;
    XMLUtils                        xmlUtils;
    FileUtils                       fileUtils;
    MetadataNodeResourcesXMLStorage metadataNodeResourcesXMLStorage;

    MpsLanguageGeneratorContext context = new MpsLanguageGeneratorContext();

    public MpsLanguageGeneratorContext getContext() {
        return context;
    }

    public void setContext(MpsLanguageGeneratorContext context) {
        this.context = context;
    }

    HashMap<Class<AbstractMpsNode>, Class<AbstractMpsLanguageConceptGenerator>> conceptGeneratorsMap = new HashMap<>();
    HashMap<Class<AbstractMpsNode>, String> skippedConceptsCache = new HashMap<>();

    protected <M extends AbstractMpsNode, C extends AbstractMpsLanguageConceptGenerator> void addConceptMap(Class<M> concept, Class<C> component) {
        conceptGeneratorsMap.put((Class<AbstractMpsNode>) concept, (Class<AbstractMpsLanguageConceptGenerator>) component);
    }

    protected <M extends AbstractMpsNode> void addSkippedConcept(Class<M> concept) {
        skippedConceptsCache.put((Class<AbstractMpsNode>) concept, "");
    }

    @Override
    protected void prepare() {
        super.prepare();

        setDestinationPath(getContext().getDestinationPath());

        addConceptMap(MpsConceptDeclaration.class, MpsLanguageConceptDeclarationGenerator.class);
        addConceptMap(MpsEnumerationDataTypeDeclaration.class, MpsLanguageEnumerationDeclarationGenerator.class);

        addSkippedConcept(MpsInterfaceConceptDeclaration.class);

    }

    HashMap<String, AbstractMpsNode> languageNodesCache = new HashMap<>();
    public AbstractMpsNode getLanguageNodeClass(String name) {

        if ( name == null ) {
            throwException("Language node name cannot be NULL", name);
        }

        AbstractMpsNode result = languageNodesCache.get(name);
        if ( result != null ) {
            return result;
        }

        String[] tokens = StringUtils.splitByLastTokenOccurrence(name, "\\.");
        if ( tokens.length < 2 ) {
            throwException("Node name '?' not in format package.name", name);
        }

        String checkName = tokens[0]
                + "." + ( tokens[1].substring(0,3).equals("Mps") ? tokens[1].substring(3) : tokens[1] );

        checkName = checkName.replaceAll("\\.metadata\\.language\\.adapter\\.jetbrains\\.mps\\.data", "");

        for (GeneratedLanguage generatedLanguage : getContext().getGeneratedLanguages() ) {
            String languageName = MpsBase.removeLastLanguageFromPackageName(generatedLanguage.getName());
            for (AbstractMpsNode node : generatedLanguage.getModel().getRootNodes()) {
                String languageNodeName = languageName
                        + "."
                        + ( node.getVirtualPackage() != null ? node.getVirtualPackage() + "." : "")
                        + node.getName();
                if ( languageNodeName.equals(checkName) ) {
                    if ( result != null ) {
                        throwException("Language models contain more then one node '?'", checkName);
                    }
                    result = node;
                }
            }
        }

        if ( result == null ) {
            throwException("Node '?' not found in languages model", checkName);
        }

        languageNodesCache.put(name, result);
        return result;
    }

    protected void processModel(GeneratedLanguage generatedLanguage) {

        logDebug("Start process model " + generatedLanguage.getName());
        MetadataNodeList<AbstractLanguageNode> generatedNodes = new MetadataNodeList<>();

        ArrayList<AbstractMpsLanguageConceptGenerator> conceptGenerators = new ArrayList<>();

        for (AbstractMpsNode node : generatedLanguage.getModel().getRootNodes()) {

            logDebug("Processing node " + node.getName() + " " + node.getClass().getName());
            Class<AbstractMpsLanguageConceptGenerator> generatorClass = conceptGeneratorsMap.get(node.getClass());
            if ( generatorClass == null ) {
                if ( skippedConceptsCache.containsKey(node.getClass()) ) {
                    logDebug("Skipped : Generator not defined for  " + node.getClass().getName());
                    continue;
                }
                throwException("Generator not defined for   ?", node.getClass().getName());
            }

            AbstractMpsLanguageConceptGenerator generator = getService(generatorClass, true);

            generator.setLanguageGenerator(this);
            generator.setLanguage(generatedLanguage);
            generator.setMetadataNode(node);

            conceptGenerators.add(generator);

            generator.run();

            if ( generator instanceof MpsLanguageConceptDeclarationGenerator) {
                Structure languageNode = ((MpsLanguageConceptDeclarationGenerator) generator).getDataStructure();
                if ( generatedLanguage.getRefConcept(languageNode.getName()) == null ) {
                    getContext().getPreparedMetadataDataStructures().add(languageNode);
                    getContext().addGeneratedLanguageNode(languageNode, generatedLanguage);
                    generatedNodes.add(languageNode);
                }
            } else if ( generator instanceof MpsLanguageEnumerationDeclarationGenerator ) {
                Enumeration languageNode = ((MpsLanguageEnumerationDeclarationGenerator) generator).getEnumerationStructure();
                getContext().getPreparedMetadataEnumerationStructures().add(languageNode);
                getContext().addGeneratedLanguageNode(languageNode, generatedLanguage);
                generatedNodes.add(languageNode);
            }


        }

        addComponent(MpsLanguageContextComponent.class).setGeneratedLanguage(generatedLanguage);
        MpsLanguageContextPrototypeComponent prototypeComponent = addComponent(MpsLanguageContextPrototypeComponent.class);
        prototypeComponent.setGeneratedLanguage(generatedLanguage);
        prototypeComponent.setConceptGenerators(conceptGenerators);


        String destinationPath = generatedLanguage.getDataClassesDestinationPath() != null ? generatedLanguage.getDataClassesDestinationPath() : getDestinationPath();
        for(AbstractLanguageNode node : generatedNodes ) {
            metadataNodeResourcesXMLStorage.saveNode(node, destinationPath);
        }

        logDebug("End process model " + generatedLanguage.getName());
    }

    protected String getLanguageUuid(String languageName) {
        String fileName = mpsParser.getLanguageMPLFileName(languageName);
        Document document = xmlUtils.parseToDocument(fileUtils.readToString(fileName));
        org.w3c.dom.Element element = document.getDocumentElement();
        if ( ! element.getTagName().equals("language") ) {
            throwException("First tag in .mpl file ? of language ? not equal 'language'", fileName, languageName);
        }
        String uuid = element.getAttribute("uuid");
        if ( uuid == null ) {
            throwException("UUID attribute not defined for 'language' tag in .mpl file ? of language ?", fileName, languageName);
        }
        return uuid;
    }


    protected void processLanguageStructureResolves() {

        HashMap<String, Structure> structuresCache = new HashMap<>();

        for (Structure structure : context.getPreparedMetadataDataStructures() ) {
            structuresCache.put(structure.getPackageName() + "." + structure.getName(), structure);
        }

        for (MpsLanguageGeneratorContext.DeferredDataStructureResolve resolve : context.getDeferredDataStructureResolves()) {
            Structure structure = structuresCache.get(resolve.className);
            if ( structure == null ) {
                String[] tokens = StringUtils.splitByLastTokenOccurrence(resolve.className, "\\.");
                structure = getMetadataBase().getNode(Structure.class, tokens[0], tokens[1], false);
                if ( structure == null ) {
                    structure = new Structure();
                    structure.setPackageName(tokens[0]);
                    structure.setName(tokens[1]);
                    structure.setJavaName(tokens[1]);
                    logError("!!! Structure not resolved : " + structure.getFullName());
                }
                structuresCache.put(resolve.className, structure);
//                throwException("Metadata node data structure ? not found in structures cache", resolve.className);
            }
            resolve.field.setElementStructure(structure);
        }

    }

    protected void processLanguageEnumerationResolves() {

        HashMap<String, Enumeration> structuresCache = new HashMap<>();

        for (Enumeration structure : context.getPreparedMetadataEnumerationStructures() ) {
            structuresCache.put(structure.getPackageName() + "." + structure.getName(), structure);
        }

        for (MpsLanguageGeneratorContext.DeferredEnumerationStructureResolve resolve : context.getDeferredEnumerationStructureResolves()) {
            Enumeration structure = structuresCache.get(resolve.className);
            if ( structure == null ) {
                throwException("Metadata node enum structure ? not found in structures cache", resolve.className);
            }
            resolve.field.setEnumeration(structure);
        }

    }

    protected void generateLanguageDataClasses() {

        for (MpsLanguageGeneratorContext.GeneratedLanguageNode generatedLanguageNode : context.getGeneratedLanguageNodes() ) {

            if ( ! generatedLanguageNode.generatedLanguage.isCreateDataClasses() ) {
                continue;
            }

            AbstractMetadataNodeGenerator        generator = null;
            AbstractMetadataNodeGeneratorContext context   = null;
            if ( generatedLanguageNode.languageNode instanceof Structure ) {
                generator = getService(MetadataNodeJavaDataClassGenerator.class, true);
                context   = new MetadataNodeJavaDataClassGeneratorContext();

            } else if ( generatedLanguageNode.languageNode instanceof Enumeration ) {
                generator = getService(MetadataNodeJavaEnumGenerator.class, true);
                context   = new MetadataNodeJavaEnumGeneratorContext();
            } else {
                throwException("Wrong metadataLanguageNode ? class ?", generatedLanguageNode.languageNode.getFullName(), generatedLanguageNode.languageNode.getClass().getName());
            }


            context.setLanguageNode(generatedLanguageNode.languageNode);


            generator.setContext(context);
            generator.setRootDestinationPath(
                    generatedLanguageNode.generatedLanguage.dataClassesDestinationPath != null
                            ? generatedLanguageNode.generatedLanguage.dataClassesDestinationPath : getDestinationPath()
            );

            generator.run();

        }
    }

    @Override
    protected void process() {

        logDebug("MPS language generator started");
        logDebug("parsing source models");

        for (GeneratedLanguage generatedLanguage : getContext().getGeneratedLanguages() ) {

            generatedLanguage.setUuid(getLanguageUuid(generatedLanguage.getName()));

            logDebug("parse " + generatedLanguage.getName() + " : " + generatedLanguage.getUuid());

            Model model = mpsParser.parseLanguage(generatedLanguage.getName(), getContext().getParserConfig());
            generatedLanguage.setModel(model);
            logDebug("Model " + generatedLanguage.getName() + " parsed");

        }

        for (GeneratedLanguage generatedLanguage : getContext().getGeneratedLanguages() ) {
            processModel(generatedLanguage);
        }

        processLanguageStructureResolves();
        processLanguageEnumerationResolves();
        generateLanguageDataClasses();

        logDebug("source models parsed");
        logDebug("MPS storage generator finished");
    }

}
