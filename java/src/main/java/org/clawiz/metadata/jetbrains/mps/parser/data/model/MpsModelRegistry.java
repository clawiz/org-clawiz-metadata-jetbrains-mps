/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.parser.data.model;

import org.clawiz.core.common.CoreException;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 */
@Root(name="registry")
public class MpsModelRegistry {

    @ElementList(name="language", inline = true, required = false)
    ArrayList<MpsModelRegistryLanguage> languages = new ArrayList<MpsModelRegistryLanguage>();

    public ArrayList<MpsModelRegistryLanguage> getLanguages() {
        return languages;
    }


    HashMap<String, String> indexesCache;

    private void putIndex(String index, String name) {
        indexesCache.put(index, name);
    }

    private void fillIndexesCache() {
        indexesCache = new HashMap<>();
        for (MpsModelRegistryLanguage language : languages ) {
            for (MpsModelRegistryLanguageConcept concept : language.getConcepts()) {
                putIndex(concept.getIndex(), concept.getName());
                for (MpsModelRegistryLanguageConceptProperty property : concept.getProperties() ) {
                    putIndex(property.getIndex(), property.getName());
                }
                for (MpsModelRegistryLanguageConceptChild child : concept.getChilds() ) {
                    putIndex(child.getIndex(), child.getName());
                }
                for (MpsModelRegistryLanguageConceptReference ref : concept.getReferences() ) {
                    putIndex(ref.getIndex(), ref.getName());
                }
            }
        }
    }

    public String indexToName(String index) {
        if ( indexesCache == null ) {
            fillIndexesCache();
        }
        String name = indexesCache.get(index);
        if ( name == null ) {
            throw new CoreException("Index '?' not found in model registry", index);
        }
        return name;
    }

}
