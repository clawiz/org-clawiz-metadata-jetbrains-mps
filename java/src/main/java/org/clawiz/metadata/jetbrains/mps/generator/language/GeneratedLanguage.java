package org.clawiz.metadata.jetbrains.mps.generator.language;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.parser.Model;

import java.util.ArrayList;
import java.util.HashMap;

public class GeneratedLanguage {


    private String   name;
    private Model    model;
    private String   uuid;

    boolean         createDataClasses = true;
    String          dataClassesDestinationPath;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isCreateDataClasses() {
        return createDataClasses;
    }

    public void setCreateDataClasses(boolean createDataClasses) {
        this.createDataClasses = createDataClasses;
    }

    public String getDataClassesDestinationPath() {
        return dataClassesDestinationPath;
    }

    public GeneratedLanguage withDataClassesDestinationPath(String dataClassesDestinationPath) {
        this.dataClassesDestinationPath = dataClassesDestinationPath;
        return this;
    }

    public class RefConcept {
        String conceptName;
        String refFieldName;
        String refMpsConceptName;
        String refGetter;
        GeneratedLanguage language;

        public RefConcept(String conceptName, String refFieldName, String  refMpsConceptName) {
            this.conceptName = conceptName;
            this.refFieldName = refFieldName;
            this.refMpsConceptName = refMpsConceptName;
            this.refGetter = "get" + StringUtils.toUpperFirstChar(refFieldName);
        }

        public String getConceptName() {
            return conceptName;
        }

        public String getRefFieldName() {
            return refFieldName;
        }

        public String  getRefMpsConceptName() {
            return refMpsConceptName;
        }

        public String getRefGetter() {
            return refGetter;
        }
    }

    ArrayList<RefConcept>        refConcepts              = new ArrayList<>();
    HashMap<String, RefConcept>  refConceptCache          = new HashMap<>();
    HashMap<String, RefConcept>  refConceptFullNamesCache = new HashMap<>();

    public RefConcept addRefConcept(String conceptName, String refFielfName, String  refMetadataNodeClass) {
        if ( refConceptCache.containsKey(conceptName)) {
            throw new CoreException("Generated language ? already contain refConcept ?", getName(), conceptName);
        }
        RefConcept refConcept = new RefConcept(conceptName, refFielfName, refMetadataNodeClass);
        refConcept.language   = this;
        refConcepts.add(refConcept);
        refConceptCache.put(conceptName, refConcept);
        return refConcept;
    }

    public GeneratedLanguage withRefConcept(String conceptName, String refFielfName, String  refMetadataNodeClass) {
        addRefConcept(conceptName, refFielfName, refMetadataNodeClass);
        return this;
    }


    private HashMap<String, String> missedRefConceptsCache = new HashMap<>();

    public RefConcept getRefConcept(String name) {
        RefConcept result = refConceptCache.get(name);
        if ( result == null) {
            result = refConceptFullNamesCache.get(name);
        }
        if ( result != null ) {
            return result;
        }
        if ( missedRefConceptsCache.containsKey(name)) {
            return null;
        }
        String[] tokens = StringUtils.splitByLastTokenOccurrence(name, "\\.");
        if ( tokens.length == 1 ) {
            missedRefConceptsCache.put(name, name);
            return null;
        }
        result = refConceptCache.get(tokens[1]);
        if ( result != null) {
            refConceptFullNamesCache.put(name, result);
        } else {
            missedRefConceptsCache.put(name, name);
        }
        return result;
    }

    public String getClassNameWithoutRef(String className) {
        RefConcept refConcept = getRefConcept(className);
        if ( refConcept == null) {
            return className;
        }
        return StringUtils.splitByLastTokenOccurrence(className, "\\.")[0] + "." +  refConcept.refMpsConceptName;
    }

    private HashMap<String, String> applyRefConceptToClassNameCache = new HashMap<>();
    public String applyRefConceptToClassName(String className) {
        String result = applyRefConceptToClassNameCache.get(className);
        if ( result != null) {
            return result;
        }

        result     = className;
        String[]   tokens     = StringUtils.splitByLastTokenOccurrence(className, "\\.");
        RefConcept refConcept = refConceptCache.get(tokens.length == 2 ? tokens[1] : tokens[0]);
        if ( refConcept != null ) {
            result = ( tokens.length == 2 ? tokens[0] + "." : "" ) + refConcept.refMpsConceptName;
        }
        if ( result.substring(result.length()-3).equals("Ref")) {
            result = result.substring(0, result.length() - 3);
        }

        applyRefConceptToClassNameCache.put(className, result);
        return result;
    }

}
