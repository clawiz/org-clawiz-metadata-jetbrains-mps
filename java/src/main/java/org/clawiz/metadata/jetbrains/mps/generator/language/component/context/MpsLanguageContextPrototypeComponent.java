/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.generator.language.component.context;

import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.metadata.jetbrains.mps.generator.language.AbstractMpsLanguageConceptGenerator;
import org.clawiz.metadata.jetbrains.mps.generator.language.component.AbstractMpsLanguageGeneratorJavaClassComponent;
import org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration.MpsConceptDeclarationClassComponent;
import org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration.MpsLanguageConceptDeclarationGenerator;
import org.clawiz.metadata.jetbrains.mps.parser.context.AbstractMpsLanguageContext;

import java.util.ArrayList;

public class MpsLanguageContextPrototypeComponent extends AbstractMpsLanguageGeneratorJavaClassComponent {


    ArrayList<AbstractMpsLanguageConceptGenerator> conceptGenerators;

    public ArrayList<AbstractMpsLanguageConceptGenerator> getConceptGenerators() {
        return conceptGenerators;
    }

    public void setConceptGenerators(ArrayList<AbstractMpsLanguageConceptGenerator> conceptGenerators) {
        this.conceptGenerators = conceptGenerators;
    }


    protected void addGetLanguageName() {
        JavaMethodElement method = addMethod("getLanguageName");
        method.setType(String.class);

        method.addText("return \"" + getGeneratedLanguage().getName() + "\";");

    }

    protected void addPrepare() {
        JavaMethodElement method = addMethod("prepare");

        method.addText("");

        for(AbstractMpsLanguageConceptGenerator conceptGenerator : conceptGenerators) {
            if ( ! ( conceptGenerator instanceof MpsLanguageConceptDeclarationGenerator ) ) {
                continue;
            }

            MpsConceptDeclarationClassComponent conceptComponent = conceptGenerator.getComponentByClass(MpsConceptDeclarationClassComponent.class);
            method.addText("addClassMap(" + conceptComponent.getPackageName() + "." + conceptComponent.getName() + ".class);");

        }


        method.addText("");

    }


    @Override
    public void process() {
        super.process();

        setName("MpsLanguageContextPrototype");

        setExtends(AbstractMpsLanguageContext.class);

        addGetLanguageName();
        addPrepare();
    }

}
