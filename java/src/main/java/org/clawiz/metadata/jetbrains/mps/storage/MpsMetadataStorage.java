/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.storage;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.MetadataBase;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeList;
import org.clawiz.core.common.metadata.storage.AbstractMetadataStorage;
import org.clawiz.core.common.storage.metadatastorage.MetadataStorageObject;
import org.clawiz.core.common.system.object.ObjectService;
import org.clawiz.core.common.utils.reflection.ReflectionUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.metadata.jetbrains.mps.parser.Model;
import org.clawiz.metadata.jetbrains.mps.parser.MpsParser;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.parser.context.MpsParserContext;
import org.clawiz.metadata.jetbrains.mps.parser.data.model.MpsModelImport;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class MpsMetadataStorage extends AbstractMetadataStorage {

    public static final String STORAGE_TYPE_FILE      = "file";
    public static final String STORAGE_TYPE_DIRECTORY = "directory";

    MpsParser mpsParser;


    private String getPath(String url, String storageType) {
        if (StringUtils.isEmpty(url)) {
            return null;
        }
        if ( ( StringUtils.substring(url, 0, storageType.length()) + ":").equals(storageType + ":")
                ) {
            return StringUtils.substring(url, storageType.length()+1, url.length());
        }
        return null;
    }


    private ConcurrentHashMap<Class, ConcurrentHashMap<String, Method>> deferredResolveSettersCache = new ConcurrentHashMap<>();

    public void processDeferredParseNodeResolves(MpsParserContext parserContext) {

        MetadataBase metadataBase = getService(MetadataBase.class);

        for (MpsParserContext.DeferredParseNodeResolve resolve : parserContext.getDeferredParseNodeResolves()) {
            MetadataNode referencedNode = parserContext.getMetadataNodeByMpsNodeId(resolve.referencedMpsNode.getMpsNodeId());

            if ( referencedNode == null ) {

                String refId = resolve.referencedMpsNode.getMpsNodeId();
                for (MpsModelImport modelImport : parserContext.getModel().getMpsModel().getImports() ) {

                    AbstractMpsNode refMpsNode = modelImport.getModel().getNodesIdCache().get(refId);
                    if ( refMpsNode != null ) {
                        referencedNode = metadataBase.getNode(refMpsNode.getMetadataNodeClass(), refMpsNode.getPackageName(), refMpsNode.getName(), false);
                        if ( referencedNode == null ) {
                            logDebug("Getting node as toMetadataNode() not from MetadataBase : " + refMpsNode.getFullName() + " ( " + refMpsNode.getMetadataNodeClass().getName() + ") ");
                            referencedNode = refMpsNode.toMetadataNode();
                            break;
                        }
                    }
                }

                if (referencedNode == null) {
                    throwException("Referenced mpsNode ? not found in parser context", resolve.referencedMpsNode.getMpsNodeId());
                }
            }

            ConcurrentHashMap<String, Method> methodsCache = deferredResolveSettersCache.get(resolve.metadataNode.getClass());
            if ( methodsCache == null ) {
                methodsCache = new ConcurrentHashMap<>();
                deferredResolveSettersCache.put(resolve.metadataNode.getClass(), methodsCache);
            }

            Method setter = methodsCache.get(resolve.fieldName);
            if ( setter == null ) {
                String name = (resolve.isArray ? "get" : "set") + StringUtils.toUpperFirstChar(resolve.fieldName);

                if ( resolve.isArray ) {
                    setter = ReflectionUtils.findMethod(resolve.metadataNode.getClass(), name);
                } else {
                    setter = ReflectionUtils.findMethod(resolve.metadataNode.getClass(), name, referencedNode.getClass());
                }
                if ( setter == null ) {
                    throwException("Method '?' for field '?' not present in ?", name, resolve.fieldName, resolve.metadataNode.getClass().getName());
                }

                try {
                    if ( resolve.isArray ) {
                        Object listObject = setter.invoke(resolve.metadataNode);
                        if ( listObject instanceof ArrayList ) {
                            ((ArrayList) listObject).add(referencedNode);
                        } else if ( listObject instanceof MetadataNodeList) {
                            ((MetadataNodeList<MetadataNode>) listObject).add(referencedNode);
                        } else {
                            throwException("Wrong field '?' list object class ?", name, listObject.getClass().getName());
                        }
                    } else {
                        setter.invoke(resolve.metadataNode, referencedNode);
                    }
                } catch (Exception e) {
                    throwException("Exception on call setter '?' method for field '?' of ? : ?", name, resolve.fieldName, resolve.metadataNode.getClass().getName(), e.getMessage(), e);
                }

            }


        }
    }

    MetadataNodeList toNodeList(Model model) {
        MetadataNodeList nodeList = new MetadataNodeList();
        for(AbstractMpsNode mpsNode : model.getRootNodes() ) {
            MetadataNode node = mpsNode.toMetadataNode();
            nodeList.add( node );
        }
        processDeferredParseNodeResolves(model.getParserContext());
        return nodeList;
    }


    private static ConcurrentHashMap<String, MetadataNodeList> nodeListCache = new ConcurrentHashMap<>();

    @Override
    public MetadataNodeList fetchNodeList(MetadataStorageObject storageObject) {
        String fileName = getPath(storageObject.getUrl(), STORAGE_TYPE_FILE);
        if ( fileName != null ) {
            return toNodeList(mpsParser.parseFile(fileName, storageObject.getPackageName()));
        }

        String directoryName = getPath(storageObject.getUrl(), STORAGE_TYPE_DIRECTORY);
        if ( directoryName != null ) {
            return toNodeList(mpsParser.parseDirectory(directoryName, storageObject.getPackageName()));
        }

        throw new CoreException("MPS metadata storage url '?' not in format '" + STORAGE_TYPE_FILE + ":' or '" + STORAGE_TYPE_DIRECTORY + ":'", new Object[]{storageObject.getUrl()});
    }
}
