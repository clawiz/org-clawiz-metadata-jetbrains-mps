package org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration.element;

import org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration.MpsConceptDeclarationClassPrototypeComponent;
import org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration.PreparedConceptField;

public class MpsConceptDeclarationFillForeignKeysMethodElement extends AbstractMpsConceptDeclarationMethodElement {

    public void processField(PreparedConceptField conceptField) {

        if ( conceptField.isPrimitive
                || conceptField.propertyType != MpsConceptDeclarationClassPrototypeComponent.PropertyType.REFERENCE
                || conceptField.linkedTargetConcept == null
                || ! conceptField.linkedTargetConcept.isRootable()) {
            return;
        }

        String getter = conceptField.dataStructureField.getGetMethodName() + "()";
        if ( conceptField.isArray ) {
            addText("for ( " + conceptField.type + " node : " + getter + " ) {");
            addText("    addForeignKey(node);");
            addText("}");
        } else {
            addText("addForeignKey(" + getter + ");");
        }

    }

    @Override
    public void process() {
        super.process();

        setName("fillForeignKeys");

        if ( getDataStructure().getExtendsNode() != null ) {
            addText("super.fillForeignKeys();");
        }

        for (PreparedConceptField field : getPreparedConceptFields() ) {
            processField(field);
        }


    }
}
