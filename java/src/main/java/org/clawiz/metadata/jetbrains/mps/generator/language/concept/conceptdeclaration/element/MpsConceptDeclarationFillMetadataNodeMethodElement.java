package org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration.element;


import org.clawiz.core.common.metadata.data.language.node.element.field.AttributeStructureField;
import org.clawiz.core.common.metadata.data.language.node.element.field.ElementStructureField;
import org.clawiz.metadata.jetbrains.mps.generator.language.GeneratedLanguage;
import org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration.PreparedConceptField;

public class MpsConceptDeclarationFillMetadataNodeMethodElement extends AbstractMpsConceptDeclarationMethodElement {

    public void processAttributeField(PreparedConceptField conceptField) {

        AttributeStructureField field = (AttributeStructureField) conceptField.dataStructureField;

        if ( field.isArray() ) {
            throwException("Attribute field ? arrays not implemented", getDataStructure().getFullName() + "." + field.getName());
        }

        String getter = field.getGetMethodName() + "()";
        if ( conceptField.isEnumeration  ) {
            getter = "( " + getter + " != null ? " + conceptField.getTypeMetadataNodeClassName() + ".valueOf(" + getter + ".toString()) : null )";
        }

        addText("structure." + field.getSetMethodName() + "(" + getter + ");");
    }

    public void processElementField(PreparedConceptField conceptField) {

        ElementStructureField field = (ElementStructureField) conceptField.dataStructureField;

        String getter = null;
        String setter = null;
        String lpad   = "";

        if ( field.isArray() ) {
            addText("structure." + field.getGetMethodName() + "().clear();");
            addText("for (" + conceptField.type + " mpsNode : " + field.getGetMethodName() + "() ) {");
            getter = "mpsNode";
            setter = field.getGetMethodName() + "().add";
            lpad   = "    ";
        } else {
            getter = field.getGetMethodName() + "()";
            setter = field.getSetMethodName();
        }

        GeneratedLanguage.RefConcept refConcept = getLanguage().getRefConcept(conceptField.getTypeMetadataNodeClassName());

        boolean isReference = field.isReference();

        if ( refConcept != null ) {
            getter = getter + "." + refConcept.getRefGetter() + "()";
            isReference = true;
        }


        addText(lpad + "if ( " + getter + " != null ) {");

        if ( isReference ) {
            addText(lpad + "    getParserContext().addDeferredParseNodeResolve(structure, \"" + field.getJavaVariableName() + "\", " + field.isArray() + ", " + getter + ");");
        } else {
            addText(lpad + "    structure." + setter + "((" + conceptField.getTypeMetadataNodeClassName() + ") " + getter + ".toMetadataNode(structure, \"" + field.getJavaVariableName()  + "\"));");
        }
        addText(lpad + "} else {");
        addText(lpad + "    structure." + setter + "(null);");
        addText(lpad + "}");

        if ( field.isArray() ) {
            addText("}");
        }


    }

    public void processField(PreparedConceptField conceptField) {

        addText("");
        if ( conceptField.isPrimitive ) {
            processAttributeField(conceptField);
        } else {
            processElementField(conceptField);
        }

    }

    @Override
    public void process() {
        super.process();

        setName("fillMetadataNode");
        setAccessLevel(AccessLevel.PROTECTED);

        addParameter("MetadataNode", "node");

        if ( getComponent().getConcept().getExtends() != null ) {
            addText("super.fillMetadataNode(node);");
        }


        if ( getLanguage().getRefConcept(getDataStructureClassName()) != null ) {
            return;
        }

        addText(getDataStructureClassName() + " structure = (" + getDataStructureClassName() + ") node;");
        for (PreparedConceptField field : getPreparedConceptFields() ) {
            processField(field);
        }
        addText("");

    }
}
