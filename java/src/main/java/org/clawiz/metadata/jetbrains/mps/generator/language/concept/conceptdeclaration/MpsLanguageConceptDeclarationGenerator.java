/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.generator.language.concept.conceptdeclaration;

import jetbrains.mps.lang.structure.metadata.language.adapter.jetbrains.mps.data.MpsConceptDeclaration;
import org.clawiz.core.common.metadata.data.language.node.element.Structure;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.metadata.jetbrains.mps.generator.language.AbstractMpsLanguageConceptGenerator;

import java.util.ArrayList;

public class MpsLanguageConceptDeclarationGenerator extends AbstractMpsLanguageConceptGenerator {


    MpsConceptDeclaration     concept;
    Structure                 dataStructure;

    public MpsConceptDeclaration getConcept() {
        return concept;
    }

    @Override
    public void setMetadataNode(AbstractMpsNode metadataNode) {
        super.setMetadataNode(metadataNode);
        concept = (MpsConceptDeclaration) metadataNode;
    }

    public Structure getDataStructure() {
        return dataStructure;
    }

    MpsConceptDeclarationClassPrototypeComponent classPrototypeComponent;

    public ArrayList<PreparedConceptField> getPreparedConceptFields() {
        return classPrototypeComponent.getPreparedConceptFields();
    }

    public String getDataStructureClassName() {
        return dataStructure.getDataClassName();
    }

    @Override
    protected void process() {
        super.process();


        dataStructure = new Structure();
        dataStructure.setPackageName(getMetadataNodeStructurePackageName());
        dataStructure.setName(concept.getName());

        if ( getConcept().getExtends() != null ) {
            String extendsType = PreparedConceptField.mpsTypeToMetadataNodeClassName(concept.getModel().nodeToJavaClassName(concept.getExtends()));
            String[]    tokens = StringUtils.splitByLastTokenOccurrence(extendsType, "\\.");
            Structure extendsDataStructure = new Structure();
            extendsDataStructure.setPackageName(tokens[0]);
            extendsDataStructure.setJavaName(tokens[1]);
            extendsDataStructure.setName(tokens[1]);
            dataStructure.setExtendsNode(extendsDataStructure);
        }

        addComponent(MpsConceptDeclarationClassComponent.class);
        classPrototypeComponent = addComponent(MpsConceptDeclarationClassPrototypeComponent.class);

    }

}
