package org.clawiz.metadata.jetbrains.mps.storage.mpsmodel;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;

import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class MpsModelServicePrototype extends AbstractTypeService<MpsModelObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).fullNameToId("org.clawiz.metadata.jetbrains.mps.storage.MpsModel");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.metadata.jetbrains.mps.storage.MpsModel"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      mpsModel Checked object
    */
    public void check(MpsModelObject mpsModel) {
        
        if ( mpsModel == null ) {
            throwException("Cannot check null ?", new Object[]{"MpsModelObject"});
        }
        
        mpsModel.fillDefaults();
        
        
        if ( nameToId(mpsModel.getName(), mpsModel.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "MpsModel", mpsModel.toName() });
        }
        
        if ( modelUidToId(mpsModel.getModelUid(), mpsModel.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "MpsModel", mpsModel.toModelUid() });
        }
        
    }
    
    /**
    * Create new MpsModelObject instance and fill default values
    * 
    * @return     Created object
    */
    public MpsModelObject create() {
        
        MpsModelObject mpsModel = new MpsModelObject();
        mpsModel.setService((MpsModelService) this);
        
        mpsModel.fillDefaults();
        
        return mpsModel;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public MpsModelObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select package_name, name, model_uid, file_name, directory_name from cw_jetbrains_mps_models where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"MpsModel", id});
        }
        
        MpsModelObject result = new MpsModelObject();
        
        result.setService((MpsModelService) this);
        result.setId(id);
        result.setPackageName(statement.getString(1));
        result.setName(statement.getString(2));
        result.setModelUid(statement.getString(3));
        result.setFileName(statement.getString(4));
        result.setDirectoryName(statement.getString(5));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of MpsModelObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public MpsModelList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of MpsModelObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public MpsModelList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        MpsModelList result = new MpsModelList();
        
        
        Statement statement = executeQuery("select id, package_name, name, model_uid, file_name, directory_name from cw_jetbrains_mps_models"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            MpsModelObject object = new MpsModelObject();
        
            object.setService((MpsModelService) this);
            object.setId(statement.getBigDecimal(1));
            object.setPackageName(statement.getString(2));
            object.setName(statement.getString(3));
            object.setModelUid(statement.getString(4));
            object.setFileName(statement.getString(5));
            object.setDirectoryName(statement.getString(6));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'Name' fields
    * 
    * @param      name Name
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name) {
        return nameToId(name, null);
    }
    
    /**
    * Find id of record by key 'Name' fields with id not equal skipId
    * 
    * @param      name   Name
    * @param      skipId Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name, BigDecimal skipId) {
        
        if ( name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_jetbrains_mps_models where upper_name = ?", name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_jetbrains_mps_models where upper_name = ? and id != ?", name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'Name' fields or create new record with values set to given parameters
    * 
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal nameToId(java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = nameToId(name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        MpsModelObject object = create();
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'Name' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select name from cw_jetbrains_mps_models where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        MpsModelObject object = new MpsModelObject();
        object.setService((MpsModelService)this);
        object.setId(id);
        object.setName(statement.getString(1));
        
        statement.close();
        
        return object.toName();
    }
    
    /**
    * Find id of record by key 'ModelUid' fields
    * 
    * @param      modelUid ModelUid
    * @return     Id of found record or null
    */
    public BigDecimal modelUidToId(java.lang.String modelUid) {
        return modelUidToId(modelUid, null);
    }
    
    /**
    * Find id of record by key 'ModelUid' fields with id not equal skipId
    * 
    * @param      modelUid ModelUid
    * @param      skipId   Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal modelUidToId(java.lang.String modelUid, BigDecimal skipId) {
        
        if ( modelUid == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_jetbrains_mps_models where upper_model_uid = ?", modelUid.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_jetbrains_mps_models where upper_model_uid = ? and id != ?", modelUid.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'ModelUid' fields or create new record with values set to given parameters
    * 
    * @param      modelUid            ModelUid
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal modelUidToId(java.lang.String modelUid, boolean createNewIfNotFound) {
        
        BigDecimal id = modelUidToId(modelUid, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        MpsModelObject object = create();
        object.setModelUid(modelUid);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'ModelUid' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToModelUid(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select model_uid from cw_jetbrains_mps_models where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        MpsModelObject object = new MpsModelObject();
        object.setService((MpsModelService)this);
        object.setId(id);
        object.setModelUid(statement.getString(1));
        
        statement.close();
        
        return object.toModelUid();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      mpsModelObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(MpsModelObject mpsModelObject) {
        return mpsModelObject.toName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, MpsModelObject oldMpsModelObject, MpsModelObject newMpsModelObject) {
        
        MpsModelObject o = oldMpsModelObject != null ? oldMpsModelObject : new MpsModelObject();
        MpsModelObject n = newMpsModelObject != null ? newMpsModelObject : new MpsModelObject();
        
        
        executeUpdate("insert into a_cw_jetbrains_mps_models (scn, action_type, id , o_package_name, o_name, o_upper_name, o_model_uid, o_upper_model_uid, o_file_name, o_directory_name, n_package_name, n_name, n_upper_name, n_model_uid, n_upper_model_uid, n_file_name, n_directory_name) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getPackageName(), o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, o.getModelUid(), o.getModelUid() != null ? o.getModelUid().toUpperCase() : null, o.getFileName(), o.getDirectoryName(), n.getPackageName(), n.getName(), n.getName() != null ? n.getName().toUpperCase() : null, n.getModelUid(), n.getModelUid() != null ? n.getModelUid().toUpperCase() : null, n.getFileName(), n.getDirectoryName());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      mpsModel Saved object
    */
    public void save(MpsModelObject mpsModel) {
        
        if ( mpsModel == null ) {
            throwException("Cannot save NULL ?", new Object[]{"MpsModelObject"});
        }
        
        TransactionAction transactionAction;
        MpsModelObject oldMpsModel;
        if ( mpsModel.getService() == null ) {
            mpsModel.setService((MpsModelService)this);
        }
        
        check(mpsModel);
        
        if ( mpsModel.getId() == null ) {
        
            mpsModel.setId(getObjectService().createObject(getTypeId()));
        
            oldMpsModel = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, mpsModel.getId());
        
            executeUpdate("insert into cw_jetbrains_mps_models" 
                          + "( id, package_name, name, upper_name, model_uid, upper_model_uid, file_name, directory_name ) "
                          + "values"
                          + "(?, ?, ?, ?, ?, ?, ?, ?)",
                          mpsModel.getId(), mpsModel.getPackageName(), mpsModel.getName(), ( mpsModel.getName() != null ? mpsModel.getName().toUpperCase() : null ), mpsModel.getModelUid(), ( mpsModel.getModelUid() != null ? mpsModel.getModelUid().toUpperCase() : null ), mpsModel.getFileName(), mpsModel.getDirectoryName() );
        
        } else {
        
            oldMpsModel = load(mpsModel.getId());
            if ( oldMpsModel.equals(mpsModel) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, mpsModel.getId());
        
            executeUpdate("update cw_jetbrains_mps_models set "
                          + "package_name = ?, name = ?, upper_name = ?, model_uid = ?, upper_model_uid = ?, file_name = ?, directory_name = ? "
                          + "where id = ?",
                          mpsModel.getPackageName(), mpsModel.getName(), ( mpsModel.getName() != null ? mpsModel.getName().toUpperCase() : null ), mpsModel.getModelUid(), ( mpsModel.getModelUid() != null ? mpsModel.getModelUid().toUpperCase() : null ), mpsModel.getFileName(), mpsModel.getDirectoryName(), mpsModel.getId() );
        
        }
        
        saveAudit(transactionAction, oldMpsModel, mpsModel);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        MpsModelObject oldMpsModelObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldMpsModelObject, null);
        
        getObjectService().deleteObject(id);
        
        executeUpdate("delete from cw_jetbrains_mps_models where id = ?", id);
        
    }
}
