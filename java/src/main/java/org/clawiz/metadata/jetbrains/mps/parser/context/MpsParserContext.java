/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.parser.context;

import org.clawiz.core.common.Core;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.metadata.jetbrains.mps.MpsBase;
import org.clawiz.metadata.jetbrains.mps.parser.data.model.MpsModel;
import org.clawiz.metadata.jetbrains.mps.parser.data.model.MpsModelImport;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.metadata.jetbrains.mps.parser.MpsParserConfig;
import org.clawiz.core.common.utils.reflection.ReflectionUtils;
import org.clawiz.metadata.jetbrains.mps.parser.data.model.MpsModelRegistry;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.metadata.jetbrains.mps.parser.Model;
import org.clawiz.metadata.jetbrains.mps.parser.MpsParser;
import org.clawiz.core.common.utils.StringUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 24.04.13
 * Time: 16?5
 * To change this template use File | Settings | File Templates.
 */
public class MpsParserContext {

    MpsParserConfig config;

    MpsModel        mpsModel;
    Model           model;

    ArrayList<MpsParserLanguageClassMap>       languageClassMaps             = new ArrayList<>();
    HashMap<String, MpsParserLanguageClassMap> languageClassMapsCache        = new HashMap<>();
    HashMap<Class, Class> metadataNodeToMpsNodeClassMapsCache                = new HashMap<>();

    ArrayList<MpsParserLanguage> languages = new ArrayList<>();
    HashMap<String, MpsParserLanguage> languagesCache = new HashMap<>();

    HashMap<Class, HashMap<String, Method>> classMethodsCache = new HashMap<>();

    ReflectionUtils reflectionUtils;

    MpsParser parser;

    Session session;

    ArrayList<MpsModelImport> skippedImporta                  = new ArrayList<>();
    HashMap<String, MpsModelImport> skippedImportsIndexCache  = new HashMap<>();

    ArrayList<AbstractMpsNode> deferredTypesResolveMpsNodesQueue     = new ArrayList<>();
    ArrayList<AbstractMpsNode> deferredProcessesResolveMpsNodesQueue = new ArrayList<>();
    ArrayList<AbstractMpsNode> deferredStructureResolveMpsNodesQueue = new ArrayList<>();



    public MpsParserConfig getConfig() {
        return config;
    }

    public void setConfig(MpsParserConfig config) {
        this.config = config;
    }

    public MpsParserContext(Session session) {
        this.session = session;
    }


    public MpsParser getParser() {
        return parser;
    }

    public void setParser(MpsParser parser) {
        this.parser = parser;
    }

    public MpsModel getMpsModel() {
        return mpsModel;
    }

    public ReflectionUtils getReflectionUtils() {
        if ( reflectionUtils == null ) {
            reflectionUtils = session.getService(ReflectionUtils.class);
        }
        return reflectionUtils;
    }

    public void setMpsModel(MpsModel mpsModel) {
        this.mpsModel = mpsModel;
        this.model.setParserContext(this);
    }

    public void addLanguageClassMap(String languageName, Class nodeClass) {
        String[] tokens = StringUtils.splitAndTrim(nodeClass.getName(), "\\.");
        String className = tokens[tokens.length-1];
        addLanguageClassMap(languageName, className, nodeClass);
        if ( className.substring(0,3).equalsIgnoreCase("Mps")) {
            className = className.substring(3);
            addLanguageClassMap(languageName, className, nodeClass);
        }
    }

    public ArrayList<MpsParserLanguageClassMap> getLanguageClassMaps() {
        return languageClassMaps;
    }

    public HashMap<Class, Class> getMetadataNodeToMpsNodeClassMapsCache() {
        return metadataNodeToMpsNodeClassMapsCache;
    }

    public void addLanguageClassMap(String modelName, String mpsClassName, Class nodeClass) {
        if ( modelName == null || mpsClassName == null || nodeClass == null ) {
            throw new CoreException("All parameters of addClassMap must be not null : (?, ?, ?)", new Object[]{modelName, mpsClassName, nodeClass});
        }
        String                    key      = modelName.toUpperCase() + "." + mpsClassName.toUpperCase();
        MpsParserLanguageClassMap classMap = languageClassMapsCache.get(key);
        if ( classMap != null ) {
            if ( classMap.getNodeClass() == nodeClass ) {
                return;
            }
            throw new CoreException("Class map (?, ?, ?) already defined for ", new Object[]{modelName, mpsClassName, nodeClass, classMap.getNodeClass()});
        }

        classMap = new MpsParserLanguageClassMap(modelName, mpsClassName, nodeClass);
        languageClassMaps.add(classMap);
        languageClassMapsCache.put(key, classMap);
    }

    public void addLanguage(String name, String alias) {
        if ( name == null || alias == null ) {
            throw new CoreException("All parameters of addLanguage must be not null : (?, ?, ?)", new Object[]{name, alias});
        }
        MpsParserLanguage language =  new MpsParserLanguage(name, alias);
        languages.add(language);
        languagesCache.put(alias.toUpperCase(), language);
    }

    public MpsParserLanguage getLanguageByAlias(String alias) {
        if ( alias == null ) {
            throw new CoreException("All parameters of getLanguage must be not null : (? )", new Object[]{alias});
        }
        MpsParserLanguage language = languagesCache.get(alias.toUpperCase());
        return language;
    }

    HashMap<String, AbstractMpsLanguageContext> loadedLanguageCache = new HashMap<>();
    public AbstractMpsLanguageContext getLanguageContext(String languageName) {
        AbstractMpsLanguageContext languageContext = loadedLanguageCache.get(languageName);
        if ( languageContext != null ) {
            return languageContext;
        }

        String contextPackageName = StringUtils.splitByLastTokenOccurrence(languageName, "\\.")[0];

        // remove last token from language name, because mps add ".structure" after real name
        String trimmedLanguageName = StringUtils.splitByLastTokenOccurrence(languageName, "\\.")[0];
        String className           = MpsBase.removeLastLanguageFromPackageName(trimmedLanguageName) + "." + MpsBase.METADATA_LANGUAGE_ADAPTER_JETBRAINS_MPS_PACKAGE_NAME + ".MpsLanguageContext";

        try {
            languageContext = (AbstractMpsLanguageContext) session.getService(Core.getClassByName(className));
        } catch (Exception e) {
            throw new CoreException("Exception on get mps language ? context class ? : ?", contextPackageName, className, e.getMessage(), e);
        }

        if ( languageContext != null ) {
            for(MpsParserLanguageClassMap cm : languageContext.getClassMaps()) {
                addLanguageClassMap(languageName, cm.getTypeName(), cm.getNodeClass());
            }
        }

        loadedLanguageCache.put(languageName, languageContext);
        return languageContext;

    }

    public MpsParserLanguageClassMap getLanguageClassMap(String languageName, String typeName) {
        if ( languageName == null || typeName == null ) {
            throw new CoreException("All parameters of getLanguageClassMap must be not null : (?, ?)", languageName, typeName);
        }
        String                  key         = languageName.toUpperCase() + "." + typeName.toUpperCase();
        MpsParserLanguageClassMap classMap  = languageClassMapsCache.get(key);
        if ( classMap == null ) {

            getLanguageContext(languageName);

            classMap = languageClassMapsCache.get(key);
            if ( classMap == null ) {
                throw new CoreException("Class map (?, ?) not found", new Object[]{languageName, typeName});
            }

        }
        return classMap;
    }

    public Class getNodeClass(String _nodeTypeName, String concept, MpsModelRegistry registry) {
        String nodeTypeName;
        if ( concept == null ) {
            nodeTypeName = _nodeTypeName;
        } else {
            nodeTypeName = "." + registry.indexToName(concept);
        }
        if ( nodeTypeName.charAt(0) != '.') {
            String[] tokens = StringUtils.splitAndTrim(nodeTypeName, "\\.");
            if ( tokens.length < 2) { throw new CoreException("Node type nodeTypeName '?' not in format 'alias.nodeTypeName'", new Object[]{nodeTypeName}); }

            MpsParserLanguage language = getLanguageByAlias(tokens[0]);
            if ( language == null ) {
                throw new CoreException("Language with alias '?' not found", new Object[]{tokens[0]});
            }
            MpsParserLanguageClassMap classMap = getLanguageClassMap(language.getName(), tokens[1]);
            if (classMap == null ) {
                throw new CoreException("Class map for '?' '?' not found", tokens[0], tokens[1]);
            }

            return classMap.getNodeClass();
        } else {
            String[] tokens = StringUtils.splitByLastTokenOccurrence(nodeTypeName.substring(1), "\\.");
            MpsParserLanguageClassMap classMap = getLanguageClassMap(tokens[0], tokens[1]);
            if (classMap == null ) {
                throw new CoreException("Class map for '?' '?' not found", tokens[0], tokens[1]);
            }

            return classMap.getNodeClass();
        }

    }

    public AbstractMpsNode getNodeById(String id) {
        AbstractMpsNode node = model.getNodesIdCache().get(id);
        if ( node != null ) {
            return node;
        }
        String[] tokens = StringUtils.splitAndTrim(id, "\\.");
        if ( tokens.length < 2 ) {
            tokens = StringUtils.splitAndTrim(id, ":");
        }
        if ( tokens.length < 2 ) {
            return null;
        }
        Model idModel = model.getImportedModelsIndexCache().get(tokens[0]);
        if ( idModel == null ) {
            return null;
        }
        node = idModel.getNodesIdCache().get(tokens[1]);
        model.getNodesIdCache().put(id, node);
        return node;
    }

    public <T extends AbstractMpsNode> T getNodeByClass(Class<T> clazz, MetadataNode referenceNameNode) {
        return getNodeByClass(clazz, referenceNameNode.getPackageName(), referenceNameNode.getName());
    }

    public <T extends AbstractMpsNode> T getNodeByClass(Class<T> clazz, String packageName, String name) {
        if ( clazz == null ) {
            throw new CoreException("Cannot get node of null class");
        }
        if ( packageName == null ) {
            throw new CoreException("Cannot get node ? with null package name", clazz.getName());
        }
        if ( name == null ) {
            throw new CoreException("Cannot get node ? with null name", clazz.getName());
        }
        for (AbstractMpsNode node : getModel().getRootNodes() ) {
            if ( clazz.isAssignableFrom(node.getClass())) {
                if ( packageName.equals(node.getPackageName()) && name.equals(node.getName()) ) {
                    return (T) node;
                }
            }
        }
        throw new CoreException("Mps node ?.? of class ? not found in parser context", packageName, name, clazz.getName());
    }

    public Method getClassMethod(Class clazz, String methodName) {
        HashMap<String, Method> mc = classMethodsCache.get(clazz);
        if ( mc == null ) {
            return null;
        }
        return mc.get(methodName);
    }

    public void putClassMethod(Class clazz, String methodName, Method method) {
        HashMap<String, Method> mc = classMethodsCache.get(clazz);
        if ( mc == null ) {
            mc = new HashMap<>();
            classMethodsCache.put(clazz, mc);
        }
        mc.put(methodName, method);
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public String getPackageName() {
        return this.getModel().getPackageName();
    }

    public ArrayList<MpsModelImport> getSkippedImporta() {
        return skippedImporta;
    }

    public void addSkippedImport(MpsModelImport modelImport) {
        skippedImporta.add(modelImport);
        skippedImportsIndexCache.put(modelImport.getIndex(), modelImport);
    }

    public boolean isImportIndexSkipped(String index) {
        return skippedImportsIndexCache.containsKey(index);
    }

    public ArrayList<AbstractMpsNode> getDeferredTypesResolveMpsNodesQueue() {
        return deferredTypesResolveMpsNodesQueue;
    }

    public ArrayList<AbstractMpsNode> getDeferredStructureResolveMpsNodesQueue() {
        return deferredStructureResolveMpsNodesQueue;
    }

    public ArrayList<AbstractMpsNode> getDeferredProcessesResolveMpsNodesQueue() {
        return deferredProcessesResolveMpsNodesQueue;
    }

    HashMap<String, MetadataNode> parsedNodesCache = new HashMap<>();
    public void registerMetadataNodeMpsNodeId(String mpsNodeId, MetadataNode node) {
        parsedNodesCache.put(mpsNodeId, node);
    }

    public MetadataNode getMetadataNodeByMpsNodeId(String mpsNodeId) {
        return parsedNodesCache.get(mpsNodeId);
    }

    public class DeferredParseNodeResolve {
        public MetadataNode    metadataNode;
        public String          fieldName;
        public boolean         isArray;
        public AbstractMpsNode referencedMpsNode;
    }
    ArrayList<DeferredParseNodeResolve> deferredParseNodeResolves = new ArrayList<>();
    public void addDeferredParseNodeResolve(MetadataNode metadataNode, String fieldName, boolean isArray, AbstractMpsNode referencedMpsNode) {

        if ( referencedMpsNode == null ) {
            return;
        }

        DeferredParseNodeResolve resolve = new DeferredParseNodeResolve();
        deferredParseNodeResolves.add(resolve);

        resolve.metadataNode      = metadataNode;
        resolve.fieldName         = fieldName;
        resolve.isArray           = isArray;
        resolve.referencedMpsNode = referencedMpsNode;
    }

    public ArrayList<DeferredParseNodeResolve> getDeferredParseNodeResolves() {
        return deferredParseNodeResolves;
    }
}
