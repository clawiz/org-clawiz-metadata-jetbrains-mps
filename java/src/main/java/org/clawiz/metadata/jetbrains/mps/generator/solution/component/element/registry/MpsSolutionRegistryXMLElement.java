/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.registry;

import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.AbstractMpsSolutionXMLElement;
import org.clawiz.metadata.jetbrains.mps.generator.solution.component.element.concept.MpsSolutionConceptNodeXMLElement;

import java.util.ArrayList;
import java.util.HashMap;

public class MpsSolutionRegistryXMLElement extends AbstractMpsSolutionXMLElement {

    public static String JETBRAINS_MPS_CORE_INAMEDCONCEPT_NAME_PROPERTY_ID = "1169194664001";

    @Override
    public String getTagName() {
        return "registry";
    }

    ArrayList<MpsSolutionRegistryLanguageXMLElement>              languages                 = new ArrayList<>();
    HashMap<String, MpsSolutionRegistryLanguageXMLElement>        languageIdsCache          = new HashMap<>();
    HashMap<String, MpsSolutionRegistryLanguageXMLElement>        languageNamesCache        = new HashMap<>();
    HashMap<String, MpsSolutionRegistryLanguageConceptXMLElement> languageConceptNamesCache = new HashMap<>();
    HashMap<AbstractMpsNode, String>                              mpsNodeToIdCache          = new HashMap<>();
    HashMap<String, AbstractMpsSolutionRegistryIndexedElement>    conceptIdToElementCache   = new HashMap<>();

    public MpsSolutionRegistryLanguageXMLElement registerLanguage(String id, String name) {

        MpsSolutionRegistryLanguageXMLElement languageElement = languageIdsCache.get(id);
        if ( languageElement != null ) {
            return languageElement;
        }

        languageElement = addElement(MpsSolutionRegistryLanguageXMLElement.class);
        languageElement.setName(name);
        languageElement.setLanguageId(id);

        languageIdsCache.put(id, languageElement);
        languageNamesCache.put(name, languageElement);


        return languageElement;
    }


    public HashMap<String, MpsSolutionRegistryLanguageConceptXMLElement> getLanguageConceptNamesCache() {
        return languageConceptNamesCache;
    }

    public HashMap<String, AbstractMpsSolutionRegistryIndexedElement> getConceptIdToElementCache() {
        return conceptIdToElementCache;
    }

    private int index = 1;
    public String createNewIndex() {
        String str = "00000" + (index++);
        str = str.substring(str.length()-5);
        return "e" + str;
    }

    public void prepareConceptNodeId(MpsSolutionConceptNodeXMLElement node) {
        String id = getComponent().getGenerator().createNewConceptId();
        node.setConceptId(id);
    }

    public HashMap<AbstractMpsNode, String> getMpsNodeToIdCache() {
        return mpsNodeToIdCache;
    }


    public <T extends AbstractMpsSolutionRegistryIndexedElement> T createIndexedElement(String id, String name, Class<T> clazz, AbstractMpsSolutionXMLElement parentElement) {

        T element = (T) parentElement.getElement(name);

        if ( element != null ) {
            return element;
        }

        element = parentElement.addElement(clazz, name);

        element.setConceptId(id);
        element.setIndex(createNewIndex());

        this.conceptIdToElementCache.put(id, element);

        return element;
    }

    String jetbrainsINamedNamePropertyIndex;
    String jetbrainsVirtualPackagePropertyIndex;

    public String getJetbrainsINamedNamePropertyIndex() {
        return jetbrainsINamedNamePropertyIndex;
    }

    public String getJetbrainsVirtualPackagePropertyIndex() {
        return jetbrainsVirtualPackagePropertyIndex;
    }

    public static final String JETBRAINS_MPS_LANG_CORE_BASECONCEPT_CONCEPT_ID = "1133920641626";
    public static final String JETBRAINS_MPS_LANG_CORE_INAMEDCONCEPT_CONCEPT_ID = "1169194658468";

    public void addJetBrainsCommonLanguages() {
        MpsSolutionRegistryLanguageXMLElement  language = registerLanguage("ceab5195-25ea-4f22-9b92-103b95ca8c0c","jetbrains.mps.lang.core");

        MpsSolutionRegistryLanguageConceptXMLElement concept  = language.registerConcept(JETBRAINS_MPS_LANG_CORE_BASECONCEPT_CONCEPT_ID, "jetbrains.mps.lang.core.structure.BaseConcept");
        jetbrainsVirtualPackagePropertyIndex =
                concept.registerProperty( MpsSolutionRegistryLanguageConceptPropertyXMLElement.PropertyType.PROPERTY, "1193676396447", "virtualPackage")
                        .getIndex();

        concept = language.registerConcept(JETBRAINS_MPS_LANG_CORE_INAMEDCONCEPT_CONCEPT_ID, "jetbrains.mps.lang.core.structure.INamedConcept");
        jetbrainsINamedNamePropertyIndex =
                concept.registerProperty( MpsSolutionRegistryLanguageConceptPropertyXMLElement.PropertyType.PROPERTY, JETBRAINS_MPS_CORE_INAMEDCONCEPT_NAME_PROPERTY_ID, "name")
                        .getIndex();


    }

    @Override
    public void prepare() {
        super.prepare();
    }

    @Override
    public void process() {
        super.process();
    }
}
