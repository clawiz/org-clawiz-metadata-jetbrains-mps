/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.parser.data.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 23.04.13
 * Time: 16?5
 * To change this template use File | Settings | File Templates.
 */
@Root(name="node")
public class MpsModelNode {

    @Attribute(required = false)
    String type;

    @Attribute(required = false)
    String typeId;

    @Attribute
    String id;

    @Attribute(required = false)
    String role;

    @Attribute(required = false)
    String roleId;

    @Attribute(required = false)
    String nodeInfo;

    @Attribute(required = false)
    String concept;

    @ElementList(name="property", inline = true, required = false)
    ArrayList<MpsModelProperty> properties = new ArrayList<MpsModelProperty>();

    @ElementList(name="link", inline = true, required = false)
    ArrayList<MpsModelLink> links= new ArrayList<MpsModelLink>();

    @ElementList(name="ref", inline = true, required = false)
    ArrayList<MpsModelNodeRef> refs= new ArrayList<MpsModelNodeRef>();

    @ElementList(name="node", inline = true, required = false)
    ArrayList<MpsModelNode> childs= new ArrayList<MpsModelNode>();


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public ArrayList<MpsModelProperty> getProperties() {
        return properties;
    }

    public void setProperties(ArrayList<MpsModelProperty> properties) {
        this.properties = properties;
    }

    public ArrayList<MpsModelLink> getLinks() {
        if ( links.size() > 0 || refs.size() == 0 ) {
            return links;
        }
        for (MpsModelNodeRef ref : refs ) {
            MpsModelLink link = new MpsModelLink();
            links.add(link);

            link.setResolveInfo(ref.getResolve());
            link.setRole(ref.getRole());
            link.setTargetNodeId(ref.getNode() != null ? ref.getNode() : ref.getTo());

        }
        return links;
    }

    public void setLinks(ArrayList<MpsModelLink> links) {
        this.links = links;
    }

    public ArrayList<MpsModelNode> getChilds() {
        return childs;
    }

    public void setChilds(ArrayList<MpsModelNode> childs) {
        this.childs = childs;
    }

    public String getNodeInfo() {
        return nodeInfo;
    }

    public void setNodeInfo(String nodeInfo) {
        this.nodeInfo = nodeInfo;
    }


    public ArrayList<MpsModelNodeRef> getRefs() {
        return refs;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }


    public String getNamePropertyValue() {
        for ( MpsModelProperty property : getProperties() ) {
            if ( property.getName().equalsIgnoreCase("name")) {
                return property.getValue();
            }
        }
        return null;
    }

}

