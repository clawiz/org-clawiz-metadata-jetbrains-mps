/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.jetbrains.mps.parser;

import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.utils.reflection.ReflectionUtils;
import org.clawiz.core.common.utils.file.FileUtils;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.parser.data.model.*;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.clawiz.metadata.jetbrains.mps.MpsBase;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import org.clawiz.metadata.jetbrains.mps.parser.context.MpsParserContext;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by abdrashitovta on 09.06.2015.
 */
public class MpsParser extends Service {

    FileUtils fileUtils;

    ReflectionUtils reflectionUtils;
    MpsBase mpsBase;

    ConcurrentHashMap<String, Model> modelsCache = new ConcurrentHashMap<>();

    public void clearCaches() {
        modelsCache.clear();
    }

    public Model parseDirectory(String directoryName, String packageName) {
        MpsParserConfig config = new MpsParserConfig();
        config.setPackageName(packageName);
        return parseDirectory(directoryName, config);
    }

    public Model parseDirectory(String directoryName, MpsParserConfig config) {
        String key = "DIR:" + directoryName + " [" + config.getPackageName() + "]";
        key = key.replaceAll("\\\\", "/");
        Model model = modelsCache.get(key);
        if ( model != null ) {
            logDebug("GET CACHED MPS MODEL " + directoryName);
            return model;
        }

        MpsModel mpsModel = null;
        Serializer serializer = new Persister();
        try {
            String fileName = directoryName + File.separator + ".model";
            logDebug("Start parsing MPS model " + fileName + " " + key);
            mpsModel = serializer.read(MpsModel.class, fileUtils.getInputStream(fileName));

            String[] f = fileUtils.getResourcesList(directoryName);
            for ( String file : f) {
                if ( file.equals(".model")) {
                    continue;
                }
                logDebug("Parse file " + directoryName + File.separator + file);
                MpsModel part = serializer.read(MpsModel.class, fileUtils.getInputStream(directoryName + File.separator + file));
                for (MpsModelRoot root : part.getRoots()) {
                    root.setRegistry(part.getRegistry());
                    mpsModel.getRoots().add(root);
                }
                for (MpsModelImport imp : part.getImports() ) {
                    if ( mpsModel.getImportByIndex(imp.getIndex()) == null ) {
                        mpsModel.getImports().add(imp);
                    }
                }
            }


        } catch (Exception e) {
            throwException("Model parse error '?'\nXML:\n?", new Object[]{e.getMessage()}, e);
        }

        model = parseMpsModel(mpsModel, config);


        modelsCache.put(key, model);
        return model;
    }


    private String prepareModelName(String modelName) {
        String[] tokens = StringUtils.splitByLastTokenOccurrence(modelName, "\\.");
        String preparedName = modelName;
        if ( tokens[tokens.length-1].equals("language") ) {
            preparedName = tokens[0];
        }
        return preparedName;
    }

    public String getLanguageMPLFileName(String modelName) {
        String preparedName = prepareModelName(modelName);

        String filePrefix = ("/" + preparedName + "." + MpsBase.METADATA_LANGUAGE_ADAPTER_JETBRAINS_MPS_PACKAGE_NAME).replaceAll("\\.", "/") + "/";

        String fileName   = filePrefix + modelName + ".mpl";

        if ( ! fileUtils.fileExists(fileName, true)) {
            throwException("Language ? .mpl file ? not found", preparedName, fileName);
        }

        return fileName;
    }

    public String getLanguageModelFileName(String modelName) {
        String filePrefix = ("/" + prepareModelName(modelName) + "." + MpsBase.METADATA_LANGUAGE_ADAPTER_JETBRAINS_MPS_PACKAGE_NAME).replaceAll("\\.", "/") + "/";

        String modelFileName = filePrefix + "models/structure.mps";
        if ( ! fileUtils.fileExists(modelFileName, true ) ) {
            modelFileName = filePrefix + "models/" + modelName + ".structure.mps";
            if ( ! fileUtils.fileExists(modelFileName, true)) {
                modelFileName = filePrefix + "languageModels/language.structure.mps";
                if ( ! fileUtils.fileExists(modelFileName,true)) {
                    throwException("Language model file ?models/structure.mps or ?languageModels/language.structure.mps or ?models/?structure.mps not found", filePrefix, filePrefix, filePrefix, modelName);
                }
            }
        }
        return modelFileName;
    }


    public Model parseLanguage(String modelName, MpsParserConfig sourceParserConfig) {
        MpsParserConfig parserConfig = new MpsParserConfig();
        parserConfig.copyFrom(sourceParserConfig);
        parserConfig.setPackageName(modelName);
        return parseFile(getLanguageModelFileName(modelName), parserConfig);
    }

    public Model parseFile(String fileName, String packageName) {
        MpsParserConfig config = new MpsParserConfig();
        config.setPackageName(packageName);
        return parseFile(fileName, config);
    }

    public Model parseFile(String fileName, MpsParserConfig config) {

        if ( fileName == null) {
            throwException("Mps file name cannot be null");
        }
        if ( fileName == null) {
            throwException("Mps package name cannot be null");
        }

        String key = "FILE:" + fileName+ "." + config.getPackageName();
        Model model = modelsCache.get(key);
        if ( model != null ) {
            return model;
        }

        String xml = fileUtils.readToString(fileName);

        MpsModel mpsModel = null;
        Serializer serializer = new Persister();
        try {
            mpsModel = serializer.read(MpsModel.class, fileUtils.getInputStream(fileName));
        } catch (Exception e) {
            throwException("Response XML parse error '?'\nXML:\n?", new Object[]{e.getMessage(), xml}, e);
        }

        for (MpsModelRoot root : mpsModel.getRoots()) {
            root.setRegistry(mpsModel.getRegistry());
        }

        model = parseMpsModel(mpsModel, config);
        modelsCache.put(key, model);

        return model;
    }


    public void prepareContext(MpsParserContext context) {

        for (MpsModelLanguageUse languageUse : context.getModel().getMpsModel().getLanguageUses() ) {
            if ( languageUse.getName() != null && ! languageUse.getName().equals("jetbrains.mps.lang.structure") ) {
                context.getLanguageContext(languageUse.getName());
            }
        }

    }

    public void processImports(MpsParserContext context) {

        for (MpsModelImport modelImport : context.getMpsModel().getImports()) {


            String[] tokens = StringUtils.splitAndTrim(modelImport.getModelUID(), "\\(");
            if ( tokens.length < 2) { throwException("'(' not found in import '?'", modelImport.getModelUID()); }

            String modelName = tokens[1];
            if ( modelName.substring(modelName.length()-1).equals("')") ) {
                throwException("Import '?' not finished with ')'",modelImport.getModelUID());
            }
            modelName = modelName.substring(0, modelName.length()-1);
            context.addLanguage(modelName, modelImport.getIndex());

            if ( context.getConfig().getSkipImportsCache().containsKey(modelName) ) {
//                logInfo("Skip model '" + modelImport.getIndex() + ":" + modelName + "' import");
                context.addSkippedImport(modelImport);
                continue;
            }

            Model model =  mpsBase.getModelByUID(modelImport.getModelUID(), context.getConfig());
            if ( model == null ) {
                // remove ".structure" added by mps for imported language models
                tokens = StringUtils.splitByLastTokenOccurrence(modelName, "\\.");
                try {
                    model = parseLanguage(tokens[0], context.getConfig());
                } catch (Exception e) {
                    throwException("Exception on parse language ? or unknown model ? : ?", tokens[0], modelName, e.getMessage(), e);
                }
            }
            modelImport.setModel(model);
            context.getModel().getImportedModelsIndexCache().put(modelImport.getIndex(), model);

        }

    }

    private void fillNodeProperties(MpsParserContext context, AbstractMpsNode node) {

        for (MpsModelProperty property : node.getMpsModelNode().getProperties() ) {
            if ( property.getName() == null ) {
                property.setName(node.getModelRegistry().indexToName(property.getRole()));
            }
            String methodName = "set"+property.getName();
            Method method = context.getClassMethod(node.getClass(), methodName);
            if ( method == null ) {
                method = reflectionUtils.findMethod(node.getClass(), methodName);
                if ( method == null ) {
                    throwException("Method set for property '?' not found at class '?'", new Object[]{property.getName(), node.getClass()});
                }
                context.putClassMethod(node.getClass(), methodName, method);
            }
            try {
                String value = property.getValue();
                if ( value != null ) {
                    value = value
                            .replaceAll("&quot;", "\"" )
                            .replaceAll("&lt;", "<")
                            .replaceAll("&gt;", ">")
                            .replaceAll("&amp;", "&")
                            .replaceAll("#!#lf#!#", "\\n")
//                            .replaceAll("#!#backslash#!#", "\\\\")
                    ;
                }
                reflectionUtils.invokeVoid(method, node, value);
            } catch (Exception e) {
                throwException("Exception on invoke node '?' method '?' for value '?' : ?", new Object[]{node.getClass(), methodName, property.getValue(), e.getMessage()}, e);
            }
        }
    }

    public AbstractMpsNode createNode(MpsParserContext context, MpsModelNode mpsNode, AbstractMpsNode parentIdde, MpsModelRegistry registry) {
        Class nodeClass = context.getNodeClass(mpsNode.getType(), mpsNode.getConcept(), registry);
        AbstractMpsNode node = null;
        try {
            node = (AbstractMpsNode) nodeClass.newInstance();
            node.setParserContext(context);
        } catch (Exception e) {
            throwException("Node create exception for type ? and class ? : ?", new Object[]{mpsNode.getType(), nodeClass, e.getMessage()}, e);
        }
        node.setMpsNodeId(mpsNode.getId());
        node.setMpsModelNode(mpsNode);
        node.setSession(getSession());
        node.setParent(parentIdde);
        node.setModel(context.getModel());
        node.setModelRegistry(registry);
        return node;
    }

    public <T extends AbstractMpsNode> T createNode(MpsParserContext context, Class<T> clazz, AbstractMpsNode parentIdde) {
        AbstractMpsNode node = null;
        try {
            node = clazz.newInstance();
            node.setParserContext(context);
        } catch (Exception e) {
            throwException("System error on create node class '?'", new Object[]{clazz});
        }
        node.setMpsNodeId(null);
        node.setMpsModelNode(null);
        node.setSession(getSession());
        node.setParent(parentIdde);
        node.setModel(context.getModel());
        return (T) node;
    }

    private void processNodeLinks(MpsParserContext context, AbstractMpsNode node) {

        for (MpsModelLink link : node.getMpsModelNode().getLinks() ) {

            AbstractMpsNode linkedNode = context.getNodeById(link.getTargetNodeId());
            if ( linkedNode == null ) {
                String[] tokens = StringUtils.splitAndTrim(link.getTargetNodeId(), ":");
                if ( context.isImportIndexSkipped(tokens[0])) {
//                    logInfo("Node link '" + link.getTargetNodeId() + "' not resolved as part of skipped import");
                    continue;
                } else {
                    throwException("MPS linked node not found for id '?'", link.getTargetNodeId());
                }
            }

            Method setMethod = reflectionUtils.findMethod(node.getClass(), "set" + link.getRole());
            if ( setMethod == null ) {
                setMethod = reflectionUtils.findMethod(node.getClass(), "set" + node.getModelRegistry().indexToName(link.getRole()));
                if ( setMethod == null ) {
                    throwException("Method ? for link ? not found for ? (?)", "set" + node.getModelRegistry().indexToName(link.getRole()), link.getRole(), node.getName(), node.getClass().getName());
                }
            }

            reflectionUtils.invokeVoid(setMethod, node, linkedNode);
            Method setMpsLinkMethod = reflectionUtils.findMethod(node.getClass(), "set" + link.getRole() + "MpsModelLink");
            if ( setMpsLinkMethod != null ) {
                reflectionUtils.invokeVoid(setMpsLinkMethod, node, link);
            }

            node.getMpsModelLinkedNodes().add(linkedNode);
            node.getAllMpsModelLinks().add(link);
        }
    }

    private void processNodeLinks(MpsParserContext context, ArrayList<AbstractMpsNode> nodes) {
        for ( AbstractMpsNode node : nodes) {
            processNodeLinks(context, node);
            processNodeLinks(context, node.getMpsModelChildNodes());
        }
    }


    private AbstractMpsNode mpsNodeToAbstractNode(MpsParserContext context, MpsModelNode mpsNode, AbstractMpsNode parentIdde, MpsModelRegistry registry) {
        AbstractMpsNode node = context.getNodeById(mpsNode.getId());
        if ( node == null ) {
            node = createNode(context, mpsNode, parentIdde, registry);
        }
        fillNodeProperties(context, node);
        return node;
    }

    private void processNodeChilds(MpsParserContext context, AbstractMpsNode node, ArrayList<MpsModelNode> mpsChilds, MpsModelRegistry registry) {

        for (MpsModelNode mpsNode : mpsChilds ) {
            AbstractMpsNode childIdde = mpsNodeToAbstractNode(context, mpsNode, node, registry);
            node.addChild(childIdde, mpsNode.getRole());
            processNodeChilds(context, childIdde, mpsNode.getChilds(), registry);
        }
    }


    private void processRoots(MpsParserContext context) {

//        logDebug("Start process roots for " + context.getMpsModel().getModelUID());

        for (MpsModelRoot mpsRoot : context.getMpsModel().getRoots() ) {
//            logDebug("process model root " + mpsRoot.getId());
            try {
                AbstractMpsNode rootNode = mpsNodeToAbstractNode(context, mpsRoot, null, mpsRoot.getRegistry());
                context.getModel().getRootNodes().add(rootNode);
                processNodeChilds(context, rootNode, mpsRoot.getChilds(), mpsRoot.getRegistry());
            } catch (Exception e) {
                throwException("Exception on parse modelRoot '?' with id '?' : ?", mpsRoot.getNamePropertyValue(), mpsRoot.getId(), e.getMessage(), e);
            }
        }
    }



    private void fillForeignKeys(ArrayList<AbstractMpsNode> nodes) {
        for ( AbstractMpsNode node :  nodes  ) {
            node.fillForeignKeys();
            fillForeignKeys(node.getMpsModelChildNodes());
        }
    }

    private void addChildForeignKeys(ArrayList<AbstractMpsNode> nodes)  {
        for ( AbstractMpsNode node :  nodes  ) {
            node.addChildForeignKeys(new HashMap<String, AbstractMpsNode>());
            addChildForeignKeys(node.getMpsModelChildNodes());
        }
    }

    private void sortByForeignKeys(ArrayList<AbstractMpsNode> nodes) {

        for ( int i1=0; i1 < nodes.size()-1; i1++) {
            for ( int i2=i1+1; i2 < nodes.size(); i2++) {
                AbstractMpsNode n1 = nodes.get(i1);
                AbstractMpsNode n2 = nodes.get(i2);

                if ( n1.isForeignKeysContain(n2)  ) {
                    nodes.set(i1, n2);
                    nodes.set(i2, n1);
                }

            }
        }

    }

    public Model parseMpsModel(MpsModel mpsModel, MpsParserConfig config) {

        MpsParserContext context = new MpsParserContext(getSession());
        context.setConfig(config);
        context.setModel(new Model());
        context.getModel().setPackageName(config.getPackageName());
        context.setParser(this);
        context.setMpsModel(mpsModel);
        prepareContext(context);


        for(MpsModelImport i : mpsModel.getImportsList()) {
            mpsModel.getImports().add(i);
        }

        processImports(context);

        processRoots(context);

        processNodeLinks(context, context.getModel().getRootNodes());

        fillForeignKeys(context.getModel().getRootNodes());
        addChildForeignKeys(context.getModel().getRootNodes());
        sortByForeignKeys(context.getModel().getRootNodes());

        logDebug("MPS model " + mpsModel.getModelUID() + " parsed");
        return context.getModel();
    }

}
