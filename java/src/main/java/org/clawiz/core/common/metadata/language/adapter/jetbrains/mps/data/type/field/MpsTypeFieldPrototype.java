package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsTypeFieldPrototype extends AbstractMpsNode {
    
    public String description;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsAbstractValueType valueType;
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.MpsAbstractTypeFieldProperty> properties = new ArrayList<>();
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.description = null;
        }
        this.description = value;
    }
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsAbstractValueType getValueType() {
        return this.valueType;
    }
    
    public void setValueType(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsAbstractValueType value) {
        this.valueType = value;
    }
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.MpsAbstractTypeFieldProperty> getProperties() {
        return this.properties;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7374764979001187723";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.TypeField";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001187723", "org.clawiz.core.common.language.structure.TypeField", ConceptPropertyType.PROPERTY, "6291228963290953133", "description"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001187723", "org.clawiz.core.common.language.structure.TypeField", ConceptPropertyType.CHILD, "7374764979001187727", "valueType"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001187723", "org.clawiz.core.common.language.structure.TypeField", ConceptPropertyType.CHILD, "3896086531067682943", "properties"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("7374764979001187723", "description", getDescription());
        addConceptNodeChild("7374764979001187723", "valueType", getValueType());
        for (AbstractMpsNode value : getProperties() ) {
            addConceptNodeChild("7374764979001187723", "properties", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.type.field.TypeField.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.type.field.TypeField structure = (org.clawiz.core.common.metadata.data.type.field.TypeField) node;
        
        structure.setDescription(getDescription());
        
        if ( getValueType() != null ) {
            structure.setValueType((org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType) getValueType().toMetadataNode(structure, "valueType"));
        } else {
            structure.setValueType(null);
        }
        
        structure.getProperties().clear();
        for (org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.MpsAbstractTypeFieldProperty mpsNode : getProperties() ) {
            if ( mpsNode != null ) {
                structure.getProperties().add((org.clawiz.core.common.metadata.data.type.field.property.AbstractTypeFieldProperty) mpsNode.toMetadataNode(structure, "properties"));
            } else {
                structure.getProperties().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.type.field.TypeField structure = (org.clawiz.core.common.metadata.data.type.field.TypeField) node;
        
        setDescription(structure.getDescription());
        
        if ( structure.getValueType() != null ) {
            setValueType(loadChildMetadataNode(structure.getValueType()));
        } else {
            setValueType(null);
        }
        
        getProperties().clear();
        for (org.clawiz.core.common.metadata.data.type.field.property.AbstractTypeFieldProperty metadataNode : structure.getProperties() ) {
            if ( metadataNode != null ) {
                getProperties().add(loadChildMetadataNode(metadataNode));
            } else {
                getProperties().add(null);
            }
        }
        
    }
}
