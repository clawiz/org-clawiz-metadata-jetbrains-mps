package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsValueTypeListPrototype extends org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsAbstractValueType {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType referencedType;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType getReferencedType() {
        return this.referencedType;
    }
    
    public void setReferencedType(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType value) {
        this.referencedType = value;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "3832164744644143602";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.ValueTypeList";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "3832164744644143602", "org.clawiz.core.common.language.structure.ValueTypeList", ConceptPropertyType.REFERENCE, "3832164744644143603", "referencedType"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeRef("3832164744644143602", "referencedType", getReferencedType());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList structure = (org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList) node;
        
        if ( getReferencedType() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "referencedType", false, getReferencedType());
        } else {
            structure.setReferencedType(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
        addForeignKey(getReferencedType());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList structure = (org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList) node;
        
        if ( structure.getReferencedType() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "referencedType", false, structure.getReferencedType());
        } else {
            setReferencedType(null);
        }
        
    }
}
