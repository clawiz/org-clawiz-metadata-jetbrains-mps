package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractExtensionTypePropertyPrototype extends org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.MpsAbstractTypeProperty {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.method.MpsAbstractExtensionTypePropertyMethod method;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.action.MpsAbstractExtensionTypePropertyAction action;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.method.MpsAbstractExtensionTypePropertyMethod getMethod() {
        return this.method;
    }
    
    public void setMethod(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.method.MpsAbstractExtensionTypePropertyMethod value) {
        this.method = value;
    }
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.action.MpsAbstractExtensionTypePropertyAction getAction() {
        return this.action;
    }
    
    public void setAction(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.action.MpsAbstractExtensionTypePropertyAction value) {
        this.action = value;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2660847388180982647";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.AbstractExtensionTypeProperty";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "2660847388180982647", "org.clawiz.core.common.language.structure.AbstractExtensionTypeProperty", ConceptPropertyType.CHILD, "2660847388180982691", "method"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "2660847388180982647", "org.clawiz.core.common.language.structure.AbstractExtensionTypeProperty", ConceptPropertyType.CHILD, "2660847388180982746", "action"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeChild("2660847388180982647", "method", getMethod());
        addConceptNodeChild("2660847388180982647", "action", getAction());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.type.property.extension.AbstractExtensionTypeProperty.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.core.common.metadata.data.type.property.extension.AbstractExtensionTypeProperty structure = (org.clawiz.core.common.metadata.data.type.property.extension.AbstractExtensionTypeProperty) node;
        
        if ( getMethod() != null ) {
            structure.setMethod((org.clawiz.core.common.metadata.data.type.property.extension.method.AbstractExtensionTypePropertyMethod) getMethod().toMetadataNode(structure, "method"));
        } else {
            structure.setMethod(null);
        }
        
        if ( getAction() != null ) {
            structure.setAction((org.clawiz.core.common.metadata.data.type.property.extension.action.AbstractExtensionTypePropertyAction) getAction().toMetadataNode(structure, "action"));
        } else {
            structure.setAction(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.core.common.metadata.data.type.property.extension.AbstractExtensionTypeProperty structure = (org.clawiz.core.common.metadata.data.type.property.extension.AbstractExtensionTypeProperty) node;
        
        if ( structure.getMethod() != null ) {
            setMethod(loadChildMetadataNode(structure.getMethod()));
        } else {
            setMethod(null);
        }
        
        if ( structure.getAction() != null ) {
            setAction(loadChildMetadataNode(structure.getAction()));
        } else {
            setAction(null);
        }
        
    }
}
