package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.security.permission;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsPermissionTypePrototype extends AbstractMpsNode {
    
    public String description;
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.description = null;
        }
        this.description = value;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "8564055953445511301";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.PermissionType";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "8564055953445511301", "org.clawiz.core.common.language.structure.PermissionType", ConceptPropertyType.PROPERTY, "8564055953445723762", "description"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("8564055953445511301", "description", getDescription());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.security.permission.PermissionType.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.security.permission.PermissionType structure = (org.clawiz.core.common.metadata.data.security.permission.PermissionType) node;
        
        structure.setDescription(getDescription());
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.security.permission.PermissionType structure = (org.clawiz.core.common.metadata.data.security.permission.PermissionType) node;
        
        setDescription(structure.getDescription());
        
    }
}
