package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.database.table.column;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsTableColumnPrototype extends AbstractMpsNode {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.database.table.column.type.MpsAbstractTableColumnType type;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.database.table.column.type.MpsAbstractTableColumnType getType() {
        return this.type;
    }
    
    public void setType(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.database.table.column.type.MpsAbstractTableColumnType value) {
        this.type = value;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "3504386328283174026";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.TableColumn";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "3504386328283174026", "org.clawiz.core.common.language.structure.TableColumn", ConceptPropertyType.CHILD, "3504386328283174143", "type"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeChild("3504386328283174026", "type", getType());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.database.table.column.TableColumn.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.database.table.column.TableColumn structure = (org.clawiz.core.common.metadata.data.database.table.column.TableColumn) node;
        
        if ( getType() != null ) {
            structure.setType((org.clawiz.core.common.metadata.data.database.table.column.type.AbstractTableColumnType) getType().toMetadataNode(structure, "type"));
        } else {
            structure.setType(null);
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.database.table.column.TableColumn structure = (org.clawiz.core.common.metadata.data.database.table.column.TableColumn) node;
        
        if ( structure.getType() != null ) {
            setType(loadChildMetadataNode(structure.getType()));
        } else {
            setType(null);
        }
        
    }
}
