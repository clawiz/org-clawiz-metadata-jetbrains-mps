package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps;

import java.lang.String;

public class MpsLanguageContextPrototype extends org.clawiz.metadata.jetbrains.mps.parser.context.AbstractMpsLanguageContext {
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public void prepare() {
        
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsAbstractValueType.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeNumber.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeDate.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeString.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeScaledNumber.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.key.MpsTypeKey.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.key.MpsTypeKeyField.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeObject.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeDateTime.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.MpsAbstractTypeProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.defaultvalue.MpsDefaultValueTypeFieldProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.MpsAbstractTypeFieldProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeEnumeration.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeEnumerationValue.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeBoolean.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeBLOB.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeText.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.validator.MpsRequiredTypeFieldProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.MpsServiceNode.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethod.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethodRef.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.database.MpsTableColumnTypeFieldProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.MpsAbstractExtensionTypeProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.MpsBeforeExtensionTypeProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.MpsAfterExtensionTypeProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.method.MpsAbstractExtensionTypePropertyMethod.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.method.MpsCheckExtensionTypePropertyMethod.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.method.MpsSaveExtensionTypePropertyMethod.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.method.MpsDeleteExtensionTypePropertyMethod.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.method.MpsCreateExtensionTypePropertyMethod.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.action.MpsAbstractExtensionTypePropertyAction.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.action.MpsCallServiceMethodExtensionTypePropertyAction.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeList.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.security.role.MpsRole.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.security.permission.MpsPermissionType.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.condition.MpsAbstractTypeCondition.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.condition.MpsAbstractConditionTypeProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.condition.MpsChangeableIfConditionTypeProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.condition.MpsVisibleIfConditionTypeProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.condition.MpsHiddenIfConditionTypeProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.condition.MpsReadOnlyIfConditionTypeProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.condition.MpsAbstractConditionTypeFieldProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.condition.MpsChangeableIfConditionTypeFieldProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.condition.MpsVisibleIfConditionTypeFieldProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.condition.MpsHiddenIfConditionTypeFieldProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.condition.MpsReadOnlyIfConditionTypeFieldProperty.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.condition.MpsRoleEqualTypeCondition.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.condition.MpsRoleNotEqualTypeCondition.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.security.role.MpsRoleRef.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.method.action.MpsAbstractTypeMethodAction.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.method.MpsTypeMethod.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.statement.MpsAbstractMethodStatement.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.parameter.MpsServiceMethodParameter.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.database.table.MpsTable.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.database.table.column.MpsTableColumn.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.database.table.column.type.MpsAbstractTableColumnType.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.database.table.column.type.MpsVarcharTableColumnType.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.valuetype.MpsAbstractServiceMethodValueType.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.valuetype.MpsServiceMethodValueTypeNumber.class);
        addClassMap(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.valuetype.MpsServiceMethodValueTypeString.class);
        
    }
}
