package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsServiceMethodRefPrototype extends AbstractMpsNode {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.MpsServiceNode service;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethod method;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.MpsServiceNode getService() {
        return this.service;
    }
    
    public void setService(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.MpsServiceNode value) {
        this.service = value;
    }
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethod getMethod() {
        return this.method;
    }
    
    public void setMethod(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethod value) {
        this.method = value;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2239280014085500682";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.ServiceMethodRef";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "2239280014085500682", "org.clawiz.core.common.language.structure.ServiceMethodRef", ConceptPropertyType.REFERENCE, "2239280014085500683", "service"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "2239280014085500682", "org.clawiz.core.common.language.structure.ServiceMethodRef", ConceptPropertyType.REFERENCE, "2239280014085500684", "method"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeRef("2239280014085500682", "service", getService());
        addConceptNodeRef("2239280014085500682", "method", getMethod());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return null;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
    }
    
    public void fillForeignKeys() {
        addForeignKey(getService());
    }
    
    public void loadMetadataNode(MetadataNode node) {
    }
}
