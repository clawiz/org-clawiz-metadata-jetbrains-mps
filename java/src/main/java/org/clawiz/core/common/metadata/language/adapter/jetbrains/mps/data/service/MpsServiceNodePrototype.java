package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsServiceNodePrototype extends AbstractMpsNode {
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethod> methods = new ArrayList<>();
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethod> getMethods() {
        return this.methods;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "605340112799755415";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.ServiceNode";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "605340112799755415", "org.clawiz.core.common.language.structure.ServiceNode", ConceptPropertyType.CHILD, "605340112799755433", "methods"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        for (AbstractMpsNode value : getMethods() ) {
            addConceptNodeChild("605340112799755415", "methods", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.service.ServiceNode.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.service.ServiceNode structure = (org.clawiz.core.common.metadata.data.service.ServiceNode) node;
        
        structure.getMethods().clear();
        for (org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethod mpsNode : getMethods() ) {
            if ( mpsNode != null ) {
                structure.getMethods().add((org.clawiz.core.common.metadata.data.service.method.ServiceMethod) mpsNode.toMetadataNode(structure, "methods"));
            } else {
                structure.getMethods().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.service.ServiceNode structure = (org.clawiz.core.common.metadata.data.service.ServiceNode) node;
        
        getMethods().clear();
        for (org.clawiz.core.common.metadata.data.service.method.ServiceMethod metadataNode : structure.getMethods() ) {
            if ( metadataNode != null ) {
                getMethods().add(loadChildMetadataNode(metadataNode));
            } else {
                getMethods().add(null);
            }
        }
        
    }
}
