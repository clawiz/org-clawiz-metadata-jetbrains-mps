package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.security.role;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsRolePrototype extends AbstractMpsNode {
    
    public String description;
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.description = null;
        }
        this.description = value;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "8564055953445511289";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.Role";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "8564055953445511289", "org.clawiz.core.common.language.structure.Role", ConceptPropertyType.PROPERTY, "8564055953445723834", "description"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("8564055953445511289", "description", getDescription());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.security.role.Role.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.security.role.Role structure = (org.clawiz.core.common.metadata.data.security.role.Role) node;
        
        structure.setDescription(getDescription());
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.security.role.Role structure = (org.clawiz.core.common.metadata.data.security.role.Role) node;
        
        setDescription(structure.getDescription());
        
    }
}
