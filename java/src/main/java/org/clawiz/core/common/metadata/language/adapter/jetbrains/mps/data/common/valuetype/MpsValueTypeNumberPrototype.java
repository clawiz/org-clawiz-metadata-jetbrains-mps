package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsValueTypeNumberPrototype extends org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsAbstractValueType {
    
    public BigDecimal precision;
    
    public BigDecimal scale;
    
    public BigDecimal getPrecision() {
        return this.precision;
    }
    
    public void setPrecision(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.precision = null;
        }
        this.precision = StringUtils.toBigDecimal(value);
    }
    
    public BigDecimal getScale() {
        return this.scale;
    }
    
    public void setScale(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.scale = null;
        }
        this.scale = StringUtils.toBigDecimal(value);
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7374764979001188259";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.ValueTypeNumber";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001188259", "org.clawiz.core.common.language.structure.ValueTypeNumber", ConceptPropertyType.PROPERTY, "7374764979001199456", "precision"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001188259", "org.clawiz.core.common.language.structure.ValueTypeNumber", ConceptPropertyType.PROPERTY, "7374764979001199457", "scale"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("7374764979001188259", "precision", getPrecision() != null ? getPrecision().toString() : null);
        addConceptNodeProperty("7374764979001188259", "scale", getScale() != null ? getScale().toString() : null);
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeNumber.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeNumber structure = (org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeNumber) node;
        
        structure.setPrecision(getPrecision());
        
        structure.setScale(getScale());
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeNumber structure = (org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeNumber) node;
        
        setPrecision(structure.getPrecision() != null ? structure.getPrecision().toString() : null);
        
        setScale(structure.getScale() != null ? structure.getScale().toString() : null);
        
    }
}
