package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.database.table.column.type;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsVarcharTableColumnTypePrototype extends org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.database.table.column.type.MpsAbstractTableColumnType {
    
    public BigDecimal length;
    
    public BigDecimal getLength() {
        return this.length;
    }
    
    public void setLength(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.length = null;
        }
        this.length = StringUtils.toBigDecimal(value);
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "3504386328283174145";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.VarcharTableColumnType";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "3504386328283174145", "org.clawiz.core.common.language.structure.VarcharTableColumnType", ConceptPropertyType.PROPERTY, "3504386328283174146", "length"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("3504386328283174145", "length", getLength() != null ? getLength().toString() : null);
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.database.table.column.type.VarcharTableColumnType.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.core.common.metadata.data.database.table.column.type.VarcharTableColumnType structure = (org.clawiz.core.common.metadata.data.database.table.column.type.VarcharTableColumnType) node;
        
        structure.setLength(getLength());
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.core.common.metadata.data.database.table.column.type.VarcharTableColumnType structure = (org.clawiz.core.common.metadata.data.database.table.column.type.VarcharTableColumnType) node;
        
        setLength(structure.getLength() != null ? structure.getLength().toString() : null);
        
    }
}
