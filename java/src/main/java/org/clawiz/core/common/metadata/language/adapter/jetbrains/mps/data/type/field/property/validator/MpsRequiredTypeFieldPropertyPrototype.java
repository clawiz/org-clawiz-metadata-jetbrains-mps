package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.validator;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsRequiredTypeFieldPropertyPrototype extends org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.MpsAbstractTypeFieldProperty {
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "1780033530379341310";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.RequiredTypeFieldProperty";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.type.field.property.validator.RequiredTypeFieldProperty.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.core.common.metadata.data.type.field.property.validator.RequiredTypeFieldProperty structure = (org.clawiz.core.common.metadata.data.type.field.property.validator.RequiredTypeFieldProperty) node;
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.core.common.metadata.data.type.field.property.validator.RequiredTypeFieldProperty structure = (org.clawiz.core.common.metadata.data.type.field.property.validator.RequiredTypeFieldProperty) node;
        
    }
}
