package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.action;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsCallServiceMethodExtensionTypePropertyActionPrototype extends org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.action.MpsAbstractExtensionTypePropertyAction {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethodRef serviceMethod;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethodRef getServiceMethod() {
        return this.serviceMethod;
    }
    
    public void setServiceMethod(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethodRef value) {
        this.serviceMethod = value;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2660847388180982722";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.CallServiceMethodExtensionTypePropertyAction";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "2660847388180982722", "org.clawiz.core.common.language.structure.CallServiceMethodExtensionTypePropertyAction", ConceptPropertyType.CHILD, "2660847388180982737", "serviceMethod"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeChild("2660847388180982722", "serviceMethod", getServiceMethod());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.type.property.extension.action.CallServiceMethodExtensionTypePropertyAction.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.core.common.metadata.data.type.property.extension.action.CallServiceMethodExtensionTypePropertyAction structure = (org.clawiz.core.common.metadata.data.type.property.extension.action.CallServiceMethodExtensionTypePropertyAction) node;
        
        if ( getServiceMethod().getMethod() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "serviceMethod", false, getServiceMethod().getMethod());
        } else {
            structure.setServiceMethod(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.core.common.metadata.data.type.property.extension.action.CallServiceMethodExtensionTypePropertyAction structure = (org.clawiz.core.common.metadata.data.type.property.extension.action.CallServiceMethodExtensionTypePropertyAction) node;
        
        if ( structure.getServiceMethod() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "serviceMethod", false, structure.getServiceMethod());
        } else {
            setServiceMethod(null);
        }
        
    }
}
