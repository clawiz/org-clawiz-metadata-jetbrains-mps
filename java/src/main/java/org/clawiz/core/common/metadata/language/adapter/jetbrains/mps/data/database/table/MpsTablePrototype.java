package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.database.table;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsTablePrototype extends AbstractMpsNode {
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.database.table.column.MpsTableColumn> columns = new ArrayList<>();
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.database.table.column.MpsTableColumn> getColumns() {
        return this.columns;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "3504386328283174023";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.Table";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "3504386328283174023", "org.clawiz.core.common.language.structure.Table", ConceptPropertyType.CHILD, "3504386328283174029", "columns"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        for (AbstractMpsNode value : getColumns() ) {
            addConceptNodeChild("3504386328283174023", "columns", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.database.table.Table.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.database.table.Table structure = (org.clawiz.core.common.metadata.data.database.table.Table) node;
        
        structure.getColumns().clear();
        for (org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.database.table.column.MpsTableColumn mpsNode : getColumns() ) {
            if ( mpsNode != null ) {
                structure.getColumns().add((org.clawiz.core.common.metadata.data.database.table.column.TableColumn) mpsNode.toMetadataNode(structure, "columns"));
            } else {
                structure.getColumns().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.database.table.Table structure = (org.clawiz.core.common.metadata.data.database.table.Table) node;
        
        getColumns().clear();
        for (org.clawiz.core.common.metadata.data.database.table.column.TableColumn metadataNode : structure.getColumns() ) {
            if ( metadataNode != null ) {
                getColumns().add(loadChildMetadataNode(metadataNode));
            } else {
                getColumns().add(null);
            }
        }
        
    }
}
