package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;

public class MpsTypeRefPrototype extends AbstractMpsNode {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType entity;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType getEntity() {
        return this.entity;
    }
    
    public void setEntity(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType value) {
        this.entity = value;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "1262809150482071322";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.TypeRef";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "1262809150482071322", "org.clawiz.core.common.language.structure.TypeRef", ConceptPropertyType.REFERENCE, "1262809150482071323", "entity"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeRef("1262809150482071322", "entity", getEntity());
        
    }
}
