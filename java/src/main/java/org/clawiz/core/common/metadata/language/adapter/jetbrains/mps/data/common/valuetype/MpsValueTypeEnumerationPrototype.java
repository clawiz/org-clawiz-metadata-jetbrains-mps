package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsValueTypeEnumerationPrototype extends org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsAbstractValueType {
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeEnumerationValue> values = new ArrayList<>();
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeEnumerationValue> getValues() {
        return this.values;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "3203155936343231452";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.ValueTypeEnumeration";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "3203155936343231452", "org.clawiz.core.common.language.structure.ValueTypeEnumeration", ConceptPropertyType.CHILD, "3203155936343241521", "values"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        for (AbstractMpsNode value : getValues() ) {
            addConceptNodeChild("3203155936343231452", "values", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration structure = (org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration) node;
        
        structure.getValues().clear();
        for (org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsValueTypeEnumerationValue mpsNode : getValues() ) {
            if ( mpsNode != null ) {
                structure.getValues().add((org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumerationValue) mpsNode.toMetadataNode(structure, "values"));
            } else {
                structure.getValues().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration structure = (org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration) node;
        
        getValues().clear();
        for (org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumerationValue metadataNode : structure.getValues() ) {
            if ( metadataNode != null ) {
                getValues().add(loadChildMetadataNode(metadataNode));
            } else {
                getValues().add(null);
            }
        }
        
    }
}
