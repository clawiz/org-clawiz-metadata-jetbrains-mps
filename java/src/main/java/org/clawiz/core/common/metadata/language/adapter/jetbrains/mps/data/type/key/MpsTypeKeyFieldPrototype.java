package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.key;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsTypeKeyFieldPrototype extends AbstractMpsNode {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField field;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField getField() {
        return this.field;
    }
    
    public void setField(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField value) {
        this.field = value;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7374764979001203128";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.TypeKeyField";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001203128", "org.clawiz.core.common.language.structure.TypeKeyField", ConceptPropertyType.REFERENCE, "7374764979001203130", "field"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeRef("7374764979001203128", "field", getField());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.type.key.TypeKeyField.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.type.key.TypeKeyField structure = (org.clawiz.core.common.metadata.data.type.key.TypeKeyField) node;
        
        if ( getField() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "field", false, getField());
        } else {
            structure.setField(null);
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.type.key.TypeKeyField structure = (org.clawiz.core.common.metadata.data.type.key.TypeKeyField) node;
        
        if ( structure.getField() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "field", false, structure.getField());
        } else {
            setField(null);
        }
        
    }
}
