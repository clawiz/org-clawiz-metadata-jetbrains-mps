package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension;

import java.util.ArrayList;

import org.clawiz.core.common.metadata.data.type.property.extension.AfterExtensionTypeProperty;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsExtensionAfterTypePropertyPrototype extends org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.extension.MpsAbstractExtensionTypeProperty {
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2660847388180982659";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.ExtensionAfterTypeProperty";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) AfterExtensionTypeProperty.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        AfterExtensionTypeProperty structure = (AfterExtensionTypeProperty) node;
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        AfterExtensionTypeProperty structure = (AfterExtensionTypeProperty) node;
        
    }
}
