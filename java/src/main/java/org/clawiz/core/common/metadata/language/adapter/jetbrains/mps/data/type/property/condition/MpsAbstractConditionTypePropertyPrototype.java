package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.condition;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractConditionTypePropertyPrototype extends org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.MpsAbstractTypeProperty {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.condition.MpsAbstractTypeCondition condition;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.condition.MpsAbstractTypeCondition getCondition() {
        return this.condition;
    }
    
    public void setCondition(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.condition.MpsAbstractTypeCondition value) {
        this.condition = value;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "8116414171235306271";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.AbstractConditionTypeProperty";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "8116414171235306271", "org.clawiz.core.common.language.structure.AbstractConditionTypeProperty", ConceptPropertyType.CHILD, "8116414171235306277", "condition"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeChild("8116414171235306271", "condition", getCondition());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.type.property.condition.AbstractConditionTypeProperty.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.core.common.metadata.data.type.property.condition.AbstractConditionTypeProperty structure = (org.clawiz.core.common.metadata.data.type.property.condition.AbstractConditionTypeProperty) node;
        
        if ( getCondition() != null ) {
            structure.setCondition((org.clawiz.core.common.metadata.data.type.condition.AbstractTypeCondition) getCondition().toMetadataNode(structure, "condition"));
        } else {
            structure.setCondition(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.core.common.metadata.data.type.property.condition.AbstractConditionTypeProperty structure = (org.clawiz.core.common.metadata.data.type.property.condition.AbstractConditionTypeProperty) node;
        
        if ( structure.getCondition() != null ) {
            setCondition(loadChildMetadataNode(structure.getCondition()));
        } else {
            setCondition(null);
        }
        
    }
}
