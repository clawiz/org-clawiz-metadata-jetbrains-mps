package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsValueTypeObjectPrototype extends org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsAbstractValueType {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType referencedType;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType getReferencedType() {
        return this.referencedType;
    }
    
    public void setReferencedType(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType value) {
        this.referencedType = value;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "1364070692453599615";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.ValueTypeObject";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "1364070692453599615", "org.clawiz.core.common.language.structure.ValueTypeObject", ConceptPropertyType.REFERENCE, "1364070692453599617", "referencedType"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeRef("1364070692453599615", "referencedType", getReferencedType());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject structure = (org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject) node;
        
        if ( getReferencedType() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "referencedType", false, getReferencedType());
        } else {
            structure.setReferencedType(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
        addForeignKey(getReferencedType());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject structure = (org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject) node;
        
        if ( structure.getReferencedType() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "referencedType", false, structure.getReferencedType());
        } else {
            setReferencedType(null);
        }
        
    }
}
