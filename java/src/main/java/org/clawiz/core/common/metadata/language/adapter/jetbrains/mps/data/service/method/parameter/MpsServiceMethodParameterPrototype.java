package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.parameter;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsServiceMethodParameterPrototype extends AbstractMpsNode {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.valuetype.MpsAbstractServiceMethodValueType valueType;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.valuetype.MpsAbstractServiceMethodValueType getValueType() {
        return this.valueType;
    }
    
    public void setValueType(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.valuetype.MpsAbstractServiceMethodValueType value) {
        this.valueType = value;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "3504386328283111625";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.ServiceMethodParameter";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "3504386328283111625", "org.clawiz.core.common.language.structure.ServiceMethodParameter", ConceptPropertyType.CHILD, "3504386328283111628", "valueType"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeChild("3504386328283111625", "valueType", getValueType());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameter.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameter structure = (org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameter) node;
        
        if ( getValueType() != null ) {
            structure.setValueType((org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType) getValueType().toMetadataNode(structure, "valueType"));
        } else {
            structure.setValueType(null);
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameter structure = (org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameter) node;
        
        if ( structure.getValueType() != null ) {
            setValueType(loadChildMetadataNode(structure.getValueType()));
        } else {
            setValueType(null);
        }
        
    }
}
