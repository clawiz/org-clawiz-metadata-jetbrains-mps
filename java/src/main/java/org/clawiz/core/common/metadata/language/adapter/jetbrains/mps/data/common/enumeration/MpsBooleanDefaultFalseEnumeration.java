package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.enumeration;

import org.clawiz.core.common.CoreException;

public enum MpsBooleanDefaultFalseEnumeration {

    TRUE, FALSE;

    
    public static MpsBooleanDefaultFalseEnumeration getDefaultValue() {
        return FALSE;
    }
    
    public static MpsBooleanDefaultFalseEnumeration toMpsBooleanDefaultFalseEnumeration(String string) {
        if ( string == null ) {
            return null;
        }
        
        try {
            return MpsBooleanDefaultFalseEnumeration.valueOf(string.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong MpsBooleanDefaultFalseEnumeration value '?", string);
        }
        
    }
    
    public static String toConceptNodePropertyString(MpsBooleanDefaultFalseEnumeration value) {
        if ( value == null ) { 
            return null;
        }
         
        switch (value) {
            case TRUE : return "true";
            case FALSE : return "false";
        }
         
        return null;
    }
}
