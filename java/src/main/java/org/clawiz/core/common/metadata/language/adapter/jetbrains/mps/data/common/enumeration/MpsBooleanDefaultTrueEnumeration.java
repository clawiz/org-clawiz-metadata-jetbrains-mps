package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.enumeration;

import org.clawiz.core.common.CoreException;

public enum MpsBooleanDefaultTrueEnumeration {

    TRUE, FALSE;

    
    public static MpsBooleanDefaultTrueEnumeration getDefaultValue() {
        return TRUE;
    }
    
    public static MpsBooleanDefaultTrueEnumeration toMpsBooleanDefaultTrueEnumeration(String string) {
        if ( string == null ) {
            return null;
        }
        
        try {
            return MpsBooleanDefaultTrueEnumeration.valueOf(string.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong MpsBooleanDefaultTrueEnumeration value '?", string);
        }
        
    }
    
    public static String toConceptNodePropertyString(MpsBooleanDefaultTrueEnumeration value) {
        if ( value == null ) { 
            return null;
        }
         
        switch (value) {
            case TRUE : return "true";
            case FALSE : return "false";
        }
         
        return null;
    }
}
