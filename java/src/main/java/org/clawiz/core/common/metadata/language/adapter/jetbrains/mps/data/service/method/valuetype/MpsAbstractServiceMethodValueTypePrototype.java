package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.valuetype;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractServiceMethodValueTypePrototype extends AbstractMpsNode {
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "3504386328283206554";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.AbstractServiceMethodValueType";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        
        return result;
    }
    
    public void fillConceptNode() {
        
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType structure = (org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType) node;
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType structure = (org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType) node;
        
    }
}
