package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsTypePrototype extends AbstractMpsNode {
    
    public String description;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.enumeration.MpsBooleanDefaultTrueEnumeration install;
    
    public String tableName;
    
    public String javaName;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.key.MpsTypeKey toStringKey;
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField> fields = new ArrayList<>();
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.key.MpsTypeKey> keys = new ArrayList<>();
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType> childs = new ArrayList<>();
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.MpsAbstractTypeProperty> properties = new ArrayList<>();
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.method.MpsTypeMethod> serviceMethods = new ArrayList<>();
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.description = null;
        }
        this.description = value;
    }
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.enumeration.MpsBooleanDefaultTrueEnumeration getInstall() {
        return this.install;
    }
    
    public void setInstall(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.install = null;
        }
        this.install = org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.enumeration.MpsBooleanDefaultTrueEnumeration.toMpsBooleanDefaultTrueEnumeration(value);
    }
    
    public String getTableName() {
        return this.tableName;
    }
    
    public void setTableName(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.tableName = null;
        }
        this.tableName = value;
    }
    
    public String getJavaName() {
        return this.javaName;
    }
    
    public void setJavaName(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.javaName = null;
        }
        this.javaName = value;
    }
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.key.MpsTypeKey getToStringKey() {
        return this.toStringKey;
    }
    
    public void setToStringKey(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.key.MpsTypeKey value) {
        this.toStringKey = value;
    }
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField> getFields() {
        return this.fields;
    }
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.key.MpsTypeKey> getKeys() {
        return this.keys;
    }
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType> getChilds() {
        return this.childs;
    }
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.MpsAbstractTypeProperty> getProperties() {
        return this.properties;
    }
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.method.MpsTypeMethod> getServiceMethods() {
        return this.serviceMethods;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7374764979001032776";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.Type";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001032776", "org.clawiz.core.common.language.structure.Type", ConceptPropertyType.PROPERTY, "6291228963290953132", "description"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001032776", "org.clawiz.core.common.language.structure.Type", ConceptPropertyType.PROPERTY, "1364070692453562734", "install"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001032776", "org.clawiz.core.common.language.structure.Type", ConceptPropertyType.PROPERTY, "4691823775969122300", "tableName"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001032776", "org.clawiz.core.common.language.structure.Type", ConceptPropertyType.PROPERTY, "2812887031877983151", "javaName"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001032776", "org.clawiz.core.common.language.structure.Type", ConceptPropertyType.REFERENCE, "1780033530379772439", "toStringKey"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001032776", "org.clawiz.core.common.language.structure.Type", ConceptPropertyType.CHILD, "7374764979001188880", "fields"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001032776", "org.clawiz.core.common.language.structure.Type", ConceptPropertyType.CHILD, "7374764979001206465", "keys"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001032776", "org.clawiz.core.common.language.structure.Type", ConceptPropertyType.CHILD, "7990555497954221500", "childs"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001032776", "org.clawiz.core.common.language.structure.Type", ConceptPropertyType.CHILD, "8770705378692856957", "properties"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001032776", "org.clawiz.core.common.language.structure.Type", ConceptPropertyType.CHILD, "8748341616664825889", "serviceMethods"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("7374764979001032776", "description", getDescription());
        addConceptNodeProperty("7374764979001032776", "install", org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.enumeration.MpsBooleanDefaultTrueEnumeration.toConceptNodePropertyString(getInstall()));
        addConceptNodeProperty("7374764979001032776", "tableName", getTableName());
        addConceptNodeProperty("7374764979001032776", "javaName", getJavaName());
        for (AbstractMpsNode value : getFields() ) {
            addConceptNodeChild("7374764979001032776", "fields", value);
        }
        for (AbstractMpsNode value : getKeys() ) {
            addConceptNodeChild("7374764979001032776", "keys", value);
        }
        for (AbstractMpsNode value : getChilds() ) {
            addConceptNodeChild("7374764979001032776", "childs", value);
        }
        for (AbstractMpsNode value : getProperties() ) {
            addConceptNodeChild("7374764979001032776", "properties", value);
        }
        for (AbstractMpsNode value : getServiceMethods() ) {
            addConceptNodeChild("7374764979001032776", "serviceMethods", value);
        }
        addConceptNodeRef("7374764979001032776", "toStringKey", getToStringKey());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.type.Type.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.type.Type structure = (org.clawiz.core.common.metadata.data.type.Type) node;
        
        structure.setDescription(getDescription());
        
        structure.setInstall(( getInstall() != null ? org.clawiz.core.common.metadata.data.common.enumeration.BooleanDefaultTrueEnumeration.valueOf(getInstall().toString()) : null ));
        
        structure.setTableName(getTableName());
        
        structure.setJavaName(getJavaName());
        
        if ( getToStringKey() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "toStringKey", false, getToStringKey());
        } else {
            structure.setToStringKey(null);
        }
        
        structure.getFields().clear();
        for (org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField mpsNode : getFields() ) {
            if ( mpsNode != null ) {
                structure.getFields().add((org.clawiz.core.common.metadata.data.type.field.TypeField) mpsNode.toMetadataNode(structure, "fields"));
            } else {
                structure.getFields().add(null);
            }
        }
        
        structure.getKeys().clear();
        for (org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.key.MpsTypeKey mpsNode : getKeys() ) {
            if ( mpsNode != null ) {
                structure.getKeys().add((org.clawiz.core.common.metadata.data.type.key.TypeKey) mpsNode.toMetadataNode(structure, "keys"));
            } else {
                structure.getKeys().add(null);
            }
        }
        
        structure.getChilds().clear();
        for (org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType mpsNode : getChilds() ) {
            if ( mpsNode != null ) {
                structure.getChilds().add((org.clawiz.core.common.metadata.data.type.Type) mpsNode.toMetadataNode(structure, "childs"));
            } else {
                structure.getChilds().add(null);
            }
        }
        
        structure.getProperties().clear();
        for (org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.property.MpsAbstractTypeProperty mpsNode : getProperties() ) {
            if ( mpsNode != null ) {
                structure.getProperties().add((org.clawiz.core.common.metadata.data.type.property.AbstractTypeProperty) mpsNode.toMetadataNode(structure, "properties"));
            } else {
                structure.getProperties().add(null);
            }
        }
        
        structure.getServiceMethods().clear();
        for (org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.method.MpsTypeMethod mpsNode : getServiceMethods() ) {
            if ( mpsNode != null ) {
                structure.getServiceMethods().add((org.clawiz.core.common.metadata.data.type.method.TypeMethod) mpsNode.toMetadataNode(structure, "serviceMethods"));
            } else {
                structure.getServiceMethods().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.type.Type structure = (org.clawiz.core.common.metadata.data.type.Type) node;
        
        setDescription(structure.getDescription());
        
        setInstall(structure.getInstall() != null ? structure.getInstall().toString() : null);
        
        setTableName(structure.getTableName());
        
        setJavaName(structure.getJavaName());
        
        if ( structure.getToStringKey() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "toStringKey", false, structure.getToStringKey());
        } else {
            setToStringKey(null);
        }
        
        getFields().clear();
        for (org.clawiz.core.common.metadata.data.type.field.TypeField metadataNode : structure.getFields() ) {
            if ( metadataNode != null ) {
                getFields().add(loadChildMetadataNode(metadataNode));
            } else {
                getFields().add(null);
            }
        }
        
        getKeys().clear();
        for (org.clawiz.core.common.metadata.data.type.key.TypeKey metadataNode : structure.getKeys() ) {
            if ( metadataNode != null ) {
                getKeys().add(loadChildMetadataNode(metadataNode));
            } else {
                getKeys().add(null);
            }
        }
        
        getChilds().clear();
        for (org.clawiz.core.common.metadata.data.type.Type metadataNode : structure.getChilds() ) {
            if ( metadataNode != null ) {
                getChilds().add(loadChildMetadataNode(metadataNode));
            } else {
                getChilds().add(null);
            }
        }
        
        getProperties().clear();
        for (org.clawiz.core.common.metadata.data.type.property.AbstractTypeProperty metadataNode : structure.getProperties() ) {
            if ( metadataNode != null ) {
                getProperties().add(loadChildMetadataNode(metadataNode));
            } else {
                getProperties().add(null);
            }
        }
        
        getServiceMethods().clear();
        for (org.clawiz.core.common.metadata.data.type.method.TypeMethod metadataNode : structure.getServiceMethods() ) {
            if ( metadataNode != null ) {
                getServiceMethods().add(loadChildMetadataNode(metadataNode));
            } else {
                getServiceMethods().add(null);
            }
        }
        
    }
}
