package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.condition;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsRoleNotEqualTypeConditionPrototype extends org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.condition.MpsAbstractTypeCondition {
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.security.role.MpsRoleRef> roles = new ArrayList<>();
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.security.role.MpsRoleRef> getRoles() {
        return this.roles;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "8116414171235306487";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.RoleNotEqualTypeCondition";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "8116414171235306487", "org.clawiz.core.common.language.structure.RoleNotEqualTypeCondition", ConceptPropertyType.CHILD, "3604368045025137852", "roles"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        for (AbstractMpsNode value : getRoles() ) {
            addConceptNodeChild("8116414171235306487", "roles", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.type.condition.RoleNotEqualTypeCondition.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.core.common.metadata.data.type.condition.RoleNotEqualTypeCondition structure = (org.clawiz.core.common.metadata.data.type.condition.RoleNotEqualTypeCondition) node;
        
        structure.getRoles().clear();
        for (org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.security.role.MpsRoleRef mpsNode : getRoles() ) {
            if ( mpsNode.getRole() != null ) {
                getParserContext().addDeferredParseNodeResolve(structure, "roles", true, mpsNode.getRole());
            } else {
                structure.getRoles().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.core.common.metadata.data.type.condition.RoleNotEqualTypeCondition structure = (org.clawiz.core.common.metadata.data.type.condition.RoleNotEqualTypeCondition) node;
        
        getRoles().clear();
        for (org.clawiz.core.common.metadata.data.security.role.Role metadataNode : structure.getRoles() ) {
            if ( metadataNode != null ) {
                getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "roles", true, metadataNode);
            } else {
                getRoles().add(null);
            }
        }
        
    }
}
