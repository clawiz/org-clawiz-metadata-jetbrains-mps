package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.security.role;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsRoleRefPrototype extends AbstractMpsNode {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.security.role.MpsRole role;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.security.role.MpsRole getRole() {
        return this.role;
    }
    
    public void setRole(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.security.role.MpsRole value) {
        this.role = value;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "3604368045025137794";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.RoleRef";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "3604368045025137794", "org.clawiz.core.common.language.structure.RoleRef", ConceptPropertyType.REFERENCE, "3604368045025137795", "role"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeRef("3604368045025137794", "role", getRole());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return null;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
    }
    
    public void fillForeignKeys() {
        addForeignKey(getRole());
    }
    
    public void loadMetadataNode(MetadataNode node) {
    }
}
