package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.key;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsTypeKeyPrototype extends AbstractMpsNode {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.enumeration.MpsBooleanDefaultFalseEnumeration unique;
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.key.MpsTypeKeyField> fields = new ArrayList<>();
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.enumeration.MpsBooleanDefaultFalseEnumeration getUnique() {
        return this.unique;
    }
    
    public void setUnique(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.unique = null;
        }
        this.unique = org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.enumeration.MpsBooleanDefaultFalseEnumeration.toMpsBooleanDefaultFalseEnumeration(value);
    }
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.key.MpsTypeKeyField> getFields() {
        return this.fields;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7374764979001203122";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.TypeKey";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001203122", "org.clawiz.core.common.language.structure.TypeKey", ConceptPropertyType.PROPERTY, "7374764979001203124", "unique"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "7374764979001203122", "org.clawiz.core.common.language.structure.TypeKey", ConceptPropertyType.CHILD, "7374764979001206446", "fields"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("7374764979001203122", "unique", org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.enumeration.MpsBooleanDefaultFalseEnumeration.toConceptNodePropertyString(getUnique()));
        for (AbstractMpsNode value : getFields() ) {
            addConceptNodeChild("7374764979001203122", "fields", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.type.key.TypeKey.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.type.key.TypeKey structure = (org.clawiz.core.common.metadata.data.type.key.TypeKey) node;
        
        structure.setUnique(( getUnique() != null ? org.clawiz.core.common.metadata.data.common.enumeration.BooleanDefaultFalseEnumeration.valueOf(getUnique().toString()) : null ));
        
        structure.getFields().clear();
        for (org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.key.MpsTypeKeyField mpsNode : getFields() ) {
            if ( mpsNode != null ) {
                structure.getFields().add((org.clawiz.core.common.metadata.data.type.key.TypeKeyField) mpsNode.toMetadataNode(structure, "fields"));
            } else {
                structure.getFields().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.type.key.TypeKey structure = (org.clawiz.core.common.metadata.data.type.key.TypeKey) node;
        
        setUnique(structure.getUnique() != null ? structure.getUnique().toString() : null);
        
        getFields().clear();
        for (org.clawiz.core.common.metadata.data.type.key.TypeKeyField metadataNode : structure.getFields() ) {
            if ( metadataNode != null ) {
                getFields().add(loadChildMetadataNode(metadataNode));
            } else {
                getFields().add(null);
            }
        }
        
    }
}
