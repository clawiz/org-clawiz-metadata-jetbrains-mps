package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsServiceMethodPrototype extends AbstractMpsNode {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.valuetype.MpsAbstractServiceMethodValueType valueType;
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.parameter.MpsServiceMethodParameter> parameters = new ArrayList<>();
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.statement.MpsAbstractMethodStatement> statements = new ArrayList<>();
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.valuetype.MpsAbstractServiceMethodValueType getValueType() {
        return this.valueType;
    }
    
    public void setValueType(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.valuetype.MpsAbstractServiceMethodValueType value) {
        this.valueType = value;
    }
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.parameter.MpsServiceMethodParameter> getParameters() {
        return this.parameters;
    }
    
    public ArrayList<org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.statement.MpsAbstractMethodStatement> getStatements() {
        return this.statements;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "605340112799755418";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.ServiceMethod";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "605340112799755418", "org.clawiz.core.common.language.structure.ServiceMethod", ConceptPropertyType.CHILD, "8693134025632222053", "valueType"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "605340112799755418", "org.clawiz.core.common.language.structure.ServiceMethod", ConceptPropertyType.CHILD, "3504386328283142933", "parameters"));
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "605340112799755418", "org.clawiz.core.common.language.structure.ServiceMethod", ConceptPropertyType.CHILD, "3504386328283143040", "statements"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeChild("605340112799755418", "valueType", getValueType());
        for (AbstractMpsNode value : getParameters() ) {
            addConceptNodeChild("605340112799755418", "parameters", value);
        }
        for (AbstractMpsNode value : getStatements() ) {
            addConceptNodeChild("605340112799755418", "statements", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.service.method.ServiceMethod.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.service.method.ServiceMethod structure = (org.clawiz.core.common.metadata.data.service.method.ServiceMethod) node;
        
        if ( getValueType() != null ) {
            structure.setValueType((org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType) getValueType().toMetadataNode(structure, "valueType"));
        } else {
            structure.setValueType(null);
        }
        
        structure.getParameters().clear();
        for (org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.parameter.MpsServiceMethodParameter mpsNode : getParameters() ) {
            if ( mpsNode != null ) {
                structure.getParameters().add((org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameter) mpsNode.toMetadataNode(structure, "parameters"));
            } else {
                structure.getParameters().add(null);
            }
        }
        
        structure.getStatements().clear();
        for (org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.statement.MpsAbstractMethodStatement mpsNode : getStatements() ) {
            if ( mpsNode != null ) {
                structure.getStatements().add((org.clawiz.core.common.metadata.data.service.method.statement.AbstractMethodStatement) mpsNode.toMetadataNode(structure, "statements"));
            } else {
                structure.getStatements().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.service.method.ServiceMethod structure = (org.clawiz.core.common.metadata.data.service.method.ServiceMethod) node;
        
        if ( structure.getValueType() != null ) {
            setValueType(loadChildMetadataNode(structure.getValueType()));
        } else {
            setValueType(null);
        }
        
        getParameters().clear();
        for (org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameter metadataNode : structure.getParameters() ) {
            if ( metadataNode != null ) {
                getParameters().add(loadChildMetadataNode(metadataNode));
            } else {
                getParameters().add(null);
            }
        }
        
        getStatements().clear();
        for (org.clawiz.core.common.metadata.data.service.method.statement.AbstractMethodStatement metadataNode : structure.getStatements() ) {
            if ( metadataNode != null ) {
                getStatements().add(loadChildMetadataNode(metadataNode));
            } else {
                getStatements().add(null);
            }
        }
        
    }
}
