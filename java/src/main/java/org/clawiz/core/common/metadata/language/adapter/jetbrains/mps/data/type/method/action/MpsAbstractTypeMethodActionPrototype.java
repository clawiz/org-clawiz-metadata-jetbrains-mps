package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.method.action;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractTypeMethodActionPrototype extends AbstractMpsNode {
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "623359627484193914";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.AbstractTypeMethodAction";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        
        return result;
    }
    
    public void fillConceptNode() {
        
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.type.method.action.AbstractTypeMethodAction.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.type.method.action.AbstractTypeMethodAction structure = (org.clawiz.core.common.metadata.data.type.method.action.AbstractTypeMethodAction) node;
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.core.common.metadata.data.type.method.action.AbstractTypeMethodAction structure = (org.clawiz.core.common.metadata.data.type.method.action.AbstractTypeMethodAction) node;
        
    }
}
