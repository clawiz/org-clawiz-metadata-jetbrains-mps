package org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.database;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsTableColumnTypeFieldPropertyPrototype extends org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.property.MpsAbstractTypeFieldProperty {
    
    public String columnName;
    
    public String getColumnName() {
        return this.columnName;
    }
    
    public void setColumnName(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.columnName = null;
        }
        this.columnName = value;
    }
    
    public String getLanguageId() {
        return "97fd598e-b769-49ad-bf12-ef327309f6db";
    }
    
    public String getLanguageName() {
        return "org.clawiz.core.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2464348282968765615";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.core.common.language.structure.TableColumnTypeFieldProperty";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("97fd598e-b769-49ad-bf12-ef327309f6db", "org.clawiz.core.common.language", "2464348282968765615", "org.clawiz.core.common.language.structure.TableColumnTypeFieldProperty", ConceptPropertyType.PROPERTY, "2464348282968765616", "columnName"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("2464348282968765615", "columnName", getColumnName());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.core.common.metadata.data.type.field.property.database.TableColumnTypeFieldProperty.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.core.common.metadata.data.type.field.property.database.TableColumnTypeFieldProperty structure = (org.clawiz.core.common.metadata.data.type.field.property.database.TableColumnTypeFieldProperty) node;
        
        structure.setColumnName(getColumnName());
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.core.common.metadata.data.type.field.property.database.TableColumnTypeFieldProperty structure = (org.clawiz.core.common.metadata.data.type.field.property.database.TableColumnTypeFieldProperty) node;
        
        setColumnName(structure.getColumnName());
        
    }
}
